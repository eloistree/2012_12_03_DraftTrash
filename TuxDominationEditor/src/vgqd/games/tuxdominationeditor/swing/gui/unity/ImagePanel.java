package vgqd.games.tuxdominationeditor.swing.gui.unity;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JPanel;

public class ImagePanel extends JPanel {

	/**
	 * Create the panel.
	 * @param string 
	 */
	
	private Image image;
	public ImagePanel(String string) {
		this.setMinimumSize(new Dimension(50,50));
		this.setBackground(Color.lightGray);
		
	}

	public void setImage(String path,boolean onTheWeb) {
		URL url;
		if (onTheWeb==true)
			try {
				url = new URL(path);
				image = Toolkit.getDefaultToolkit().getImage(url);
			} catch (MalformedURLException e) {
				System.out.println("No Image at all");
			}
		//else Toolkit.getDefaultToolkit().getImage(path);
		
	}

	@Override
	public void paintComponent(Graphics g) {
		 super.paintComponent(g);
	      if (image!=null) g.drawImage(image, 0, 0,this.getWidth(),this.getHeight(), this);
		
	}
	
	

}
