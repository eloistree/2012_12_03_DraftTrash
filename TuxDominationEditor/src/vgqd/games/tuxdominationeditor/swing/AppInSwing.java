package vgqd.games.tuxdominationeditor.swing;

import vgqd.games.tuxdomination.GestionnaireApp;
import vgqd.games.tuxdomination.baseclass.Army;
import vgqd.games.tuxdominationeditor.swing.gui.JTuxEditor;

public class AppInSwing extends GestionnaireApp{
	
	private JTuxEditor window = new JTuxEditor(appObserver);

	public AppInSwing(Army armeeEnMemoire) {
		super(armeeEnMemoire);
		window.setTitle(""+armeeEnMemoire.getName());
		window.update(this.armeeEnMemoire);
		//window.update(pointedUnity);
		
				
		start();
	}
	

	@Override
	public void start() {
		super.start();
		window.setVisible(true);
	}


	@Override
	public String getNameCurrent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setNameCurrent(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getNameArmyCurrent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setNameArmyCurrent(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getAttackCurrent() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setAttackCurrent(double attack) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getDefenceCurrent() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setDefenceCurrent(double defence) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getVieCurrent() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setVieCurrent(double vie) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getPorteeCurrent() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setPorteeCurrent(double portee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getActionPointCurrent() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setActionPointCurrent(double actionpoint) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void next() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void previous() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showXMLResult() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showDescription() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void updateDescripteurs() {
		// TODO Auto-generated method stub
		
	}

}
