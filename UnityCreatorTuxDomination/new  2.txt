package vgqd.games.tuxdominationeditor.swing.gui.unity;


import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;



@SuppressWarnings("serial")
public class UnityFullPanel extends javax.swing.JPanel {


    public UnityFullPanel() {
        initComponents();
    }

    private void initComponents() {

        jTextField5 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        //ImagePanel = new ImagePanel(null);
        DescriptionPanel = new javax.swing.JPanel();
        jLabName = new javax.swing.JLabel();
        jTextName = new javax.swing.JTextField();
        jLabVie = new javax.swing.JLabel();
        jTextVie = new javax.swing.JTextField();
        jLabPortee = new javax.swing.JLabel();
        jTextPortee = new javax.swing.JTextField();
        jLabAttack = new javax.swing.JLabel();
        jTextAttack = new javax.swing.JTextField();
        jLabPointAction = new javax.swing.JLabel();
        jTextPointAction = new javax.swing.JTextField();
        jLabDefence = new javax.swing.JLabel();
        jTextDefence = new javax.swing.JTextField();

        jTextField5.setText("jTextField5");

        setLayout(new java.awt.BorderLayout());

        jPanel1.setMaximumSize(new java.awt.Dimension(100, 100));

        //ImagePanel.setBackground(new java.awt.Color(0, 0, 0));
        //ImagePanel.setMinimumSize(new java.awt.Dimension(50, 50));
        //ImagePanel.setPreferredSize(new java.awt.Dimension(50, 50));

        //javax.swing.GroupLayout ImagePanelLayout = new javax.swing.GroupLayout(ImagePanel);
        /*ImagePanel.setLayout(ImagePanelLayout);
         ImagePanelLayout.setHorizontalGroup(
            ImagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );
        ImagePanelLayout.setVerticalGroup(
            ImagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );*/

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 25, Short.MAX_VALUE)
                    //.addComponent(ImagePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 25, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 110, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 50, Short.MAX_VALUE)
                    //.addComponent(ImagePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 50, Short.MAX_VALUE)))
        );

        //ImagePanel.getAccessibleContext().setAccessibleName("");

        add(jPanel1, java.awt.BorderLayout.WEST);

        DescriptionPanel.setLayout(new java.awt.GridLayout(3, 4));

        jLabName.setText("Vie");
        DescriptionPanel.add(jLabName);

        jTextName.setCaret(null);
        DescriptionPanel.add(jTextName);

        
        DescriptionPanel.add(new JPanel());
        DescriptionPanel.add(new JPanel());

        
        jLabVie.setText("Vie");
        DescriptionPanel.add(jLabVie);

        jTextVie.setCaret(null);
        DescriptionPanel.add(jTextVie);

        jLabPortee.setText("Portee");
        DescriptionPanel.add(jLabPortee);

        jTextPortee.setToolTipText("");
        jTextPortee.setCaret(null);
        DescriptionPanel.add(jTextPortee);

        jLabAttack.setText("Attack");
        DescriptionPanel.add(jLabAttack);

        jTextAttack.setCaret(null);
        DescriptionPanel.add(jTextAttack);

        jLabPointAction.setText("Point action");
        DescriptionPanel.add(jLabPointAction);

        jTextPointAction.setToolTipText("");
        jTextPointAction.setCaret(null);
        DescriptionPanel.add(jTextPointAction);

        jLabDefence.setText("Def");
        DescriptionPanel.add(jLabDefence);

        jTextDefence.setCaret(null);
        DescriptionPanel.add(jTextDefence);

        add(DescriptionPanel, java.awt.BorderLayout.CENTER);
    }
    
    private javax.swing.JPanel DescriptionPanel;
    // private ImagePanel ImagePanel;
    private javax.swing.JLabel jLabAttack;
    private javax.swing.JLabel jLabDefence;
    private javax.swing.JLabel jLabPointAction;
    private javax.swing.JLabel jLabPortee;
    private javax.swing.JLabel jLabVie;
    private javax.swing.JLabel jLabName;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextName;
    private javax.swing.JTextField jTextAttack;
    private javax.swing.JTextField jTextDefence;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextPointAction;
    private javax.swing.JTextField jTextPortee;
    private javax.swing.JTextField jTextVie;
	/*public javax.swing.JPanel getImagePanel() {
		return ImagePanel;
	}

	public void setImagePanel(String path,boolean fromTheWeb) {
		ImagePanel.setImage(path,fromTheWeb);
	}*/

	public String getAttack() {
		return jTextAttack.getText();
	}

	public void setAttack(double value) {
		this.jTextAttack.setText(""+value);
	}

	public String getDefence() {
		return jTextDefence.getText();
	}

	public void setDefence(double value) {
		this.jTextDefence.setText(""+value);
	}

	public String getField5() {
		return jTextField5.getText();
	}

	public void setField5(double value) {
		this.jTextField5.setText(""+value);
	}

	public String getPointAction() {
		return jTextPointAction.getText();
	}

	public void setPointAction(double value) {
		this.jTextPointAction.setText(""+value);
	}

	public String getPortee() {
		return jTextPortee.getText();
	}

	public void setPortee(double value) {
		this.jTextPortee.setText(""+value);
	}

	public String getVie() {
		return jTextVie.getText();
	}

	public void setVie(double value) {
		this.jTextVie.setText(""+value);
	}
	public String getName() {
		return jTextVie.getText();
	}

	public void setName(String value) {
		this.jTextVie.setText(""+value);
	}
    
    public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setLayout(new GridLayout());
		f.setSize(200,100);
		f.add(new UnityFullPanel());
		f.setVisible(true);
	}
    
   
}

