package vgqd.games.tuxdomination.IO;

import vgqd.games.tuxdomination.IO.in.XMLToArmy;
import vgqd.games.tuxdomination.IO.out.ExportToXML;
import vgqd.games.tuxdomination.baseclass.Army;

public class InOutManager implements WhatInOutHaveToDo{

	@Override
	public void exportSomeUnitiesToXML(Army army, String path) {
		
		ExportToXML.write(army, path);
		
	}

	@Override
	public Army importSomeUnitiesFromXML(String path) {
	    return XMLToArmy.read(path);
	}


}
