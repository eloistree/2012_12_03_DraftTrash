package vgqd.games.tuxdomination.IO;

import vgqd.games.tuxdomination.baseclass.Army;
import vgqd.games.tuxdomination.baseclass.Unity;

public interface WhatInOutHaveToDo {

	public  void exportSomeUnitiesToXML(Army army, String filename);
	public  Army importSomeUnitiesFromXML(String path);
}
