package vgqd.games.tuxdomination.IO.out;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import vgqd.games.tuxdomination.baseclass.Army;
import vgqd.games.tuxdomination.baseclass.Unity;

public class ExportToXML {

	static public  void write(Army army, String path) {
		writeOnFile(makeTheText(army),path);

	}
	static public void write(Unity[] unities, String path) {
		writeOnFile(makeTheText(unities),path);

	}
	static private void writeOnFile(String s, String path)
	{

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(path));
			out.write(s);
			out.close();
		} catch (IOException e) {
			System.out.println("Exception ");

		}
		
	}

	static public String makeTheText(Unity[] unities) {
		Army a = new Army();
		a.setName("MyTuxArmy");
		a.setUnities(unities);
		return makeTheText(a);

	}

	static public String makeTheText(Army army) {

		StringBuilder description = new StringBuilder();
		description.append("<?xml version=\"1.0\"?>\n");
		description.append("<unites armyname=\"" + army.getName() + "\">\n");
		for (Unity unity : army.getUnities()) {
			description.append("<unite numero=\""+Math.abs(unity.getName().hashCode())+"\" name=\"" + unity.getName()
					+ "\">\n");
			description.append("<attaque value=\"" + unity.getAttack() + "\">\n");
			description
					.append("<defence value=\"" + unity.getDefence() + "\">\n");
			description.append("<vie value=\"" + unity.getVie() + "\">\n");
			description.append("<range value=\"" + unity.getPortee() + "\">\n");
			description.append("<pointaction value=\"" + unity.getActionPoint()
					+ "\">\n");
			description.append("<alpha value=\"1\" />\n");
			description.append("</unite>\n");

		}

		description.append("</unites>\n");
		return description.toString();

	}

}
