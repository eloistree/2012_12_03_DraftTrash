package vgqd.games.tuxdomination.baseclass;

import java.awt.Image;

public class Unity {
	
	private String name;
	private double attack;
	private double defence;
	private double vie;
	private double portee;
	private double actionpoint;
	private Image image;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getAttack() {
		return attack;
	}
	public void setAttack(double attack) {
		this.attack = attack;
	}
	public double getDefence() {
		return defence;
	}
	public void setDefence(double defence) {
		this.defence = defence;
	}
	public double getVie() {
		return vie;
	}
	public void setVie(double vie) {
		this.vie = vie;
	}
	public double getPortee() {
		return portee;
	}
	public void setPortee(double portee) {
		this.portee = portee;
	}
	public double getActionPoint() {
		return actionpoint;
	}
	public void setActionPoint(double actionpoint) {
		this.actionpoint = actionpoint;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(actionpoint);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(attack);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(defence);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		temp = Double.doubleToLongBits(portee);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(vie);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unity other = (Unity) obj;
		if (Double.doubleToLongBits(actionpoint) != Double
				.doubleToLongBits(other.actionpoint))
			return false;
		if (Double.doubleToLongBits(attack) != Double
				.doubleToLongBits(other.attack))
			return false;
		if (Double.doubleToLongBits(defence) != Double
				.doubleToLongBits(other.defence))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(portee) != Double
				.doubleToLongBits(other.portee))
			return false;
		if (Double.doubleToLongBits(vie) != Double.doubleToLongBits(other.vie))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Unity [name=" + name + ", attack=" + attack + ", defence="
				+ defence + ", vie=" + vie + ", portee=" + portee
				+ ", actionpoint=" + actionpoint + ", image=" + image + "]";
	}
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	
	

}
