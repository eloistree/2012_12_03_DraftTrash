package vgqd.games.tuxdomination;


import vgqd.games.tuxdomination.IO.InOutManager;
import vgqd.games.tuxdomination.baseclass.Army;
import vgqd.games.tuxdomination.baseclass.Unity;
import vgqd.games.tuxdomination.builder.ArmyBuilder;
import vgqd.games.tuxdomination.observable.ArmyObserved;
import vgqd.games.tuxdomination.observable.UnityObserved;

public abstract class GestionnaireApp  implements WhatTheGestionnaireHaveToGiveAccess
{
	
	protected ArmyObserved armeeEnMemoire=null;
	protected UnityObserved pointedUnity =null;
	
	private InOutManager inout = new InOutManager();
	protected GestionnaireEyeesAndEars appObserver;
	
	

	public GestionnaireApp(Army armyToUse) {
		super();
		appObserver = new GestionnaireEyeesAndEars(this);
		
		
		if( armyToUse !=null)
			setPointedArmy(armyToUse);
		else setPointedArmy(ArmyBuilder.createArmy());
		
		System.out.println("" +this.armeeEnMemoire);
		for (Unity u : this.armeeEnMemoire.getUnities()) {
			System.out.println(MoneyManager.coutOfThis(u)+" $:"+ u+" => "+MoneyManager.incomeMoneyOfThis(u)+" $ ");
		}
		
		
		
		
	}
	public void start(){System.out.println("Start app");}


	public  void setPointedArmy(Army army)
	{
		this.armeeEnMemoire = new ArmyObserved(army);
		this.setPointedUnity( armeeEnMemoire.getUnityFocus());
		this.armeeEnMemoire.addObserver( appObserver);
		
	}
	
	public void setPointedUnity(Unity unity)
	{
		this.pointedUnity = new UnityObserved(unity);
		this.pointedUnity.addObserver( appObserver);
		
	}

	@Override
	public void exportArmy() {
		inout.exportSomeUnitiesToXML(armeeEnMemoire.getArmy(), "soldier.xml");
		
	}



	@Override
	public void importArmy() {
		Army a = inout.importSomeUnitiesFromXML("solider.xml");
		if(a!=null) armeeEnMemoire.setArmy(a);
		//else explication
	}
	
	
	public abstract void updateDescripteurs();
	
	
	@Override
	public void next() {
		armeeEnMemoire.next();
		setPointedUnity( armeeEnMemoire.getUnityFocus());
	}

	@Override
	public void previous() {
		armeeEnMemoire.previous();
		setPointedUnity( armeeEnMemoire.getUnityFocus());
	}
	
	
	

}
