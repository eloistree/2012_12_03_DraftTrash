package vgqd.games.tuxdomination.observable;

import java.awt.Image;
import java.util.Observable;

import vgqd.games.tuxdomination.baseclass.Unity;

public class UnityObserved extends Observable {

	private Unity unity;

	public UnityObserved(Unity unity) {
		this.unity=unity;
	}

	public String getName() {
		return unity.getName();
	}

	public void setName(String name) {
		unity.setName(name);
		modifNotif();
	}

	private void modifNotif() {
		setChanged();
		notifyAll();
		
	}

	public double getAttack() {
		return unity.getAttack();
		
	}

	public void setAttack(double attack) {
		unity.setAttack(attack);modifNotif();
	}

	public double getDefence() {
		return unity.getDefence();
	}

	public void setDefence(double defence) {
		unity.setDefence(defence);modifNotif();
	}

	public double getVie() {
		return unity.getVie();
	}

	public void setVie(double vie) {
		unity.setVie(vie);modifNotif();
	}

	public double getPortee() {
		return unity.getPortee();
	}

	public void setPortee(double portee) {
		unity.setPortee(portee);modifNotif();
	}

	public double getActionPoint() {
		return unity.getActionPoint();
	}

	public void setActionPoint(double actionpoint) {
		unity.setActionPoint(actionpoint);modifNotif();
	}

	public boolean equals(Object obj) {
		return unity.equals(obj);
	}

	public Image getImage() {
		return unity.getImage();
	}

	public void setImage(Image image) {
		unity.setImage(image);modifNotif();
	}

	public Unity getUnity() {
		return unity;
	}

	public void setUnity(Unity unity) {
		this.unity = unity;modifNotif();
	}
	
	
	
}
