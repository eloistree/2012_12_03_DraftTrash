package vgqd.games.tuxdomination.observable;

import java.util.Observable;

import vgqd.games.tuxdomination.baseclass.Army;
import vgqd.games.tuxdomination.baseclass.Unity;

public class ArmyObserved extends Observable {
	
	
	private Army army;
	private int cursor;

	public ArmyObserved(Army armee) {
		army =armee;
		cursor=0;
	}

	public Army getArmy() {
		return army;
	}

	public void setArmy(Army army) {
		this.army = army;modifNotif();
		cursor=0;
	}

	public String getName() {
		return army.getName();
	}

	public void setName(String name) {
		army.setName(name);modifNotif();
	}

	public Unity[] getUnities() {
		return army.getUnities();
	}

	public void setUnities(Unity[] unities) {
		army.setUnities(unities);modifNotif();
	}
	
	private void modifNotif() {
		setChanged();
		notifyAll();
		
	}

	public void next() {
		int length = army.getUnities().length;
		cursor++;
		cursor=cursor%length;
		
	}

	public void previous() {
		int length = army.getUnities().length;
		cursor--;
		if (cursor<0)cursor =length-1;
		
		
		
	}
	public Unity getUnityFocus(){return army.getUnities()[cursor];}
	

}
