package vgqd.games.tuxdomination.builder;

import java.util.ArrayList;

import vgqd.games.tuxdomination.baseclass.Army;
import vgqd.games.tuxdomination.baseclass.Unity;

public class ArmyBuilder {

	public static Army createRandomArmy() {
		ArrayList<Unity> tab = new ArrayList<>();

	
		tab.add(UnityBuilder.createUnity());
		for (int i = 0; i < 6; i++) {
			

			tab.add(UnityBuilder.createRandomUnity());
		}
		
		Army army = new Army();
		army.setName("Random army");
		army.setUnities(tab.toArray(new Unity[0]));
		return army;
	}

	public static Army createArmy() {	ArrayList<Unity> tab = new ArrayList<>();

	
	for (int i = 0; i < 7; i++) {
		tab.add(UnityBuilder.createUnity());
	}
	
	Army army = new Army();
	army.setName("Basic army");
	army.setUnities(tab.toArray(new Unity[0]));
	return army;
	}

	public static Army createEmptyArmy() {
		Army army = new Army();
		army.setName("Empty army");
		return army;
	}
	

}
