package vgqd.games.tuxdomination.builder;

import vgqd.games.tuxdomination.BoiteOutil;
import vgqd.games.tuxdomination.baseclass.Unity;

public class UnityBuilder {

	
	static public Unity createUnity()
	{
		Unity u = new Unity();
		u.setName("Default unity ");
		u.setVie(1);
		u.setDefence(0);
		u.setAttack(1);
		u.setActionPoint(5);
		u.setPortee(1);
		return u;
	}

	public static Unity createRandomUnity() {
		Unity u = new Unity();
		u.setName("A Soldiers ");
		u.setVie(BoiteOutil.randomNumber());
		u.setDefence(BoiteOutil.randomNumber());
		u.setAttack(BoiteOutil.randomNumber());
		u.setActionPoint(2+BoiteOutil.randomNumber());
		u.setPortee(BoiteOutil.randomNumber());
		return u;
	}
	
}
