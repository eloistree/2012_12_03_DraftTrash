package vgqd.games.tuxdomination;

public interface WhatTheGestionnaireHaveToGiveAccess {
	public abstract String getNameCurrent();
	public abstract void setNameCurrent(String name) ;
	public abstract  String getNameArmyCurrent();
	public abstract  void setNameArmyCurrent(String name);
	public abstract  double getAttackCurrent();
	public abstract  void setAttackCurrent(double attack);
	public abstract  double getDefenceCurrent();
	public abstract  void setDefenceCurrent(double defence);
	public abstract  double getVieCurrent() ;
	public abstract  void setVieCurrent(double vie);
	public abstract  double getPorteeCurrent();
	public abstract  void setPorteeCurrent(double portee);
	public abstract  double getActionPointCurrent();
	public abstract  void setActionPointCurrent(double actionpoint);
	
	
	public abstract  void next();
	public abstract  void previous();
	
	public abstract  void showXMLResult();
	public abstract  void showDescription();
	
	public void exportArmy();
	public void importArmy();
	
	
}
