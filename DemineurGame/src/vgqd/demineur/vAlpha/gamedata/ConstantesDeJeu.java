package vgqd.demineur.vAlpha.gamedata;

import java.awt.Dimension;
import java.util.Properties;

public class ConstantesDeJeu {

	/** Devra permettre de modifier les donn�es de constantes de jeu � partir d'une convention de propri�t�es*/
	static public void setWithThisValue(Properties p)
	{
		
	}
	
	/*Donn�es propres au jeu*/
	/**1 dommage par attack*/
	static private double attack=1.0;
	/**1 dommage amortis par attack*/
	static private double defence=1.0;
	/**100% de vitesse d'attack pour 1.0*/
	static private double speedattack=1.0;
	

	/*Donn�es propres au performance*/
	/**100% de vitesse d'attack pour 1.0*/
	static private double fps=40;
	/**Fullscreen ou non*/
	static private boolean fullscreen=false;
	
	/*Communication*/
	static public enum LANGAGE{francais,english};
	static private LANGAGE langage = LANGAGE.english;
	
	/**Resolution*/
	static private Dimension resolution=new Dimension(800, 600);
	static private Dimension tailleEcran=new Dimension(800, 600);
	
	
	
}
