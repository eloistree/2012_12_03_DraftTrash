package vgqd.demineur.vAlpha.gamelogic;


/**
 * Caracter
 * */
public interface ICarateristicsLink extends IDescripteur {

	/** retourne le nombre de d�gat � appliquer*/
	public double getDommages();
	
	/** retourne le nombre de d�gat absorber*/
	public double getDommagesReduction();
	
	/** retourne le nombre de vie qu'il rest*/
	public double getLife();
	
	/** retourne la vitesse de tire*/
	public double getSpeedAttack();
	
	/** retourne la vitesse de d�placement*/
	public double getSpeedMove();
	
}
