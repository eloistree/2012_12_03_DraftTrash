package vgqd.demineur.vAlpha.gamelogic.primaryclass;

public class Pourcentage {

	private int pourcentage;


	public Pourcentage() {
		super();
		this.pourcentage = 0;
	}
	public Pourcentage(int pourcentage) {
		super();
		setPourcentage(pourcentage);
	}

	public int getPourcentage() {
		return pourcentage;
	}

	public void setPourcentage(int value) {
		if (value<0) value=0;
		this.pourcentage = value;
	}
	
	public double getValueFor(double value)
	{
		return value* ((double) pourcentage)/100.0;
		
	}

}
