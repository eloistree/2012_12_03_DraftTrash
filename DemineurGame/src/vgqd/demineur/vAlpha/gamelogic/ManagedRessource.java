package vgqd.demineur.vAlpha.gamelogic;

import java.util.HashMap;

import vgqd.demineur.vAlpha.ressources.IRessource;


public class ManagedRessource implements IManagedRessource, IRessource{
	
	private IRessource pojoUsed;
	private HashMap<String,  IDescripteur> descripteurs= new HashMap<String, IDescripteur>();
/*
	private ITimeLink timeLinked;
	private IPhysiqueStateLink etatLinked
	private IIntelligenceLink  espritLinked
*/
	
	public void addDescripteur( IDescripteur descripteur)
	{
		descripteurs.put(descripteur.getClass().getSimpleName(), descripteur);
	}
	
	public IDescripteur getDescription(String classWantedName)
	{
		
		return descripteurs.get(classWantedName);
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	
	@Override
	public String getDescription() {
		
		return toString();
	}

	@Override
	public String getClassDescription() {
		// TODO Auto-generated method stub
		return "Use to linked additionnals ressources to a POJO ressource.";
	}

	@Override
	public String toString() {
		return "ManagedRessource [pojoUsed=" + pojoUsed.getDescription() + ", descripteurs="
				+ descripteurs.size() + "]";
	}
	
	
}

