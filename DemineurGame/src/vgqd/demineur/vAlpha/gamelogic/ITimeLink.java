package vgqd.demineur.vAlpha.gamelogic;

/**
 * Classe qui � pour but de lier l'objet manager 
 * au temps passer dans le jeu
 * */
public interface ITimeLink  extends IDescripteur {

	/** retourne le nombre de temps passer en jeu en ms */
	public int getLifeTime();
	/** retourne le nombre de temps depuis sa cr�ationen ms*/
	public int getTimeWhenCreated();
}
