/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.donnees;

import java.util.Observable;

/**
 *
 * @author java06
 */
public class ModèlePartie extends Observable{
  
    
   private boolean gameOver = false;
   private boolean gameWin = false;
   private int timeAvantEnnemiesArrivesEnS;
   private int timeAvantAlliésArrivesEnS;
   private int tempsDepuisDebutEnS=0;

   {
   initialiserLaPartie();
   }
   
   public void initialiserLaPartie()
   {
   gameOver= false;
   gameWin=false;
   timeAvantEnnemiesArrivesEnS= 10;
   timeAvantAlliésArrivesEnS=30;
   tempsDepuisDebutEnS=0;
   
   }
   
public void incTempsDepuisDebutEnS(){tempsDepuisDebutEnS++;}
    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }
   public boolean isGameWin() {
        return gameWin;
    }

    public void setGameWin(boolean gameWin) {
        this.gameWin = gameWin;
    }
   
    
     public void notifierAuxObserveurs()
    {
    setChanged();
    notifyObservers();
    
    
    }

    public int getTempsDepuisDebutEnS() {
        return tempsDepuisDebutEnS;
    }

    public void setTempsDepuisDebutEnS(int tempsDepuisDebutEnS) {
        this.tempsDepuisDebutEnS = tempsDepuisDebutEnS;
    }

    public int getTimeAvantAlliésArrivesEnS() {
        return timeAvantAlliésArrivesEnS;
    }

    public void setTimeAvantAlliésArrivesEnS(int timeAvantAlliésArrivesEnS) {
        this.timeAvantAlliésArrivesEnS = timeAvantAlliésArrivesEnS;
    }

    public int getTimeAvantEnnemiesArrivesEnS() {
        return timeAvantEnnemiesArrivesEnS;
    }

    public void setTimeAvantEnnemiesArrivesEnS(int timeAvantEnnemiesArrivesEnS) {
        this.timeAvantEnnemiesArrivesEnS = timeAvantEnnemiesArrivesEnS;
    }
     
    
}
