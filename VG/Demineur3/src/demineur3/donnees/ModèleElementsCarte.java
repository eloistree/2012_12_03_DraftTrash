/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.donnees;

import demineur3.controleur.CONSTANTDUJEU;
import demineur3.modeleDeDonnee.BombreARetardement;
import demineur3.modeleDeDonnee.Drapeau;
import demineur3.modeleDeDonnee.Explosif;
import demineur3.modeleDeDonnee.Mine;
import demineur3.outils.BoitAOutil;
import java.awt.Image;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author java06
 */
public class ModèleElementsCarte extends Observable {

    private Image imageDeCarte;
    private int width = CONSTANTDUJEU.ECRANJEULARGEURPARDEFAUT;
    private int height = CONSTANTDUJEU.ECRANJEUHAUTEURPARDEFAUT;
    //private HashMap<MyPoint,Element> elements;
    private int[][] grilles = new int[CONSTANTDUJEU.ECRANJEULARGEURPARDEFAUT][CONSTANTDUJEU.ECRANJEUHAUTEURPARDEFAUT];
    private ArrayList<Drapeau> flags;
    private ArrayList<Mine> mines;
    //private ArrayList<C4> flags;
    //private ArrayList<BombreARetardement> flags;

    {
        flags = new ArrayList<Drapeau>();
        mines = new ArrayList<Mine>();
        imageDeCarte = new javax.swing.ImageIcon(getClass().getResource("/demineur3/affichage/ressources/carte.jpg")).getImage();


    }

    public Image getImageDeCarte() {
        return imageDeCarte;
    }

    public void setImageDeCarte(Image imageDeCarte) {
        this.imageDeCarte = imageDeCarte;
    }

    public void createFlagHere(Point position) {

        flags.add(new Drapeau(position));
    }

    public ArrayList<Drapeau> getFlags() {
        return flags;
    }

    public ArrayList<Mine> getMines() {
        return mines;
    }

    public void createBombePlace(int mine, int c4, int bombe) {
        for (int i = 0; i < mine; i++) {

            createMine(new Point(
                    BoitAOutil.nombreAléatoireEntreA_B(0, width),
                    BoitAOutil.nombreAléatoireEntreA_B(0, height)));
        }
    }

    public void createMine(Point position) {
        mines.add(new Mine(position));
    }

    public ArrayList<Mine> getAllMinesNext(Point p) {

        ArrayList<Mine> mineProximiter = new ArrayList<Mine>();
        if (p != null) {
            for (Mine tmp : mines) {
                int dist = BoitAOutil.distanceEntreDeuxPoints(p, tmp.getPosition());
                if (dist <= tmp.getPorteeOnde()) {
                    mineProximiter.add(tmp);
                }
            }
        }
        return mineProximiter;

    }
     public ArrayList<Mine> getAllMinesOn(Point p) {

        ArrayList<Mine> mineProximiter = new ArrayList<Mine>();
        if (p != null) {
            for (Mine tmp : mines) {
                int dist = BoitAOutil.distanceEntreDeuxPoints(p, tmp.getPosition());
                if (dist <= tmp.getPorteeActivation()) {
                    mineProximiter.add(tmp);
                }
            }
        }
        return mineProximiter;

    }
     public Mine getFirstMineOn(Point p) {
      
    
            for (Mine tmp : mines) {
                int dist = BoitAOutil.distanceEntreDeuxPoints(p, tmp.getPosition());
                if (dist <= tmp.getPorteeActivation()) {
                    return tmp;
                }
            }
        return null;
        
    }

    public ArrayList<Mine> getMinesHowExploded() {
        
         ArrayList<Mine> minesExploded = new ArrayList<Mine>();
            for (Mine tmp : mines) {
                
                if (tmp.isExplosed()) {
                    minesExploded.add(tmp);
                }
            }
        
        return minesExploded;
    }

    public ArrayList<Mine> getMinesActivated() {
        
         ArrayList<Mine> minesActivated = new ArrayList<Mine>();
            for (Mine tmp : mines) {
                
                if (tmp.isActiver()) {
                    minesActivated.add(tmp);
                }
            }
        
        return minesActivated;
        
    }
    public ArrayList<Mine> getMinesDesarm() {
        
         ArrayList<Mine> minesActivated = new ArrayList<Mine>();
            for (Mine tmp : mines) {
                
                if (tmp.isDeminer()) {
                    minesActivated.add(tmp);
                }
            }
        
        return minesActivated;
        
    }

    public int getNombreRestant()
    {   int i=0;
        for (Mine tmp : mines) {
                
                if (!tmp.isDeminer()) {
                   i++;
                }
            }
    return i;
    }
    
    public int getNombreMines()
    {
    return mines.size();
    
    }
    public boolean isAllMinesDesarm() {
       boolean allDesarm = true;
        for (Mine mine : mines) {
            if (!mine.isDeminer()) allDesarm=false;
            
        }
    return allDesarm;
    }

    public void notifierAuxObserveurs()
    {
    setChanged();
    notifyObservers();
    
    
    }
    

    
}
