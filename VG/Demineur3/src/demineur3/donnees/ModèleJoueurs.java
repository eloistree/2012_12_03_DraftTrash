/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.donnees;

import demineur3.modeleDeDonnee.Joueur;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Observable;

/**
 *
 * @author java06
 */
public class ModèleJoueurs extends Observable{
    
    private Joueur joueur =null;
    private Joueur[] autrejoueurs =null;

    public ModèleJoueurs() {
        
        joueur = new Joueur("Benoit le vile", new Point(1, 1), new Dimension(10, 18),
                new javax.swing.ImageIcon(getClass().getResource("/demineur3/affichage/ressources/tuxmilitaire.jpg")).getImage());
    }

    
    
    public Joueur[] getAutrejoueurs() {
        return autrejoueurs;
    }

    public void setAutrejoueurs(Joueur[] autrejoueurs) {
        this.autrejoueurs = autrejoueurs;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }
    
    
    
    public void notifierAuxObserveurs()
    {
    setChanged();
    notifyObservers();
    
    
    }
    
    
}
