/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.modeleDeDonnee;

import demineur3.outils.BoitAOutil;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;

/**
 *
 * @author java06
 */
public  abstract class Explosif extends Element {
    
    private static int nombreBombe=0;
    protected int timerBombeEnSec;
    protected int tempsRestant;
    protected boolean deminer;
    protected boolean activer;
    protected Image imageParDefaulExplosed=null;
            
            {
            imageParDefaulExplosed = new javax.swing.ImageIcon(getClass().getResource("/demineur3/affichage/ressources/explosion.png")).getImage();
            }

     public Explosif(String nom, Point position,Dimension dimension, Image image, int timer) {
        super( nom,  position,dimension,  image);
        timerBombeEnSec =timer;
        tempsRestant =timer;
        deminer=false;
        activer=false;
        
    }
     public Explosif(Point position) {
         super(position);
        this.nom = "Explosif"+nombreBombe;
        this.position = position;
        this.dimension = new Dimension(0, 0);
        this.imageParDefaul = null;
        timerBombeEnSec = BoitAOutil.nombreAléatoireEntreA_B(15, 25);
        tempsRestant =timerBombeEnSec;
    }
    
    public Image getImageParDefaulExplosed() {
        return imageParDefaulExplosed;
    }

    public boolean isActiver() {
        return activer;
    }

    public void setActiver(boolean activer) {
        this.activer = activer;
    }

    

    public boolean isDeminer() {
        return deminer;
    }

    public void setDeminer(boolean deminer) {
        this.deminer = deminer;
    }

    public int getTempsRestantEnSec() {
        return tempsRestant;
    }

    public void setTempsRestantEnSec(int decrementeurEnSec) {
        this.tempsRestant = decrementeurEnSec;
    }

    
    public void decTempsRestantEnSec(int decrementeurEnSec) {
     
        if (!deminer && tempsRestant>0)
        this.tempsRestant--;
      
    }

    public int getTimerBombeEnSec() {
        return timerBombeEnSec;
    }

    public void setTimerBombeEnSec(int timerBombeEnSec) {
        this.timerBombeEnSec = timerBombeEnSec;
    }
    public boolean isExplosed()
    {
        if (!deminer && tempsRestant<=0)
            return true;
        return false;
    }
    public void EXPLOooooode()
    {
    activer=true;
    deminer=false;
    tempsRestant=1;
    
    }

    
    
}
