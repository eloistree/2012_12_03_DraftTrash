/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.modeleDeDonnee;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;

/**
 *
 * @author java06
 */
public class Personnage extends Element {
        
    private Point destination =null;
    
       public Personnage(String nom, Point position,Dimension dimension, Image image) {
        super( nom,  position,dimension,  image);
       
    }

    public Point getDestination() {
        return destination;
    }

    public void setDestination(Point destination) {
        this.destination = destination;
    }
       
       
    
}
