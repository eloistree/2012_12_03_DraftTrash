/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.modeleDeDonnee;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;

/**
 *
 * @author java06
 */
public abstract class Element {

    private static int nombreElement=0;
    protected String nom = "";
    protected Point position = null;
    protected Image imageParDefaul = null;
    
    protected  Dimension dimension=null;
    protected boolean immatériel=false;
    
    {nombreElement++;}

    public Element(String nom, Point position,Dimension dimension, Image image) {
        this.nom = nom;
        this.position = position;
        this.dimension = dimension;
        this.imageParDefaul = image;
    }
     public Element(Point position) {
        this.nom = "Element"+nombreElement;
        this.position = position;
        this.dimension = new Dimension(0, 0);
        this.imageParDefaul = null;
    }


    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }
    
    

    public Image getImageParDefaul() {
        return imageParDefaul;
    }

    public void setImageParDefaul(Image imageParDefaul) {
        this.imageParDefaul = imageParDefaul;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public boolean isImmatériel() {
        return immatériel;
    }

    public void setImmatériel(boolean immatériel) {
        this.immatériel = immatériel;
    }
    
    
}
