/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.affichage;

import demineur3.donnees.ModèleElementsCarte;
import demineur3.donnees.ModèleInterface;
import demineur3.donnees.ModèlePartie;
import java.awt.Color;
import java.awt.Image;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.netbeans.lib.awtextra.*;

/**
 *
 * @author christine
 */
public class FenetrePrincipal extends JFrame implements Observer  {
    private Ecran2D ecranJeu = new Ecran2D();
    private Menu menu = new Menu();

    public FenetrePrincipal(String title, int tailleX, int tailleY) {

        this(title, tailleX, tailleY, 0, 0);


    }

    public FenetrePrincipal(String title, int largeurFenetreP, int hauteurFenetreP, int xPositionFenetreP, int yPositionFenetreP) {
        super(title);
        initialiserFenetre(largeurFenetreP,hauteurFenetreP);
      
       
  
    }

    private void initialiserFenetre(int width, int height ) {
        
        this.setSize(width + 200, height);
        this.setLocation(50 , 0);
        
        this.setResizable(false);
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        setLayout( new org.netbeans.lib.awtextra.AbsoluteLayout());
        ecranJeu = new Ecran2D();
        ecranJeu.setSize(width, height);
        add(ecranJeu, new AbsoluteConstraints(0, 0, width, height));
        menu = new Menu();
        menu.setSize(200, height);
        add(menu, new AbsoluteConstraints(width, 0, 200, height));




    }

    public JPanel getEcranJeu() {
        return ecranJeu;
    }

    public Object getMenuGame() {
        return menu;
    }

    public void setAffichage2D(Image img) {
        ecranJeu.paintThis(img);
        menu.setMiniEcran(img);
    }

    public void setKeysOnOff(boolean keyBoardUp, boolean keyBoardLeft, boolean keyBoardDown, boolean keyBoardRight) {
        int a = 0;
        if (keyBoardUp) {
            a = 1;
        } else {
            a = -1;
        }
        menu.setArrowOnOff(0, a);
        
        
        a = 0;
        if (keyBoardLeft) {
            a = 1;
        } else {
            a = -1;
        }

        menu.setArrowOnOff(1, a);
        
        
        a = 0;
        if (keyBoardDown) {
            a = 1;
        } else {
            a = -1;
        }
        menu.setArrowOnOff(2, a);
        
        
        a = 0;
        if (keyBoardRight) {
            a = 1;
        } else {
            a = -1;
        }
        menu.setArrowOnOff(3, a);
        
        

    }

    @Override
    public void update(Observable o, Object arg) {
        
        if(o instanceof ModèleElementsCarte)
        {
            ModèleElementsCarte carte = (ModèleElementsCarte) o;
            menu.setMines(carte.getNombreRestant(),carte.getNombreMines());
        }
        else if(o instanceof ModèlePartie)
        {
            ModèlePartie partie = (ModèlePartie) o;
            
            int temps = partie.getTempsDepuisDebutEnS();
            int tEnnemis =partie.getTimeAvantEnnemiesArrivesEnS();
            int tAllies=partie.getTimeAvantAlliésArrivesEnS();
            if(temps>tAllies)menu.setTime(temps+"("+tEnnemis+","+tAllies+")",Color.red);
            else if(temps>tEnnemis)menu.setTime(temps+"("+tEnnemis+","+tAllies+")",Color.ORANGE);
            else menu.setTime(temps+"("+tEnnemis+","+tAllies+")",Color.green);
        }
        else if(o instanceof ModèleInterface)
        {
            ModèleInterface materiel = (ModèleInterface) o;
            if(materiel.isKeyBoardUp()) menu.setArrowOnOff(0, 1);
            else menu.setArrowOnOff(0, 0);
            
            if(materiel.isKeyBoardLeft()) menu.setArrowOnOff(1, 1);
            else menu.setArrowOnOff(1, 0);
            
            if(materiel.isKeyBoardDown()) menu.setArrowOnOff(2, 1);
            else menu.setArrowOnOff(2, 0);
            
            if(materiel.isKeyBoardRight()) menu.setArrowOnOff(3, 1);
            else menu.setArrowOnOff(3, 0);
            
            
        }
        
        
    }
}
