/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.outils;

import demineur3.modeleDeDonnee.Drapeau;
import demineur3.modeleDeDonnee.Joueur;
import demineur3.modeleDeDonnee.Mine;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author christine
 */
public class Draw {

    public static void drawThisIn(Graphics g, Mine mine, boolean avecOnde, int temps) {
        temps = temps * 2;
        g.setColor(Color.lightGray);
        int onde = mine.getDiametrePorteeOnde();

        int ondeActivation = mine.getDiametrePorteeActivation();
        int x;
        int dimMax = mine.getDimension().height;
        if (dimMax < mine.getDimension().width) {
            dimMax = mine.getDimension().width;
        }

        if (!mine.isDeminer()) {
            for (int i = 1; i <= 3; i++) {

                x = (temps + (onde / i)) % onde;
                if (ondeActivation < x) {
                    g.setColor(Color.lightGray);
                } else {
                    g.setColor(Color.red);
                }
                g.drawOval(mine.getPosition().x - (x / 2), mine.getPosition().y - (x / 2), x, x);
            }
        }
        g.setColor(Color.lightGray);


        g.drawOval(mine.getPosition().x - (onde / 2), mine.getPosition().y - (onde / 2), onde, onde);

        if (!mine.isExplosed()) {
            g.drawImage(mine.getImageParDefaul(), mine.getPosition().x - mine.getDimension().width / 2, mine.getPosition().y - mine.getDimension().height / 2,
                    mine.getDimension().width,
                    mine.getDimension().height, null);
            if (mine.isDeminer()) {
                Draw.drawACrossHere(g, mine.getPosition(), mine.getDimension(), Color.red);
            }
        } else {
            g.drawImage(mine.getImageParDefaulExplosed(), mine.getPosition().x - mine.getDimension().width / 2, mine.getPosition().y - mine.getDimension().height / 2,
                    mine.getDimension().width,
                    mine.getDimension().height, null);
        }

        if (mine.isActiver() && !mine.isDeminer()) {
            Draw.drawThisIn(g, "" + mine.getTempsRestantEnSec(),
                    new Point(mine.getPosition().x - (mine.getDimension().width / 2),
                    mine.getPosition().y - (mine.getDimension().height / 2) - 5),
                    Color.red);
        }



    }

    public static void drawThisIn(Graphics g, String text, Point p, Color color) {
        g.setColor(color);
        g.drawString(text, p.x, p.y);

    }

    public static void drawThisIn(Graphics g, Drapeau drapeau) {
        if (g != null && drapeau != null && drapeau.getImageParDefaul() != null && drapeau.getPosition() != null && drapeau.getDimension() != null) {
            g.drawImage(drapeau.getImageParDefaul(), drapeau.getPosition().x, drapeau.getPosition().y - drapeau.getDimension().height,
                    drapeau.getDimension().width,
                    drapeau.getDimension().height, null);
        }


    }

    public static void drawThisIn(Graphics g, Joueur joueur) {
        if (g != null && joueur != null && joueur.getImageParDefaul() != null && joueur.getPosition() != null && joueur.getDimension() != null) {
            g.drawImage(
                    joueur.getImageParDefaul(),
                    joueur.getPosition().x - joueur.getDimension().width / 2,
                    joueur.getPosition().y - joueur.getDimension().height / 2,
                    joueur.getDimension().width,
                    joueur.getDimension().height, null);
        }

    }

    public static void drawThisIn(Graphics g, ArrayList<Drapeau> chemin) {
        if (g != null && chemin != null && chemin.size() > 2) {
            g.setColor(Color.yellow);
            Point p1, p2;
            int i = 0;
            p2 = chemin.get(i).getPosition();
            drawThisIn(g, p2, false);
            for (Drapeau drap : chemin) {
                p1 = p2;
                p2 = chemin.get(i).getPosition();
                drawThisIn(g, p1, p2);
                i++;
            }

            drawThisIn(g, p2, true);

        }
    }

    private static void drawThisIn(Graphics g, Point p1, boolean vide) {

        if (vide) {
            g.drawOval(p1.x - (10 / 2), p1.y - (10 / 2), 10, 10);
        } else {
            g.fillOval(p1.x - (10 / 2), p1.y - (10 / 2), 10, 10);
        }

    }

    private static void drawThisIn(Graphics g, Point p1, Point p2) {
        g.drawLine(p1.x, p1.y, p2.x, p2.y);
    }

    public static void drawThoseCursors(Graphics g, Point ptBut1, Point ptBut3, Point ptCurseur) {
        g.setColor(Color.green);

        if (ptBut1 != null) {
            g.setColor(Color.red);
            g.drawOval(ptBut1.x - 5, ptBut1.y - 5, 11, 11);

            g.setColor(Color.green);
        }

        if (ptBut3 != null) {
            g.drawOval(ptBut3.x - 5, ptBut3.y - 5, 11, 11);
        }

        if (ptCurseur != null) {
            g.fillOval(ptCurseur.x - 2, ptCurseur.y - 2, 5, 5);
        }

        if (ptBut3 != null && ptBut1 != null) {
            g.drawLine(ptBut1.x, ptBut1.y, ptBut3.x, ptBut3.y);
        } else if (ptBut3 != null && ptCurseur != null) {
            g.drawLine(ptBut3.x, ptBut3.y, ptCurseur.x, ptCurseur.y);
        }
    }

    private static void drawACrossHere(Graphics g, Point p, Dimension d, Color couleur) {
        g.setColor(couleur);
        g.drawLine(p.x - (d.width / 2), p.y - (d.width / 2), p.x + (d.width / 2), p.y + (d.width / 2));
        g.drawLine(p.x + (d.width / 2), p.y + (d.width / 2), p.x - (d.width / 2), p.y - (d.width / 2));
    }
}
