/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.outils;

import java.awt.Point;
import java.util.Random;

/**
 *
 * @author java06
 */
public class BoitAOutil {
      static public int nombreAléatoireEntreA_B(int aMin,int bMax)
    {
        Random r = new Random();
        return aMin + r.nextInt(bMax - aMin);
       
    }
      
      static public int distanceEntreDeuxPoints( Point p1, Point p2)  
      {     
          int y=0, x=0;
              if ( p1.x>p2.x) x=p1.x-p2.x;
              else  x=p2.x-p1.x;
              
              if ( p1.y>p2.y) y=p1.y-p2.y;
              else  y=p2.y-p1.y;
              
         
        return (int) Math.sqrt((double) ((x*x) + (y*y)) );
      
      }
}
