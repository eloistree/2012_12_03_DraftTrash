/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur.Gestionnaire;

import demineur3.donnees.ModèleElementsCarte;
import demineur3.donnees.ModèleInterface;
import demineur3.donnees.ModèleJoueurs;
import demineur3.donnees.ModèlePartie;
import demineur3.modeleDeDonnee.Drapeau;
import demineur3.modeleDeDonnee.Mine;
import demineur3.outils.Draw;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 *
 * @author christine
 */
public class GestionnaireAffichage2D {

    private BufferedImage imgTemp = null;
    private int i = 0;

    public Image getImageOf(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, ModèlePartie niveaux, int x, int y, int largueur, int hauteur) {
        i++;
        imgTemp = new BufferedImage(largueur, hauteur, BufferedImage.TYPE_INT_RGB);

        Graphics2D g = (Graphics2D) imgTemp.getGraphics();
        //g.setColor(Color.black);
        //g.fillRect(0, 0, largueur, hauteur);

        g.drawImage(carte.getImageDeCarte(), 0, 0, largueur, hauteur, null);

        for (Mine mine : carte.getAllMinesNext(joueurs.getJoueur().getPosition())) {
            Draw.drawThisIn(g, mine, true, i);
        }
        for (Mine mine : carte.getMinesHowExploded()) {
            Draw.drawThisIn(g, mine, true, i);
        }
        for (Mine mine : carte.getMinesDesarm()) {
            Draw.drawThisIn(g, mine, true, i);
        }
        Draw.drawThisIn(g, carte.getFlags());
        for (Drapeau drap : carte.getFlags()) {
            Draw.drawThisIn(g, drap);
        }


        Draw.drawThoseCursors(g, materiel.getMousePositionBUTTON1Pressed(), materiel.getMousePositionBUTTON3Pressed(), materiel.getMousePositionActuelle());




        Draw.drawThisIn(g, joueurs.getJoueur());






        g.setColor(Color.green);
        g.drawLine(0, i % hauteur, largueur, i % hauteur);






        if (niveaux.isGameOver()) {
            
            g.setColor(Color.red);
            g.setFont(new Font("Arial", Font.BOLD, 40));
            g.drawString("GAME OVER", imgTemp.getWidth()/3, imgTemp.getHeight()/3);
        }
        else if (niveaux.isGameWin())
        {
            g.setColor(Color.green);
            g.setFont(new Font("Arial", Font.BOLD, 40));
            g.drawString("YOU WIN", imgTemp.getWidth()/3, imgTemp.getHeight()/3);
        
        }










        return imgTemp;
    }
}
