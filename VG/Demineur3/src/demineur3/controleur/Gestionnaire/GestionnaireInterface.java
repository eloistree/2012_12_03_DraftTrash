/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur.Gestionnaire;

import demineur3.affichage.Console;
import demineur3.affichage.Ecran2D;
import demineur3.affichage.FenetrePrincipal;
import demineur3.controleur.CONSTANTDUJEU;
import demineur3.controleur.Controleur;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 * @author christine
 */
public class GestionnaireInterface implements MouseMotionListener, MouseListener, KeyListener, ActionListener {
    
    public void println(String string) {
        console.sendMessage(string);
        System.out.println(string);
    }
    private FenetrePrincipal affichage;
    private int largeurFenetreP = 800;
    private int hauteurFenetreP = 600;
    private int xPositionFenetreP = 100;
    private int yPositionFenetreP = 100;
    private Console console;
    private Controleur controleur;
    
    {
        
        affichage = new FenetrePrincipal("Demineur 3", largeurFenetreP, hauteurFenetreP, xPositionFenetreP + 100, yPositionFenetreP);
        console = new Console("Console de Demineur 3", largeurFenetreP, hauteurFenetreP, xPositionFenetreP + largeurFenetreP + 200, yPositionFenetreP);
        
        //affichage.addMouseListener(this);
        //affichage.addMouseMotionListener(this);
        affichage.getEcranJeu().addMouseListener(this);
        affichage.getEcranJeu().addMouseMotionListener(this);
        affichage.addKeyListener(this);
        console.addKeyListener(this);
        
        affichage.setVisible(true);
        
        
    }
    
    public GestionnaireInterface(Controleur aThis) {
        controleur = aThis;
        
        
        
        
    }

    public void setAffichage2D(Image img) {
        affichage.setAffichage2D(img);
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.getSource() instanceof Ecran2D) {
            controleur.recoitSignalisationDe("SIGNALISATION:PLAYERMOUSEMOVED:" + e.getX() + "," + e.getY());
        }
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        if (e.getSource() instanceof Ecran2D) {
            controleur.recoitSignalisationDe("SIGNALISATION:PLAYERMOUSEMOVED:" + e.getX() + "," + e.getY());
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getSource() instanceof Ecran2D) {
            if (e.getButton() == MouseEvent.BUTTON3) {
                controleur.recoitSignalisationDe("SIGNALISATION:ACTIONCLICKDROITPRESSED:" + e.getX() + "," + e.getY());
            } else if (e.getButton() == MouseEvent.BUTTON1) {
                controleur.recoitSignalisationDe("SIGNALISATION:ACTIONCLICKPRESSED:" + e.getX() + "," + e.getY());
            }
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        
        if (e.getSource() instanceof Ecran2D) {
            if (e.getButton() == MouseEvent.BUTTON3) {
                controleur.recoitSignalisationDe("SIGNALISATION:ACTIONCLICKDROITRELEASED:" + e.getX() + "," + e.getY());
            } else if (e.getButton() == MouseEvent.BUTTON1) {
                controleur.recoitSignalisationDe("SIGNALISATION:ACTIONCLICKRELEASED:" + e.getX() + "," + e.getY());
            }
        }
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
        
        
        
        
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_C) {
            console.setVisible(!console.isVisible());
            
        }
        
        controleur.recoitSignalisationDe("SIGNALISATION:ACTIONKEYBOARDPRESSED:" + e.getKeyChar());
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        
        controleur.recoitSignalisationDe("SIGNALISATION:ACTIONKEYBOARDRELEASED:" + e.getKeyChar());
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    }
    
    public void setKeysOnOff(boolean keyBoardUp, boolean keyBoardLeft, boolean keyBoardDown, boolean keyBoardRight) {
        
        affichage.setKeysOnOff(keyBoardUp, keyBoardLeft, keyBoardDown, keyBoardRight);
    }
    
    public void setDimension(int width, int height) {
        affichage.setSize(width, height);
        affichage.revalidate();
    }

    public int getHauteurFenetreP() {
        return hauteurFenetreP;
    }

    public void setHauteurFenetreP(int hauteurFenetreP) {
        this.hauteurFenetreP = hauteurFenetreP;
    }

    public int getLargeurFenetreP() {
        return largeurFenetreP;
    }

    public void setLargeurFenetreP(int largeurFenetreP) {
        this.largeurFenetreP = largeurFenetreP;
    }

    public FenetrePrincipal getAffichage() {
        return affichage;
    }
    
    
    
    
}
