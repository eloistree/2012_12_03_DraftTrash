/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur.Gestionnaire;

import demineur3.controleur.Controleur;
import demineur3.donnees.ModèleInterface;
import java.awt.Point;

/**
 *
 * @author christine
 */
public class TraitementActionSouris {

    private ModèleInterface data;

    public TraitementActionSouris( ModèleInterface data) {
        this.data = data;
    }

    public void processThisMove(int x, int y) {
        processThisMove(new Point(x, y));

    }

    public void processThisMove(Point pt) {
        if (!(data.isMouseBUTTON1() && data.isMouseBUTTON3())) {
            data.setMousePositionActuelle(pt);
            
        }
    }

    public void processThisClick(int x, int y, int buttonNumber, boolean isPressed) {
        processThisClick(new Point(x, y), buttonNumber, isPressed);

    }

    public void processThisClick(Point pt, int buttonNumber, boolean isPressed) {

        // si j'active le but 3 je peux activer le but 1



        if (buttonNumber == 3) {
            data.setMouseBUTTON3(isPressed);
            if (isPressed) {
                data.setMousePositionBUTTON3Pressed(pt);
            } else {
                data.setMousePositionActuelle(data.getMousePositionBUTTON3Pressed());
                data.setMousePositionBUTTON3Pressed(null);
                data.setMouseBUTTON3(isPressed);
                data.setMousePositionBUTTON1Pressed(null);
                

            }

        }
        if (buttonNumber == 1 && data.isMouseBUTTON3()) {

            data.setMouseBUTTON1(isPressed);
            if (isPressed) {
                data.setMousePositionBUTTON1Pressed(pt);
                
              
            } else {
                data.setMousePositionBUTTON1Pressed(null);
            }
        }


    }
}
