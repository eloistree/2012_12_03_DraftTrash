/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur.Gestionnaire;

import demineur3.controleur.CONSTANTDUJEU;
import demineur3.controleur.Controleur;
import demineur3.time.Temps;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author christine
 */
public class GestionnaireDeTemps extends Gestionnaire implements ActionListener {

    private Controleur controleur;
    private Temps timer;
    private int frame;
    private int seconde;

    {
        frame = 0;

    }

    public GestionnaireDeTemps(Controleur aThis) {
        controleur = aThis;
        timer = new Temps();
        timer.ajouterUnEcouteur(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        frame++;
        //definir par après
        boolean conditionRequise = true;
        conditionRequise = frame % CONSTANTDUJEU.FRAMEPERSECONDEOFTHEGAME == 1;
        controleur.recoitOrdreDeFaire("ORDER:UPDATEAFFICHAGE");
        
        if (conditionRequise) {
            seconde++;
            controleur.recoitSignalisationDe("SIGNALISATION:UNESECONDEDEPASSE:" + frame/CONSTANTDUJEU.FRAMEPERSECONDEOFTHEGAME);
        }


    }

    public void pause(){ timer.pause();}
    
    public void play(){ timer.play();}
    
    public int getFrame() {
        return frame;
    }

    public void setFrame(int frame) {
        this.frame = frame;
    }

    public int getSeconde() {
        return seconde;
    }

    public void setSeconde(int seconde) {
        this.seconde = seconde;
    }
    
    public int getFramePerSeconde() {
        return frame/seconde;
    }

    
    
    
    
    
}
