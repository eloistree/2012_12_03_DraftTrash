/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur.Gestionnaire;

import demineur3.controleur.CONSTANTDUJEU;
import demineur3.donnees.ModèleInterface;

/**
 *Cette classe fait la liaison entre le signal émit au controleur et le traitement qui lui est associer pour stocker la valeur de ce signale dans les données principales du jeu.
 * Par exemple:
 * Dans Controleur => SIGNALISATION:ACTIONKEYBOARDPRESSEDz:z
 * Dans traitement => la touche "z" est associer à up,=> demande à data d'enregistrer que la touche vers le haut à été enfoncée 
 * Dans Data => la valeur monter est à true maintenant
 * Plus tard, d'autre classe pourrons maintenant savoir que l'utilisateur veut aller en haut à partir de data et grace à traitementactionclavier
 * 
 * @author christine
 */
public class TraitementActionClavier {

    private ModèleInterface data;
    
    public TraitementActionClavier(ModèleInterface data) {
        this.data=data;
    }

    public void processThisKey(String variable, boolean isPressed ) {

        if (variable.equals(CONSTANTDUJEU.KEYUP)) {
            data.setKeyBoardUp(isPressed);
            
        }
        else  if (variable.equals(CONSTANTDUJEU.KEYLEFT)) {
            data.setKeyBoardLeft(isPressed);
        }
        else  if (variable.equals(CONSTANTDUJEU.KEYDOWN)) {
            data.setKeyBoardDown(isPressed);
        }
        else  if (variable.equals(CONSTANTDUJEU.KEYRIGHT)) {
            data.setKeyBoardRight(isPressed);
        }
        
    }
    
    
    
    
    
}
