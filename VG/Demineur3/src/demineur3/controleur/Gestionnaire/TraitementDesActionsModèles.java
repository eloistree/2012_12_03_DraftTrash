/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur.Gestionnaire;

import demineur3.controleur.Controleur;
import demineur3.donnees.ModèleElementsCarte;
import demineur3.donnees.ModèleInterface;
import demineur3.donnees.ModèleJoueurs;
import demineur3.donnees.ModèlePartie;
import demineur3.modeleDeDonnee.Explosif;
import demineur3.modeleDeDonnee.Joueur;
import demineur3.modeleDeDonnee.Mine;
import demineur3.outils.BoitAOutil;
import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author christine
 */
public class TraitementDesActionsModèles {

  

    public void initialiser(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, ModèlePartie niveaux, int frameSinceStart) {

        carte.createBombePlace(10, 1, 2);

    }

    public void update(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, ModèlePartie niveaux, int frameSinceStart) {
        checkGameOver(materiel, carte, joueurs, niveaux, frameSinceStart);
        checkGameWin(materiel, carte, joueurs, niveaux, frameSinceStart);
        if (niveaux.isGameOver()) {
            putInterfaceToGameOver(materiel, carte, joueurs, niveaux, frameSinceStart);
        }

        answerToInterfaceChange(materiel, carte, joueurs, frameSinceStart);
        


        niveaux.notifierAuxObserveurs();
    }

    public void answerToInterfaceChange(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, int frameSinceStart) {



        Joueur j = joueurs.getJoueur();
        if (j != null) {


            //gestion de la position du joueur principale
            j.setDestination(materiel.getMousePositionActuelle());

            if (materiel.isMouseBUTTON3()) {
                j.setPosition(materiel.getMousePositionBUTTON3Pressed());
            } else if (!materiel.isMouseBUTTON1()) {
                j.setPosition(materiel.getMousePositionActuelle());
            }

            //gestion d'un action de la sourit
            if (materiel.isMouseBUTTON1() && materiel.isMouseBUTTON3()) {
                Explosif explosif = null;
                if ((explosif = carte.getFirstMineOn(materiel.getMousePositionBUTTON1Pressed())) != null) {
                    explosif.setDeminer(true);
                } else {
                    carte.createFlagHere(materiel.getMousePositionBUTTON1Pressed());
                }

            }
        }



        carte.notifierAuxObserveurs();

    }

    private void putInterfaceToGameOver(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, ModèlePartie niveaux, int frameSinceStart) {

        //si je veux voir toutes les bombes au game over
        for (Mine mine : carte.getMines()) {
            mine.setActiver(true);
            mine.setTempsRestantEnSec(BoitAOutil.nombreAléatoireEntreA_B(0, 5));
        }
    }

    private void checkGameOver(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, ModèlePartie niveaux, int frameSinceStart) {
        ArrayList<Mine> minesExploded = null;
        if ((minesExploded = carte.getMinesHowExploded()) != null && minesExploded.size() > 0) {

            niveaux.setGameOver(true);
        }
    }

    private void checkGameWin(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, ModèlePartie niveaux, int frameSinceStart) {

        if (carte.isAllMinesDesarm()) {
            niveaux.setGameWin(true);
            
        }
    }

    public void aSecondPast(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, ModèlePartie niveaux) {

        ArrayList<Mine> minesActivated = null;
        if ((minesActivated = carte.getMinesActivated()) != null) {
            for (Mine mine : minesActivated) {
                mine.decTempsRestantEnSec(1);
            }
        }
        niveaux.incTempsDepuisDebutEnS();
    }
}
