/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur;

import demineur3.controleur.Gestionnaire.GestionnaireAffichage2D;
import demineur3.controleur.Gestionnaire.GestionnaireDeTemps;
import demineur3.controleur.Gestionnaire.GestionnaireInterface;
import demineur3.controleur.Gestionnaire.TraitementActionClavier;
import demineur3.controleur.Gestionnaire.TraitementActionSouris;
import demineur3.controleur.Gestionnaire.TraitementCollision;
import demineur3.controleur.Gestionnaire.TraitementDesActionsModèles;
import demineur3.donnees.ModèleElementsCarte;
import demineur3.donnees.ModèleInterface;
import demineur3.donnees.ModèleJoueurs;
import demineur3.donnees.ModèlePartie;
import demineur3.donnees.ModèleObjectif;
import java.awt.Image;

/**
 * Le controleur ce charge de synchroniser les demandes les signaux du jeux.
 *
 * @author christine
 */
public class Controleur {

    private GestionnaireDeTemps gTimer;
    private GestionnaireInterface gInterGraph;
    private GestionnaireAffichage2D gAff2D;
    private TraitementActionClavier traitKeyBoard;
    private TraitementActionSouris traitMouse;
    private TraitementDesActionsModèles traitDesModeles;
    private TraitementCollision traitCollision;
    private ModèleInterface mMateriel;
    private ModèleElementsCarte mMap;
    private ModèleJoueurs mUsers;
    private ModèlePartie mLevels;
    private ModèleObjectif mMission;

    {

        mMateriel = new ModèleInterface();
        mMap = new ModèleElementsCarte();
        mUsers = new ModèleJoueurs();
        mLevels= new ModèlePartie();
        mMission = new ModèleObjectif();
        
        
        gAff2D = new GestionnaireAffichage2D();
        traitKeyBoard = new TraitementActionClavier(mMateriel);
        traitMouse = new TraitementActionSouris(mMateriel);
        traitDesModeles = new TraitementDesActionsModèles();
        traitCollision = new TraitementCollision();
        
        gInterGraph = new GestionnaireInterface(this);
        mMateriel.addObserver(gInterGraph.getAffichage());
        mMap.addObserver(gInterGraph.getAffichage());
        mUsers.addObserver(gInterGraph.getAffichage());
        mLevels.addObserver(gInterGraph.getAffichage());
        mMission.addObserver(gInterGraph.getAffichage());
        
        gTimer = new GestionnaireDeTemps(this);

        recoitOrdreDeFaire("ORDER:INITIALISER");


    }

    public Controleur() {
    }

    synchronized public void recoitOrdreDeFaire(String ordre) {

        String sujet = "";
        String action = "";
        String variable = "";

        {// recupérer donnée
            String tmpTab[] = ordre.split(":");
            int taille = 3;
            if (1 < tmpTab.length) {
                sujet = tmpTab[0];
                action = tmpTab[1];
            }
            if (taille == tmpTab.length) {
                variable = tmpTab[2];
            }

        }


        if (sujet.equals("ORDER")) {
            if (action.equals("UPDATEAFFICHAGE")) {
                
                traitDesModeles.update(mMateriel, mMap, mUsers,mLevels, gTimer.getFrame());
                traitCollision.collision(mMateriel, mMap, mUsers, mLevels, gTimer.getFrame());

                Image imgTraitée = gAff2D.getImageOf(mMateriel,mMap, mUsers,mLevels,0,0, gInterGraph.getLargeurFenetreP(), gInterGraph.getHauteurFenetreP());
                gInterGraph.setAffichage2D(imgTraitée);
               if (mLevels.isGameOver() || mLevels.isGameWin() ) gTimer.pause();
                
            }
        }

        if (sujet.equals("ORDER")) {
            if (action.equals("INITIALISER")) {
                
                traitDesModeles.initialiser(mMateriel, mMap, mUsers,mLevels, gTimer.getFrame());

            }
        }
    }

    /**
     * le controleur reçoit une signalisation sur un évènement. Le controleur à
     * donc le choix d'execute ou pas une action
     *
     * @param String format= categorie:signalisation:parametre
     *
     */
    synchronized public void recoitSignalisationDe(String signalisation) {


        String sujet = "";
        String signalement = "";
        String variable = "";

        {// recupérer donnée
            String tmpTab[] = signalisation.split(":");
            int taille = 3;
            if (taille == tmpTab.length) {
                sujet = tmpTab[0];
                signalement = tmpTab[1];
                variable = tmpTab[2];
            }
        }


        if (sujet.equals("SIGNALISATION")) {

            int xPosMove = 0;
            int yPosMove = 0;
            if (variable != null && variable.split(",").length == 2) {
                xPosMove = Integer.parseInt((variable.split(","))[0]);
                yPosMove = Integer.parseInt((variable.split(","))[1]);

            }
            if (signalement.equals("UNESECONDEDEPASSE")) {

                
                if (variable != null && !variable.equals("")) {
                    gInterGraph.println("Seconde depuis le debut du jeu:" + variable);
                    gInterGraph.println("---------      Bilan     -----------");
                    gInterGraph.println(mMateriel + "");
                    traitDesModeles.aSecondPast(mMateriel, mMap, mUsers, mLevels);
                    
                    

                }
            } else if (signalement.equals("PLAYERMOUSEMOVED") && variable != null) {

                traitMouse.processThisMove(xPosMove, yPosMove);
                //gInterGraph.println("Move:" + "(" + xPosMove + "," + yPosMove + ")");
            } else if (signalement.equals("ACTIONCLICKDROITPRESSED") && variable != null) {

                traitMouse.processThisClick(xPosMove, yPosMove, 3, true);
                gInterGraph.println("Clicked right pressed:" + "(" + xPosMove + "," + yPosMove + ")");
            } else if (signalement.equals("ACTIONCLICKDROITRELEASED") && variable != null) {


                traitMouse.processThisClick(xPosMove, yPosMove, 3, false);
                gInterGraph.println("Clicked right released:" + "(" + xPosMove + "," + yPosMove + ")");
            } else if (signalement.equals("ACTIONCLICKPRESSED") && variable != null) {


                traitMouse.processThisClick(xPosMove, yPosMove, 1, true);
                gInterGraph.println("Click press:" + "(" + xPosMove + "," + yPosMove + ")");
            } else if (signalement.equals("ACTIONCLICKRELEASED") && variable != null) {


                traitMouse.processThisClick(xPosMove, yPosMove, 1, false);
                gInterGraph.println("Clicked released:" + "(" + xPosMove + "," + yPosMove + ")");
            } else if (signalement.equals("ACTIONKEYBOARDPRESSED") && variable != null) {
                traitKeyBoard.processThisKey(variable, true);
                //gInterGraph.println("Clavier pressed: " + variable);
            } else if (signalement.equals("ACTIONKEYBOARDRELEASED") && variable != null) {
                traitKeyBoard.processThisKey(variable, false);
                //gInterGraph.println("Clavier released: " + variable);
            }


        }
    }
}
