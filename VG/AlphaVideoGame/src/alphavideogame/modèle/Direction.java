/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alphavideogame.modèle;

/**
 *
 * @author Eloi
 */
public enum Direction {
    NORD,
    NORDEST,
    EST,
    SUDEST,
    SUD,
    SUDOUEST,
    OUEST,
    NORDOUEST,
    PASDIRECTION;

}
