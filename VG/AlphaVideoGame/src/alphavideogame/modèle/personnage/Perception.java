/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alphavideogame.modèle.personnage;

/**
 *
 * @author Eloi & Fab
 */
public class Perception {

    private int angle;
    private int drection;
    private int protéeDeDistinction;
    private int protéeDeSurprise;
    private int protéeDeVision;

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getDrection() {
        return drection;
    }

    public void setDrection(int drection) {
        this.drection = drection;
    }

    public int getProtéeDeDistinction() {
        return protéeDeDistinction;
    }

    public void setProtéeDeDistinction(int protéeDeDistinction) {
        this.protéeDeDistinction = protéeDeDistinction;
    }

    public int getProtéeDeSurprise() {
        return protéeDeSurprise;
    }

    public void setProtéeDeSurprise(int protéeDeSurprise) {
        this.protéeDeSurprise = protéeDeSurprise;
    }

    public int getProtéeDeVision() {
        return protéeDeVision;
    }

    public void setProtéeDeVision(int protéeDeVision) {
        this.protéeDeVision = protéeDeVision;
    }




}
