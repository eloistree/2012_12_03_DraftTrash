/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alphavideogame.modèle.personnage;

import alphavideogame.modèle.Direction;
import alphavideogame.modèle.ElementComposant;
import java.awt.Image;
import mécanisme.SérieDeMéthodeIndépendante;
import mécanisme.TraitementImage;

/**
 *
 * @author Fabrice
 */
public class Personnage extends ElementComposant {

    private int amure;
    private int attaque;
    private EtatDeForme etatDeForme;
    /** possèdes toutes caractéristiques sur le déplacement du personnage*/
    private EtatDeplacement etatDeplacement;
    private boolean invicilité;
    private int vieActuel;
    private int vieMax;

    /**
     * 
     * @param nom
     * @param largeur
     * @param hauteur
     * @param images
     * @param solide 
     */
    public Personnage(String nom, float largeur, float hauteur, Image[] images, boolean solide) {
        super(nom, largeur, hauteur, images, solide);

        etatDeForme = new EtatDeForme();
        etatDeplacement = new EtatDeplacement();
    }

    /**
     * Permet au personnage de sauter selon un critère de impulsion (vitesse initial)
     * @param value
     */
    public void jump(int value) {
        etatDeplacement.setMoving(true);
        etatDeplacement.setVitesseVertical(Math.abs(value));


    }

    public int getAmure() {
        return amure;
    }

    public void setAmure(int amure) {
        this.amure = amure;
    }


    public void setLargeur(float largeur) {
        this.largeur = largeur;
    }


    public void setHauteur(float hauteur) {
        this.hauteur = hauteur;
    }

    public int getAttaque() {
        return attaque;
    }

    public void setAttaque(int attaque) {
        this.attaque = attaque;
    }

    public EtatDeForme getEtatDeForme() {
        return etatDeForme;
    }

    public void setEtatDeForme(EtatDeForme etatDeForme) {
        this.etatDeForme = etatDeForme;
    }

    public EtatDeplacement getEtatDeplacement() {
        return etatDeplacement;
    }

    public void setEtatDeplacement(EtatDeplacement etatDeplacement) {
        this.etatDeplacement = etatDeplacement;
    }

    public boolean isInvicilité() {
        return invicilité;
    }

    public void setInvicilité(boolean invicilité) {
        this.invicilité = invicilité;
    }

    public int getVieActuel() {
        return vieActuel;
    }

    public void setVieActuel(int vieActuel) {
        this.vieActuel = vieActuel;
    }

    public int getVieMax() {
        return vieMax;
    }

    public void setVieMax(int vieMax) {
        this.vieMax = vieMax;
    }

    public boolean isHeMoving() {
        return etatDeplacement.isHeMoving();

    }

    public boolean isHeFalling() {
        if (etatDeplacement.getVitesseVertical() >= 0) {
            return true;
        }
        return false;

    }

    public void setVitesseHorizontal(int value) {
        etatDeplacement.setVitesseHorizontal(value);
    }

    public void setVitesseVertical(int value) {
        etatDeplacement.setVitesseVertical(value);
    }

    public float getVitesseHorizontal() {
        return etatDeplacement.getVitesseHorizontal() + getVitesseBonus();
    }

    public float getVitesseVertical() {
        return etatDeplacement.getVitesseVertical();
    }

    public void inverseVitesseHV() {
        inverseVitesseHV(Direction.PASDIRECTION);
    }

    public void inverseVitesseHV(Direction i) {
        if (i == Direction.PASDIRECTION
                || i == Direction.OUEST
                || i == Direction.EST
                ) {
            etatDeplacement.setVitesseHorizontal(-etatDeplacement.getVitesseHorizontal());
        }
        if (i == Direction.PASDIRECTION
                || i == Direction.NORD
                || i == Direction.SUD
                ) {
            etatDeplacement.setVitesseVertical(-etatDeplacement.getVitesseVertical());
        }
    }
    public void setXY() {
        setXY(getPositionX(),getPositionY());
    }
    public void setXY(float x, float y) {
        setPositionX(x);
        setPositionY(y);
        etatDeplacement.refreshVitesse();

    }

    /**se charge de recolté les viteses bonus de ce personnage*/
    public float getVitesseBonus() {
        return 0;

    }

    public void setMoving(boolean b) {
        etatDeplacement.setMoving(b);
    }

    public void resetMoving() {
        setMoving(false);

    }

    public void setFalling(boolean b) {
        etatDeplacement.setFalling(b);
    }

///////////////////////////////imagerie start///////////////////
    @Override
    public Image getImage(int i) {
        if (images == null || i < 0 || i > images.length) {
            return null;
        }
        return images[i];

    }

    @Override
    public Image getImageAnimation(long i) {
        return animations[0].getGoodImage(i);

    }

    @Override
    public Image getCurrentImage() {

        String etat = "";
        Image image;
        if (etatDeplacement.isHeMoving()) {
            etat = "M ";
            image = getImageAnimation(SérieDeMéthodeIndépendante.getTheTime());
            etat += "l";
            if (etatDeplacement.getVitesseHorizontal() > 0) {
                // TraitementImage.retournéeImageRightToLeft(image);
                etat += "r";
            }
        } else {
            etat = "DM ";
            image = getImage(0);

        }

        if (etatDeplacement.isHeFalling()) {
            etat = "F ";

        }

        System.out.println(etat);
        return image;

    }


///////////////////////////////imagerie start///////////////////
}
