/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alphavideogame.modèle.personnage;

import java.util.Calendar;
import mécanisme.EtatDesTouchesUtilisateur;
import mécanisme.SérieDeMéthodeIndépendante;

/**
 *
 * @author Fabrice
 */
public class EtatDeplacement {

    private EtatDesTouchesUtilisateur direction;
    private boolean flyMode;
    private boolean haveGround;
    private boolean moving;
    private boolean falling;
    private int acceleration;// par seconde
    private int decceleration; // par seconde
    private float vitesseVertical;
    private float vitesseVerticlaAccélération;
    private float gravité;
    private float vitesseMaxGravité;
    private float vitesseHorizontal;
    private float vitesseHorizontalAccélération;
    private float viteseMaxHorizontal;
    private long tempsCourse; //permet de savoir depuis combien de temps il sait déplacé
    private long tempsChute; //permet de savoir depuis combien de temps il tombe


    {
        setMoving(true);
        haveGround = false;
        flyMode = false;
        acceleration = 100;
        decceleration = 100;
        vitesseHorizontal = 0;
        vitesseVertical = 0;
        viteseMaxHorizontal = 500;
        vitesseMaxGravité = 400;
        gravité = 300;
        direction=null;


    }

    public void déplacmentX(int vitesse) {
        float vitessetmp = getVitesseHorizontal() + (float) vitesse;
        setVitesseHorizontal(vitessetmp);
    }

    public void déplacmentY(int vitesse) {
        float vitessetmp = getVitesseVertical() + (float) vitesse;
        setVitesseVertical(vitessetmp);

    }

    public long getTempsInitialDeplacement() {

        return tempsCourse;

    }

    /**
     * permet de définit si le personnage bouge.
     * @param value
     */
    public void setMoving(boolean value) {
        moving = value;
        if (value) {
            this.tempsCourse = SérieDeMéthodeIndépendante.getTheTime();
        } else {
            tempsCourse = -1;
            resetMoving();
        }
    }

    public boolean isHeMoving() {
        return moving;
    }

    private void resetMoving() {
        vitesseHorizontal = 0;

    }

    public void setFalling(boolean value) {
        falling = value;
        if (value) {
            this.tempsChute = SérieDeMéthodeIndépendante.getTheTime();
        } else {
            tempsChute = -1;
            resetFalling();
        }
    }

    public boolean isHeFalling() {
        return falling;
    }

    private void resetFalling() {
        vitesseVertical = 0;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(int acceleration) {
        this.acceleration = acceleration;
    }

    public int getDecceleration() {
        return decceleration;
    }

    public void setDecceleration(int decceleration) {
        this.decceleration = decceleration;
    }

    public float getGravité() {
        return gravité;
    }

    public void setGravité(float gravité) {
        this.gravité = gravité;
    }

    public boolean isAccelering() {
        if (tempsCourse <= 0) {
            return false;
        } else {
            return true;
        }

    }

    public void jump(float vitesse) {
        vitesseVertical += vitesse;
    }

    public float getVitesseHorizontal() {

        return vitesseHorizontal + vitesseHorizontalAccélération;
    }

    public void setVitesseHorizontal(float vitesse) {
        if (vitesse > viteseMaxHorizontal) {
            vitesse = viteseMaxHorizontal;
        }
        this.vitesseHorizontal = vitesse;
    }

    public float getVitesseVertical() {

        return vitesseVertical + vitesseVerticlaAccélération;
    }

    public void setVitesseVertical(float vitesse) {

        if (vitesse > vitesseMaxGravité) {
            vitesse = viteseMaxHorizontal;
        }
        this.vitesseVertical = vitesse;
    }

    public float getVitesseMax() {
        return viteseMaxHorizontal;
    }

    public void setVitesseMax(float vitesseMax) {
        this.viteseMaxHorizontal = vitesseMax;
    }

    public void refreshVitesse() {

        float temps = durée(tempsCourse, SérieDeMéthodeIndépendante.getTheTime());
        if (temps >= 0) {
            refreshVitesseWithDuréeDéplacement(temps);
        }
        temps = durée(tempsChute, SérieDeMéthodeIndépendante.getTheTime());
        if (temps >= 0) {
            refreshVitesseWithDuréeChute(temps);
        }
    }

    /**
    @param  timeDebutAccélération == temps à moment du début de ça course
    @param  timeWhenAction == temps au moment du refresh
     */
    public float durée(long timeDebutAccélération, long timeWhenAction) {

        System.out.println("d:" + timeDebutAccélération + " -> " + timeDebutAccélération + "");

        if (timeDebutAccélération >= 0 && timeWhenAction >= 0) {

            return ((float) (timeWhenAction - timeDebutAccélération)) / 1000;

        }
        return 0;
    }

    public void refreshVitesseWithDuréeDéplacement(float tempsCourse) {
        if (tempsCourse > 0) {
            //permière partie sur la vitesse horizontal
            if (acceleration != 0 && moving && vitesseHorizontal != 0) {

                int accélération;
                if (vitesseHorizontal <= 0.0) {
                    accélération = -acceleration;
                } else {
                    accélération = acceleration;
                }


                float vitesseTmp = accélération * tempsCourse;


                System.out.println("Vitesse =" + vitesseTmp + " = " + acceleration + " * " + tempsCourse + "/1000");
                if (vitesseTmp > viteseMaxHorizontal) {
                    vitesseHorizontalAccélération = viteseMaxHorizontal;
                } else if (vitesseTmp < -viteseMaxHorizontal) {
                    vitesseHorizontalAccélération = -viteseMaxHorizontal;
                } else {
                    vitesseHorizontalAccélération = vitesseTmp;
                }
                System.out.println("Vitesse réel: " + vitesseHorizontalAccélération);
            } else if (!moving) {
                vitesseHorizontalAccélération = 0;
                if (vitesseHorizontal != 0) {
                    if (vitesseHorizontal > 0) {
                        vitesseHorizontal -= decceleration;
                        if (vitesseHorizontal < 0) {
                            vitesseHorizontal = 0;
                        }

                    } else if (vitesseHorizontal < 0) {

                        vitesseHorizontal += decceleration;

                        if (vitesseHorizontal > 0) {
                            vitesseHorizontal = 0;
                        }
                    }
                }
            }
        }
    }

    private void refreshVitesseWithDuréeChute(float tempsChute) {


        if (tempsChute > 0) {
            if (falling) {


                float vitesseTmp = gravité * tempsChute;



                System.out.println("Vitesse vertical =" + vitesseTmp + " = " + gravité + " * " + tempsChute + "/1000");

                vitesseVerticlaAccélération = vitesseTmp;
                System.out.println("Vitesse réel: " + vitesseVerticlaAccélération);
            } /*else if (vitesseVerticlaAccélération != 0 && haveGround) {
            System.out.println("Toucher le sol");
            vitesseVertical = 0;

            }*/ else {
                vitesseVertical = 0;
                vitesseVerticlaAccélération = 0;
            }
        }
    }

    public void setDirection(EtatDesTouchesUtilisateur direction) {
        this.direction = direction;
    }

    public EtatDesTouchesUtilisateur getDirection() {
        return direction;
    }



}
