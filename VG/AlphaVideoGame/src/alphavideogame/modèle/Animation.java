/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alphavideogame.modèle;

import java.awt.Image;

/**
 *
 * @author Eloi & Fab
 */
public class Animation {

    private Image[] animation;
    /**frame par seconde*/
    private int vitesseAnimation;
    public Animation (Image[] animation, int vitesse)
    {
        this.animation = animation;
        vitesseAnimation = vitesse;
    }
     public Image getGoodImage( long tempsActuelle)
    {
         return getGoodImage(0, tempsActuelle);
    }
    public Image getGoodImage(long tempsAuDebutDuMouvement, long tempsActuelle)
    {
        if (animation!= null && animation.length >0)
        { tempsActuelle= Math.abs(tempsActuelle);
        int indice = (int)(((float)tempsActuelle/(float)1000)*(float) Math.abs(vitesseAnimation))%animation.length;
        return animation[indice];
        }
        return null;
    }


}
