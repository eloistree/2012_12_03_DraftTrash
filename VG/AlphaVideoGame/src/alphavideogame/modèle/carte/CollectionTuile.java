package alphavideogame.modèle.carte;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import alphavideogame.modèle.carte.Tuile;
import java.awt.Image;
import java.util.HashMap;

/**
 *
 * @author Eloi & Fab
 */
public class CollectionTuile {

    private HashMap<Integer, Tuile> collectionDeTuile;

    public CollectionTuile() {

        collectionDeTuile = new HashMap<Integer, Tuile>();
        initialisation();

    }
    /** 
     * Méthode qui initialise les tuiles du jeux dans une table hash
     */
    private void initialisation() {


        Image[] images = new Image[1];
        images[0] = new javax.swing.ImageIcon(getClass().getResource("/alphavideogame/ressources/images/tuilles/ciel.gif")).getImage();

        Tuile t0 = new Tuile("VIDE", null, 0, false);

        images = new Image[1];
        images[0] = new javax.swing.ImageIcon(getClass().getResource("/alphavideogame/ressources/images/tuilles/sol.gif")).getImage();

        Tuile t1 = new Tuile("SOL", images, 1, true);

        collectionDeTuile.put(new Integer(t0.getId()), t0);
        collectionDeTuile.put(new Integer(t1.getId()), t1);
    }

    public Tuile getTuilleFor(int i) {
        return collectionDeTuile.get(i);
    }

    public HashMap<Integer, Tuile> getCollectionDeTuile() {
        return collectionDeTuile;
    }

    

    
}
