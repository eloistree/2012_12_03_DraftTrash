/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alphavideogame.modèle.carte;

import alphavideogame.modèle.ElementComposant;
import alphavideogame.modèle.personnage.Personnage;
import java.awt.Image;

/**
 *
 * @author Eloi & Fab
 */
public class Tuile extends ElementComposant {

    private  int id=0;
    boolean touchée;


    public Tuile(String nom, Image [] images, int id, boolean solide)
    {
        super(nom, images, solide);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Image getCurrentImage() {
       return getImage(0);
    }

    public void touchée(Personnage perso)
    {
        touchée = true;
        // gestion d'action si réaction au personange


    }

    

}