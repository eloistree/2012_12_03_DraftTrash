/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Fenetre.java
 *
 * Created on 1 nov. 2011, 14:32:17
 */

package alphavideogame;

import java.awt.Graphics;
import javax.swing.JPanel;
import sun.swing.BakedArrayList;

/**
 *
 * @author Eloi & Fab
 */
public class Fenetre extends javax.swing.JFrame {


    private JLayoutPanel backGround;
    private JLayoutPanel arrièrePlan;
    private JLayoutPanel plateforme;
    private JLayoutPanel avantPlan;

    /** Creates new form Fenetre */
    public Fenetre() {
        initComponents();
        backGround= new JLayoutPanel();
        arrièrePlan= new JLayoutPanel();
        plateforme= new JLayoutPanel();
        avantPlan= new JLayoutPanel();
        backGround.setOpaque(false);
        arrièrePlan.setOpaque(false);
        plateforme.setOpaque(false);
        avantPlan.setOpaque(false);

        this.add(backGround, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,  1280, 800));
        this.add(arrièrePlan, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,  1280, 800));
        this.add(plateforme, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,  1280, 800));
        this.add(avantPlan, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,  1280, 800));
        backGround.setSize(1280, 800);
        arrièrePlan.setSize(1280, 800);
        plateforme.setSize(1280, 800);
        avantPlan.setSize(1280, 800);
        this.pack();

        this.setVisible(true);
    }

    public Graphics getGraphicsBackGround(){return backGround.getGraphics();}
    public Graphics getGraphicsArrièrePlan(){return arrièrePlan.getGraphics();}
    public Graphics getGraphicsPlateforme(){return plateforme.getGraphics();}
    public Graphics getGraphicsAvantPlan(){return avantPlan.getGraphics();}

     public JPanel getJPanelBackGround(){return backGround;}
    public JPanel getJPanelArrièrePlan(){return arrièrePlan;}
    public JPanel getJPanelPlateforme(){return plateforme;}
    public JPanel getJPanelAvantPlan(){return avantPlan;}



    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
