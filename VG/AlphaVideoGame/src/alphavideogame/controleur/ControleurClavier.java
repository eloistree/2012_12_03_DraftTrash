/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alphavideogame.controleur;

import alphavideogame.Fenetre;
import alphavideogame.modèle.ConstantesDuJeu;
import alphavideogame.modèle.personnage.Personnage;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import mécanisme.EtatDesTouchesUtilisateur;

/**
 *
 * @author Eloi
 */
public class ControleurClavier implements KeyListener,ConstantesDuJeu {

    private Fenetre fenetre;
    private ControleurPrincipal controleurPrincipal;
    private EtatDesTouchesUtilisateur direction;
    private Personnage  characterAssocié;
    private boolean isMoving;

    public ControleurClavier(ControleurPrincipal controleurPrincipal ,Fenetre fenetre) {
    
    this.fenetre =fenetre;
    this.controleurPrincipal=controleurPrincipal;
    characterAssocié =controleurPrincipal.getCurrentCharactrer();
    direction = characterAssocié.getEtatDeplacement().getDirection();
    fenetre.addKeyListener(this);
    isMoving=false;
    }

   public void keyTyped(KeyEvent e) {
        System.out.println("Typed");
    }


    public void keyPressed(KeyEvent e) {

        System.out.println("Pressed: "+e.getKeyCode());
        direction.setKeyPressed(e.getKeyCode(), true);

        if (isMoving==false) {characterAssocié.setMoving(true);
            isMoving=true;}
        if(direction.isHeJumping() )
        {
            characterAssocié.setFalling(true);
            characterAssocié.jump(400);

        }
        if (direction.isToTheLeft())
            characterAssocié.setVitesseHorizontal(300);
        else if(direction.isToTheRight())
            characterAssocié.setVitesseHorizontal(-300);
        if (direction.isClimbing())
        {
            characterAssocié.setFalling(true);
            characterAssocié.setVitesseVertical(-200);
        }

        if (direction.isPressedDown())
            characterAssocié.setVitesseVertical(200);

        if (e.getKeyCode() == KeyEvent.VK_ENTER){
            characterAssocié.setPositionX(300);
            characterAssocié.setPositionY(300);
            characterAssocié.setFalling(false);
            characterAssocié.setMoving(false);


        }

     
    }
    public void keyReleased(KeyEvent e) {
        System.out.println("Released"+e.getKeyCode());
        direction.setKeyPressed(e.getKeyCode(), false);
        if(direction.isAllArrowReleased()){characterAssocié.setMoving(false); isMoving=false;}
     
    }

}
