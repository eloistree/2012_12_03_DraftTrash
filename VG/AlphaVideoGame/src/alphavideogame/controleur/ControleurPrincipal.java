package alphavideogame.controleur;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import alphavideogame.controleur.affichage.ControleurRafraichissement;
import alphavideogame.controleur.affichage.ControleurDeCollision;
import alphavideogame.controleur.donnée.GameData;
import alphavideogame.Fenetre;
import alphavideogame.JLayoutPanel;
import alphavideogame.modèle.carte.Map;
import alphavideogame.modèle.personnage.Personnage;
import alphavideogame.moteur.TimerApp;
import java.awt.event.ActionEvent;
import mécanisme.SérieDeMéthodeIndépendante;

/**
 *
 * @author Eloi & Fab
 */
public class ControleurPrincipal {

    private Fenetre fenetre;
    private JLayoutPanel[] layoutsApplication;
    private TimerApp timer;
    private ControleurRafraichissement refreshControl;
    private GameData gameDataControl;
    private ControleurClavier keyBoardControl;
    private ControleurDeCollision bigBangContoller;

    public ControleurPrincipal(Fenetre fenetre, JLayoutPanel[] layoutsApplication) {

             gameDataControl = new GameData();

         bigBangContoller = new ControleurDeCollision(this,gameDataControl);
         refreshControl = new ControleurRafraichissement(this);
     
        refreshControl.ajouterObject(gameDataControl.getLevel1());
        refreshControl.ajouterObject(gameDataControl.getMainCharacter());

        this.fenetre = fenetre;
        
        keyBoardControl = new ControleurClavier(this,fenetre);
        this.layoutsApplication = layoutsApplication;



        this.timer = new TimerApp(30, this);
        SérieDeMéthodeIndépendante.setTimer(timer);
       
        
    
       
    }

    /**
     @param e évènement correspondant à la fréquence de rafraichissement définit par TimerApp: ici 15 ms
     */
    public void signalDeRafraichissement(ActionEvent e) {


            refreshControl.refreshPosition(e);
            refreshControl.refreshGraphics();
        
    }

    public Fenetre getFenetre() {
       return fenetre;
    }

    public TimerApp getTimer() {
        return timer;
    }

    public Map getCurrentMap(){return gameDataControl.getLevel1();}

    public long getTime(){return timer.getTimeSinceStartApp();}

    public Personnage getCurrentCharactrer() {
     return gameDataControl.getMainCharacter();
    }

   public  ControleurDeCollision getControlerCollision() {
       return bigBangContoller;
    }

}
