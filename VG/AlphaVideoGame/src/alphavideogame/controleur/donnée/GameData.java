/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alphavideogame.controleur.donnée;

import alphavideogame.modèle.carte.CollectionTuile;
import alphavideogame.modèle.ConstantesDuJeu;
import alphavideogame.modèle.carte.Map;
import alphavideogame.modèle.personnage.Personnage;
import java.awt.Image;
import mécanisme.EtatDesTouchesUtilisateur;

/**
 *
 * @author Eloi & Fab
 */
public class GameData  implements ConstantesDuJeu {

    private Map level1;
    private Personnage mainCharacter;
    private Personnage [] personnages;
    private CollectionTuile toutesLesTuiles;
    //private ArrayList<Personnage> personnages;

    {

        toutesLesTuiles = new CollectionTuile();
        initialiserMapLevel1();
    }

    private void initialiserMapLevel1() {

           Image [] imageDuPersonnage = new Image[1];
            imageDuPersonnage[0] =  new javax.swing.ImageIcon(getClass().getResource("/alphavideogame/ressources/images/mario1.gif")).getImage();

           Image [] animation = new Image[3];
            animation[0] =  new javax.swing.ImageIcon(getClass().getResource("/alphavideogame/ressources/images/mario1.gif")).getImage();
            animation[1] =  new javax.swing.ImageIcon(getClass().getResource("/alphavideogame/ressources/images/mario2.gif")).getImage();
            animation[2] =  new javax.swing.ImageIcon(getClass().getResource("/alphavideogame/ressources/images/mario3.gif")).getImage();

        mainCharacter = new Personnage("Mario",imageDuPersonnage[0].getWidth(null),imageDuPersonnage[0].getHeight(null),imageDuPersonnage , true);
        mainCharacter.getEtatDeplacement().setDirection(new EtatDesTouchesUtilisateur());
        mainCharacter.setAnimation(animation, 6);
        mainCharacter.setPositionX(200);
        mainCharacter.setPositionY(200);
        
        int l = 20  , c = 30;
        int[][] unLevel = new int[l][c];

        for (int i = 0; i < l; i++) 
            for (int j = 0; j < c; j++) {
                unLevel[i][j] = 0;
                if (j == c - 1 || i == l - 1 || j == 0 || i == 0) {
                    unLevel[i][j] = 1;
                }

            }

        unLevel[3][10] = 1;
        unLevel[4][6] = 1;

        level1 = new Map(unLevel,l,c, toutesLesTuiles.getCollectionDeTuile());
    }

    public Map getLevel1() {
        return level1;
    }

    public void setLevel1(Map level1) {
        this.level1 = level1;
    }

    public Personnage getMainCharacter() {
        return mainCharacter;

    }
       public void setMainCharacter(Personnage perso) {
        mainCharacter =perso;

    }
}
