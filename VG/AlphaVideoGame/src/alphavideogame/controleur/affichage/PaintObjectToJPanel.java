/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alphavideogame.controleur.affichage;

import alphavideogame.modèle.carte.CollectionTuile;
import alphavideogame.modèle.carte.Map;
import alphavideogame.modèle.personnage.Personnage;
import alphavideogame.modèle.*;
import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author Eloi
 */
public class PaintObjectToJPanel implements ConstantesDuJeu {

private CollectionTuile tmpIni = new CollectionTuile();

  

    void printObjectToThisGraphics(Graphics buffer, Map m) {

            for(int i =0; i<m.getLigne();i++)
            {
                for(int j =0; j<m.getColomn();j++)
                {
                    Image tmp = tmpIni.getTuilleFor(m.getIdTo(i, j)).getImage(0);
                    if (tmp!=null)
                    {
                        
                        buffer.drawImage(tmp, j * TAILLE_TUILE, i * TAILLE_TUILE, null);
                    }
                }
            }

             for(int i =0; i<m.getLigne();i++)
            {
                buffer.drawLine(0,i*(TAILLE_TUILE)-1,m.getLargeur()-1, i*(TAILLE_TUILE)-1);

            }
             for(int i =0; i<m.getColomn();i++)
             {
                   buffer.drawLine(i*(TAILLE_TUILE)-1, 0, i*(TAILLE_TUILE)-1, m.getHauteur()-1);


              }

    }

    void printObjectToThisGraphics(Graphics buffer, Personnage m) {


                    Image tmp = m.getCurrentImage();
                    if (tmp!=null)
                   buffer.drawImage(tmp,(int) m.getPositionX(), (int)m.getPositionY(),null );
                   buffer.drawRect((int)m.getPositionX(),(int) m.getPositionY(),(int)m.getLargeur(), (int)m.getHauteur());
                   buffer.drawRect((int)m.getPositionX(),(int) m.getPositionY(),TAILLE_TUILE, 2*TAILLE_TUILE);

    }

}
