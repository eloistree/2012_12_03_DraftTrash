/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alphavideogame.controleur.affichage;

import alphavideogame.controleur.ControleurPrincipal;
import alphavideogame.controleur.donnée.GameData;
import alphavideogame.modèle.ConstantesDuJeu;
import alphavideogame.modèle.Direction;
import alphavideogame.modèle.carte.Map;
import alphavideogame.modèle.personnage.Personnage;
import alphavideogame.modèle.carte.Tuile;
import java.util.ArrayList;
import mécanisme.SérieDeMéthodeIndépendante;

/**
 *
 * @author Eloi
 */
public class ControleurDeCollision implements ConstantesDuJeu {

    private ControleurPrincipal controleurPrincipal;
    private GameData gameData;
    private Map mapCourrante;

    public ControleurDeCollision(ControleurPrincipal aThis, GameData gameDataControl) {
        controleurPrincipal = aThis;
        gameData = gameDataControl;
        regreshMap();


    }

    public final void regreshMap() {
        if (mapCourrante == null) {
            mapCourrante = gameData.getLevel1();
        }
    }

    public void gestionDuDéplacementDe(Personnage perso, int tempsDepuisDernierAction) {
        int ut = tempsDepuisDernierAction;
        float[] futurePosition = SérieDeMéthodeIndépendante.monObjectSeraLà(ut, perso.getVitesseHorizontal(), perso.getVitesseVertical(), perso.getPositionX(), perso.getPositionY());
        // si une carte est chargée
        if (mapCourrante != null) {

            if (mapCourrante.pointInTheMap(perso, futurePosition)) {
                boolean collisionAvecSolide = false;
                ArrayList<Tuile> tuilleEnCollision;
                // si il y a des collisions
                if ((tuilleEnCollision = mapCourrante.getAllTuilesTouch(perso, futurePosition)) != null) {
                    // ajouter les tuiles à la liste des objets à rafraichir avec un évènement d'animation
                    for (Tuile tmpT : tuilleEnCollision) {
                        System.out.println(tmpT);
                        tmpT.touchée(perso);
                        if (tmpT.isSolide()) {
                            collisionAvecSolide = true;
                        }

                    }

                    if (!collisionAvecSolide) {
                        perso.setXY(futurePosition[0], futurePosition[1]);
                    } else {
                        // si collision travail de replacement
                        System.out.println("Collision");
                    }
                }


            } else {

                System.out.println("Personnage hors carte");
                perso.setFalling(false);
                perso.inverseVitesseHV(Direction.PASDIRECTION);
                perso.setXY(200, 200);
                // replacer la position.
            }




            //else if(false /*si rencontre un bloc*/) {/* stopper*/}
            //else if(false /*si du vide en dessou*/) {/* appliquer la gravité*/}
        } //si pas de carte, je place le perso à une position fixe
        else {

            System.out.println("Il n'y a pas de carte");
            perso.setXY(TAILLE_TUILE, TAILLE_TUILE * 2);
        }






    }

     private boolean isThereSolideInThis(ArrayList<Tuile> tuilleEnCollision) {
        boolean collisionSolid=false;
        for (Tuile tmpT : tuilleEnCollision) {
            if (tmpT.isSolide()) {
                collisionSolid = true;
            }
        }
        return collisionSolid;

}
}
