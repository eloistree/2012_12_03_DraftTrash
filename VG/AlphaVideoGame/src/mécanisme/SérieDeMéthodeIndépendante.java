/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mécanisme;

import alphavideogame.modèle.carte.Map;
import alphavideogame.modèle.personnage.Personnage;
import alphavideogame.moteur.TimerApp;

/**
 *
 * @author Eloi
 */


public class SérieDeMéthodeIndépendante {

    static private TimerApp  timer ;

    static public void setTimer(TimerApp timer)
    {
        SérieDeMéthodeIndépendante.timer = timer;
    }
    
    static private long getTimeNow(){
        if (timer!=null) return timer.getTimeSinceStartApp();
        else return -1;
    }

    static public int getUnitéTemps(){
       if (timer!=null) return timer.getUnitéDeTemps();
        else return -1;
   }
   

      static  public float[] monObjectSeraLà(int timeMS, float vitesseHorizontal, float vitesseVertical, float positionX, float positionY)
      {
        float[] p = new float[2];
        p[0] = positionX;
        p[1] = positionY;

        float d = ((float)timeMS)*((float)vitesseHorizontal)/(float)1000.0 ;
        p[0] += d;
         d = ((float)timeMS)*((float)vitesseVertical)/(float)1000.0 ;
       p[1] += d;
        return p;
    }
      static public long getTheTime(){return getTimeNow();}
      
      
      
     
}
