/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame;

import alpha_v2_vidéogame.controleur.ControleurPrincipal;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 *
 * @author Eloi
 */
public class Main {

    /**
     * Lancement du jeu
     */
    public static void main(String[] args) {


                printUsage();

            Fenetre fenetre = new Fenetre();
            
            

            ControleurPrincipal cp = new ControleurPrincipal(fenetre);



    }

            private static void printUsage() {
          OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
          for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().startsWith("get")
                && Modifier.isPublic(method.getModifiers())) {
                    Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (Exception e) {
                    value = e;
                } // try
                System.out.println(method.getName() + " = " + value);
            } // if
          } // for
        }


}
