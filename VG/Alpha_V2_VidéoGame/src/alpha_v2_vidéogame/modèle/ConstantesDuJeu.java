/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.modèle;

/**
 *
 * @author Eloi & Fab
 */
public interface ConstantesDuJeu {
    public final int TAILLE_TUILE = 32;

    public final int HAUTEURZONEDEJEU = 20 *TAILLE_TUILE;
    public final int LARGEURZONEDEJEU = 25 *TAILLE_TUILE;
}