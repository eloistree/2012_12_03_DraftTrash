/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.mécanisme;

import java.awt.event.KeyEvent;

/**
 *
 * Décrit les touches du clavier que l'utilisateur est entrain de presser
 * @author Eloi
 */
public class EtatDesTouchesUtilisateur {

      private boolean up;
      private boolean down;
      private boolean left;
      private boolean right;
      private boolean space;
      private long timeSinceNoReleased;
      private long timeNow;


       public int setKeyPressed(KeyEvent e,boolean pressed){
           int keyPressed = e.getKeyCode();

            // si premier évènement
            if (timeSinceNoReleased<0) timeSinceNoReleased = SérieDeMéthodeStatic.getTimeNow();
            //sinon
            else timeNow = SérieDeMéthodeStatic.getTimeNow();

         switch ( keyPressed) {
            case KeyEvent.VK_RIGHT:
                right = pressed;
                break;
            case KeyEvent.VK_LEFT:
                left = pressed;
                break;
            case KeyEvent.VK_UP:
                up = pressed;
                break;
            case KeyEvent.VK_DOWN:
                down = pressed;
                break;
             case KeyEvent.VK_SPACE:
                space = pressed;
                break;
            }



       return getDirectionKey();
    }

       public int getDirectionKey(){
       if (up && right && !down && !left) return 1;
       else  if (up && left  && !down && !right) {return 7;}
       else  if (up && !right && !left && !down)return 0;
       else  if (down && right && !up && !left) return 3 ;
       else  if(down && left && !up && !right) return 5;
       else  if (down && !right && !left && !up)return 4;
       else  if (left && !up && !down && !right)return 6;
       else  if (right && !up && !down && !left )return 2;
       else  if ( !right && !left && !up && !down){timeSinceNoReleased=-1; timeNow =-1; return 10;}
       else return 9;

     }

    void setDirection(int i) {

        up= false;
        down= false;
        left= false;
        right= false;
        switch(i)
        {
            case 0:up =true; break;
            case 1:up=true; right=true; break;
            case 2:right =true; break;
            case 3:down=true; right=true; break;
            case 4:down =true; break;
            case 5:down=true; left=true; break;
            case 6:left =true; break;
            case 7: up=true; left=true; break;

        }
    }

    public boolean isHeJumping()
    {
     return space;

    }

    public boolean isToTheLeft() {
        int d=getDirectionKey();
       if (d==1 || d==2 || d==3 ) return true;
        return false;
    }
    public boolean isToTheRight() {
        int d=getDirectionKey();
       if (d==5 || d==6 || d==7 ) return true;
        return false;
    }

    public boolean isClimbing() {
       if (up) return true;
        return false;
    }

    public boolean isPressedDown() {
       if (down) return true;
        return false;
    }

    public boolean isAllArrowReleased() {
        if (!right && !left && !up && !down) return true;
        return false;
    }

    public long getDuréeMS() {
        if (timeNow>=0 && timeSinceNoReleased>=0)
        return timeNow - timeSinceNoReleased;
        return 0;
    }
    public int getDurée() {
        if (getDuréeMS()!=0)
        return (int) (getDuréeMS()/1000);
        return 0;
    }

    public String getDirectionInString() {


           switch(getDirectionKey())
        {
            case 0:return "Sud";
            case 1:return "Sud Est";
            case 2:return "Est";
            case 3:return "Nord Est";
            case 4:return "Nord";
            case 5:return "Nord Ouest";
            case 6:return "Ouest";
            case 7:return "Sud Ouest";
            case 10:return "None";
            case 9:return "Indéterminée";

        }
           return "";
    }
}
