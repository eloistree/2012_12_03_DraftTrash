/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Fenetre.java
 *
 * Created on 1 nov. 2011, 14:32:17
 */

package alpha_v2_vidéogame;

import alpha_v2_vidéogame.modèle.ConstantesDuJeu;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 *
 * @author Eloi & Fab
 */
public class Fenetreee extends javax.swing.JFrame implements ConstantesDuJeu {


    private JLayoutPanel backGround;
    private JLayoutPanel arrièrePlan;
    private JLayoutPanel plateforme;
    private JLayoutPanel avantPlan;

    /**
            //background
            //arrièreplan
            //"tuile"
            //avant plan
            */
    public Fenetreee() {
        initComponents();

          GraphicsEnvironment env = GraphicsEnvironment.
                getLocalGraphicsEnvironment();
            GraphicsDevice device = env.getDefaultScreenDevice();


            GraphicsConfiguration gc = device.getDefaultConfiguration();

            this.setUndecorated(true);
            this.setIgnoreRepaint(true);
           // device.setFullScreenWindow(this);

            int numBuffers =4;
            Rectangle bounds = this.getBounds();



        backGround= new JLayoutPanel();
        arrièrePlan= new JLayoutPanel();
        plateforme= new JLayoutPanel();
        avantPlan= new JLayoutPanel();
        backGround.setOpaque(true);
        arrièrePlan.setOpaque(false);
        plateforme.setOpaque(false);
        avantPlan.setOpaque(false);

        setLocation(100, 100);
        int width =LARGEURZONEDEJEU ;
        int height = HAUTEURZONEDEJEU;
        this.setSize( width, height);
        this.add(backGround, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, width, height));
        this.add(arrièrePlan, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,  width, height));
        this.add(plateforme, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,  width, height));
        this.add(avantPlan, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,  width, height));


        backGround.setSize(width, height);
        arrièrePlan.setSize(width, height);
        plateforme.setSize(width, height);
        avantPlan.setSize(width, height);
        avantPlan.setBorder(new LineBorder(Color.GREEN));
        this.pack();

        this.setVisible(true);
    }

    public Graphics getGraphicsBackGround(){return backGround.getGraphics();}
    public Graphics getGraphicsArrièrePlan(){return arrièrePlan.getGraphics();}
    public Graphics getGraphicsPlateforme(){return plateforme.getGraphics();}
    public Graphics getGraphicsAvantPlan(){return avantPlan.getGraphics();}

     public JPanel getJPanelBackGround(){return backGround;}
    public JPanel getJPanelArrièrePlan(){return arrièrePlan;}
    public JPanel getJPanelPlateforme(){return plateforme;}
    public JPanel getJPanelAvantPlan(){return avantPlan;}



    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 551, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 401, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
