/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.controleur;

import alpha_v2_vidéogame.AideAuxProgrammeurs;
import java.util.Observable;

/**
 *
 * @author Eloi
 */
abstract public class  Controleur extends Observable {


    static private AideAuxProgrammeurs fenetreAideProgrammeur = new AideAuxProgrammeurs();

    protected Controleur()
    {
    this.addObserver(fenetreAideProgrammeur);
    }

     protected void notifyChange(String value)
   {
   setChanged();
   notifyObservers(value);

   }
     protected void notifyChange(Object value)
   {
   setChanged();
   notifyObservers(value);

   }

    public static AideAuxProgrammeurs getFenetreAideProgrammeur() {
        return fenetreAideProgrammeur;
    }

}
