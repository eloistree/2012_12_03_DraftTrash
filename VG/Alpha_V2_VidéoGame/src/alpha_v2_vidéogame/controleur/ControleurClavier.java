/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.controleur;

import alpha_v2_vidéogame.Fenetre;
import alpha_v2_vidéogame.modèle.ConstantesDuJeu;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import alpha_v2_vidéogame.mécanisme.EtatDesTouchesUtilisateur;

/**
 *
 * @author Eloi
 */
public class ControleurClavier extends Controleur implements KeyListener,ConstantesDuJeu {

    private Fenetre fenetre;
    private ControleurPrincipal controleurPrincipal;
    private EtatDesTouchesUtilisateur direction;
    /** Permet de savoir si le jeu est en pause au non à partir de la touche p du clavier*/
    private boolean keyPause;


    {
        keyPause = false;
    }
    public ControleurClavier(ControleurPrincipal controleurPrincipal ,Fenetre fenetre) {
    super();
    this.fenetre =fenetre;
    this.controleurPrincipal=controleurPrincipal;
    direction = new EtatDesTouchesUtilisateur();
  
    fenetre.addKeyListener(this);
    }

   public void keyTyped(KeyEvent e) {
   
    }


    public void keyPressed(KeyEvent e) {

        // active ou désactive le mode pause, si la touche p enfoncée.
        if (e.getKeyChar() == 'p')
        {
            keyPause = !keyPause;
            if (keyPause)
            controleurPrincipal.pause();
            else
            controleurPrincipal.play();
        }

        if (keyPause){System.out.println("Touche clavier: '"+e.getKeyChar()+"' ("+e.getKeyCode()+") impossible -> jeu en pause");}
        else{
                   direction.setKeyPressed(e, true);
            controleurPrincipal.signalDuClavier(e,direction);
     
        }

        notifyChange(direction);
    }
    public void keyReleased(KeyEvent e) {

        if (keyPause){}
        else{
              direction.setKeyPressed(e, false);
            controleurPrincipal.signalDuClavier(e,direction);
          
        }
            

        notifyChange(direction);
    }

}
