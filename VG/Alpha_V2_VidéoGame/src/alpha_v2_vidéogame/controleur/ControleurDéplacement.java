/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alpha_v2_vidéogame.controleur;

import alpha_2v_videogame.modèle.carte.Map;
import alpha_v2_videogame.modèle.ElementMovible;
import alpha_v2_videogame.modèle.ElementUse;
import alpha_v2_videogame.modèle.personnage.EtatDeplacement;
import alpha_v2_vidéogame.mécanisme.FloatPoint;
import alpha_v2_vidéogame.mécanisme.SérieDeMéthodeStatic;
import java.awt.event.ActionEvent;

/**
 *
 * @author Eloi
 */
public class ControleurDéplacement extends Controleur {

    private ControleurPrincipal controleurPrincipal;
    private ObjectInScreen gestionnaireDesElementsEnJeu;
    private GestionCollision bigbang;
    private Map mapCourante;
    

    public ControleurDéplacement(ControleurPrincipal aThis, ObjectInScreen gestionnaireDesElementsEnJeu) {
        super();
        this.controleurPrincipal = aThis;
        this.gestionnaireDesElementsEnJeu = gestionnaireDesElementsEnJeu;
        bigbang = new GestionCollision(controleurPrincipal);


    }

    {

    }

    void signalCalcul(ActionEvent e) {

        if (mapCourante==null)mapCourante= controleurPrincipal.getGameDataControler().getMapCourante();

        for (ElementUse tmp : gestionnaireDesElementsEnJeu.getMovible()) {
            if (tmp.getObject() instanceof ElementMovible) {
                ElementMovible tmpMovingElement = (ElementMovible) tmp.getObject();
                // calcule du point
                FloatPoint tmpPt = EtatDeplacement.getPositionOfThis(
                        tmpMovingElement.getPosition(),
                        tmpMovingElement.getEtatDeplacement(), 
                        SérieDeMéthodeStatic.getUnitéTempsPhysiques());

                tmpPt = bigbang.gestionDeLaCollision(mapCourante, tmpMovingElement, tmpPt);
                tmpMovingElement.setXY(tmpPt.x, tmpPt.y);

                // gestion collision
                // replacement

            }

        }



    }
}
