/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alpha_v2_vidéogame.controleur;

import alpha_2v_videogame.modèle.carte.Map;
import alpha_2v_videogame.modèle.carte.Tuile;
import alpha_v2_videogame.modèle.ElementComposant;
import alpha_v2_videogame.modèle.ElementMovible;
import alpha_v2_videogame.modèle.ElementUse;
import java.util.Collections;
import java.util.LinkedList;

/**
 *
 * @author Eloi
 */
public class ObjectInScreen extends Controleur {

    /**
     * liste des éléments décoratifs présent à l'écran: background, élément décors, map, tuile ...
     */
    private LinkedList<ElementUse> décor;
    /**
     * liste des éléments à déplacement présent à l'écran: hero, ennemie, projectil ...
     */
    private LinkedList<ElementUse> movible;
    /**
     * liste des éléments à supprimer des objects à raffraichir une fois la méthode de suppression appelée
     */
    private LinkedList<ElementUse> elementToSupprimer;

    public ObjectInScreen() {
        décor = new LinkedList<ElementUse>();
        movible = new LinkedList<ElementUse>();
        elementToSupprimer = new LinkedList<ElementUse>();
    }

    public LinkedList<ElementUse> getDécor() {
        return décor;
    }

    public LinkedList<ElementUse> getMovible() {
        return movible;
    }


    


    /**
     * Ajouter un objet avec aucune temps limites de rafraichissement(rafraichissement inifinie conditionnel)
     * @param o
     */
    public void ajouterObject(Object o) {

        addObject(o, new ElementUse(o));
    }

    /**
     * Ajouter un objet avec un nombre de passage définit
     * @param o
     * @param pourNombreFrameX
     */
    public void ajouterObject(Object o, int pourNombreFrameX) {
        addObject(o, new ElementUse(o, pourNombreFrameX));
    }

    /**
     * méthode d'ajout privée appliquée derrière la méthode ajouterObject
     * @param o
     * @param eu
     */
    private void addObject(Object o, ElementUse eu) {
        notifyChange("add:"+o.toString());
        if (o instanceof Map
                || o instanceof Tuile) {
            // vérifier si o est pas déjà dans la liste
            // WARNING
            décor.add(new ElementUse(o));
        } else if (o instanceof ElementMovible) {
            ((ElementComposant) o).addObserver(eu);

            // vérifier si o est pas déjà dans la liste
            // WARNING
            movible.add(new ElementUse(o));
        }



        Collections.sort(décor);
        Collections.sort(movible);
    }

/**
 * Si cette méthode est appelée, elle supprime tout les éléments qui on été marqué comme inutile
 */
    public void supprimerObjetRésidu() {

        for (ElementUse tmp :getDécor())
        {
            if (tmp.isToDelete()) elementToSupprimer.add(tmp);
        }


        for (ElementUse tmp :getMovible())
        {

            if (tmp.isToDelete()) elementToSupprimer.add(tmp);

        }

        for (ElementUse o : elementToSupprimer) {
            if(!décor.isEmpty())
            décor.remove(o);
            if(!movible.isEmpty())
            movible.remove(o);
            notifyChange("supprimer:"+o.toString());
        }
        elementToSupprimer.clear();

    }
}
