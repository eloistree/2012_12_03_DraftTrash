/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_2v_videogame.modèle.carte;

import alpha_v2_videogame.modèle.ElementComposant;
import alpha_v2_videogame.modèle.ElementMovible;
import alpha_v2_videogame.modèle.personnage.Personnage;
import java.awt.Image;

/**
 *
 * @author Eloi & Fab
 */
public class Tuile extends ElementComposant {

    private  int id=0;
    boolean touchée;


    public Tuile(String nom, Image [] images, int id, boolean solide)
    {
        super(nom, images, solide);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Image getCurrentImage() {
       return getImage(0);
    }

    public void touchée(ElementMovible perso)
    {
        touchée = true;
        // gestion d'action si réaction au personange


    }


    

}