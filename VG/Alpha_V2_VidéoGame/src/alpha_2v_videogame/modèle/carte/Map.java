/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alpha_2v_videogame.modèle.carte;

import alpha_v2_videogame.modèle.ElementComposant;
import alpha_v2_videogame.modèle.personnage.Personnage;
import alpha_v2_vidéogame.modèle.ConstantesDuJeu;
import alpha_v2_vidéogame.mécanisme.FloatPoint;
import java.util.ArrayList;
import java.util.HashMap;


/**
 *
 * @author Eloi & Fab
 */
public class Map implements ConstantesDuJeu {

    public static float getLimitTuileCourante(float[]point, boolean limteHorizontal, boolean limteInférieur ) {
       int p_X=((int)point[0])/TAILLE_TUILE;
       int p_Y=((int)point[1])/TAILLE_TUILE;
       if (limteHorizontal && limteInférieur ) return (p_X) *TAILLE_TUILE;
       if(limteHorizontal && !limteInférieur) return (p_X+1) * TAILLE_TUILE;
        if(!limteHorizontal && limteInférieur) return (p_Y) * TAILLE_TUILE;
        if(!limteHorizontal && !limteInférieur) return (p_Y+1) * TAILLE_TUILE;
        return -1;
    }

    private String nom;
    private int colomn ;
    private int ligne ;
    private int[][] level;
    private HashMap<Integer, Tuile> collectionDeTuille;

   
    public Map(String nom,int ligne, int colomn, HashMap<Integer, Tuile> collectionTuile) {
        this.nom=nom;
        this.collectionDeTuille = collectionTuile;
        this.colomn = colomn;
        this.ligne = ligne;



        for (int i = 0; i < ligne; i++) 
            for (int j = 0; j < colomn; j++) {
                level[i][j] = 0;
            }


        

    }

    public Map(String nom,int[][] level,int ligne, int colomn,HashMap<Integer, Tuile> collectionTuile) {
        this.nom= nom;
        this.collectionDeTuille = collectionTuile;
        this.level = level;
        this.ligne = ligne;
        this.colomn = colomn;


    }


    public String getNom() {
        return nom;
    }

    public int getLargeur() {
        return colomn *TAILLE_TUILE;

    }

    public int getHauteur() {
        return ligne * TAILLE_TUILE;

    }

    public int getColomn() {
        return colomn;
    }


    public int getLigne() {
        return ligne;
    }

    public int getIdTo(int ligne, int colonne)
    {
        if (  (ligne< this.ligne && ligne>=0)
                &&
              (colonne< this.colomn && colonne>=0)

                )
        return level[ligne][colonne];
        else return -1;


    }
 public int getIdTo(float x, float y)
    {
         int p_X=((int)x)/TAILLE_TUILE;
         int p_Y=((int)y)/TAILLE_TUILE;
        if (  (p_X< this.ligne*TAILLE_TUILE && p_X>=0)
                &&
              (p_Y< this.colomn*TAILLE_TUILE && p_Y>=0)

                )
        return level[p_X][p_Y];
        else return -1;


    }

     public boolean pointInTheMap(Personnage perso,FloatPoint p)
    {

        if ((p.x  >= 0 && p.x + perso.getLargeur()< this.getLargeur() )
                && (p.y  >= 0 && p.y + perso.getHauteur()< this.getHauteur()) ) {
            return true;
        }
        return false;
    }

     private ArrayList<Tuile> tmpArray  = new ArrayList<Tuile>();
     public   ArrayList<Tuile>  getAllTuilesTouch(ElementComposant element, FloatPoint p)
     {
         tmpArray.clear();
         int hauteurEnTuille = (int)(((int) element.getHauteur())/TAILLE_TUILE)+1;
         int largeurEnTuille = (int)(((int) element.getLargeur())/TAILLE_TUILE)+1;
         int p_X=((int)p.x)/TAILLE_TUILE;
         int p_Y=((int)p.y)/TAILLE_TUILE;

         for(int i = p_X; i<= p_X+largeurEnTuille;i++)
           for(int j = p_Y; j<= p_Y+hauteurEnTuille;j++)
           {
               int tuileID = getIdTo(i, j);
               if(tuileID>=0)
               {
                   if (tmpArray.indexOf( collectionDeTuille.get(tuileID) )<0)
                   tmpArray.add( collectionDeTuille.get(tuileID));
               }
               
           }
         return tmpArray;
     }

    

}