/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alpha_v2_videogame.modèle;

import alpha_v2_videogame.modèle.animation.Animation;
import alpha_v2_vidéogame.modèle.ConstantesDuJeu;
import alpha_v2_vidéogame.mécanisme.FloatPoint;
import java.awt.Image;
import java.util.Observable;

/**
 *
 * @author Eloi & Fab
 */
public abstract class ElementComposant extends Observable implements ConstantesDuJeu {

    protected float largeur;
    protected float hauteur;
    protected float positionX;
    protected float positionY;
    protected String nom;
    protected Image[] images;
    protected Animation[] animations;
    protected int transparence;
    // note que l'on pourrait créer un énum pour (solide, liquide, vide ...)
    protected boolean solide;

    {
        positionX = 0;
        positionY = 0;
        transparence = 100;
        solide = true;

        animations = new Animation[1];

    }

    public  ElementComposant(String nom, float largeur, float hauteur, Image[] images, boolean solide) {

        this.nom = nom;
        this.largeur = largeur;
        this.hauteur = hauteur;
        this.images = images;
        this.solide = solide;
    }
      public ElementComposant(String nom, Image[] images,boolean solide) {
        this(nom,TAILLE_TUILE,TAILLE_TUILE,images, solide);

    }

    abstract public Image getCurrentImage();

     public Image getImage(int i)
     {
         if (images==null ||i<0 || i > images.length) return null;
         return images[i];

     }

      public Image getImageAnimation(long i)
     {
          return animations[0].getGoodImage(i);

     }


     public void setAnimation(Image [] iamgesAnim, int vitesse)
     {
        animations[0]= new Animation(iamgesAnim,vitesse);
     
     }

    public FloatPoint getPosition() {
        return new FloatPoint(getPositionX(), getPositionY());
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public boolean isSolide() {
        return solide;
    }

    public float getHauteur() {
        return hauteur;
    }

    public float getLargeur() {
        return largeur;
    }

    public String getNom() {
        return nom;
    }

    public int getTransparence() {
        return transparence;
    }


     

}
