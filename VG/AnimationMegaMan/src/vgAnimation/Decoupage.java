package vgAnimation;

public class Decoupage {


	protected int getX() {
		return x;
	}

	protected int getY() {
		return y;
	}

	protected int getWidthOneImage() {
		return width/repetition;
	}
	protected int getWidth() {
		return width;
	}

	protected int getHeight() {
		return height;
	}

	protected int getRepetition() {
		return repetition;
	}



	
	private int x,y;
	private int width, height;
	private int repetition;
	
	
	public Decoupage( int x, int y, int width, int height) {
		this( x, y, width, height,1);
	}
	
	public Decoupage( int x, int y, int width, int height,
			int repetition) {
		super();
		

		if(x<0)throw new IllegalStateException("x doit �tre positif");
		this.x = x;

		if(y<0)throw new IllegalStateException("y doit �tre positif");
		this.y = y;

		if(width<0)throw new IllegalStateException("largeur doit �tre positif");
		this.width = width;

		if(height<0)throw new IllegalStateException("hauteur doit �tre positif");
		this.height = height;
		if(repetition<1)throw new IllegalStateException("Au moins un r�p�tition d'image � d�couper");
		this.repetition = repetition;
	}
	
	
	
	
}
