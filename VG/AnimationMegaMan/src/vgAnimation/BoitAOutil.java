package vgAnimation;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 *
 * @author java06
 */
public class BoitAOutil {
      static public int nombreAleatoireEntreA_B(int aMin,int bMax)
    {
        Random r = new Random();
        return aMin + r.nextInt(bMax - aMin);
       
    }
      
      static public int distanceEntreDeuxPoints( Point p1, Point p2)  
      {     
          int y=0, x=0;
              if ( p1.x>p2.x) x=p1.x-p2.x;
              else  x=p2.x-p1.x;
              
              if ( p1.y>p2.y) y=p1.y-p2.y;
              else  y=p2.y-p1.y;
              
         
        return (int) Math.sqrt((double) ((x*x) + (y*y)) );
      
      }
        static public Image couperUneImage(Image image, int x, int y, int widthOfNewSection, int heightOfNewSection) {

        BufferedImage imageOriginal;
        BufferedImage imageFinal=new BufferedImage(1,1, BufferedImage.TYPE_INT_RGB);
        if (widthOfNewSection<1) widthOfNewSection=1;
        if (heightOfNewSection<1) heightOfNewSection=1;
        if (image != null && widthOfNewSection > 0 && heightOfNewSection > 0) {
            imageOriginal = toBufferedImage(image);
           
                imageFinal = new BufferedImage(widthOfNewSection, heightOfNewSection, imageOriginal.getType());
            
            for (int i = 0; i < widthOfNewSection; i++) {
                for (int j = 0; j < heightOfNewSection; j++) {
                    if (x + i < imageOriginal.getWidth() && x + i > 0
                            && y + j < imageOriginal.getHeight() && y + j > 0) {
                        imageFinal.setRGB(i, j, imageOriginal.getRGB(x + i, y + j));
                    }
                }

            }
        }


        return (Image) imageFinal;
    }
        
        
        static public Image inverserImage(Image image) {

            BufferedImage imageOriginal;
            BufferedImage imageFinal=new BufferedImage(1,1, BufferedImage.TYPE_INT_RGB);
          
            if (image != null) {
                imageOriginal = toBufferedImage(image);
               
                    imageFinal = new BufferedImage(imageOriginal.getWidth(), imageOriginal.getHeight(), imageOriginal.getType());
                
                for (int i = 0; i < imageOriginal.getWidth(); i++) {
                    for (int j = 0; j < imageOriginal.getHeight(); j++) {
                   
                            imageFinal.setRGB(i, j, imageOriginal.getRGB(imageOriginal.getWidth()-1-i,j));
                        
                    }

                }
            }


            return (Image) imageFinal;
        }


    public static BufferedImage toBufferedImage(Image image) {
        /*
         * On test si l'image n'est pas déja une instance de BufferedImage
         */
        if (image instanceof BufferedImage) {
            /*
             * cool, rien à faire
             */
            return ((BufferedImage) image);
        } else {
            /*
             * On s'assure que l'image est complètement chargée
             */
            image = new ImageIcon(image).getImage(); //(1)

            /*
             * On crée la nouvelle image
             */
            BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
            Graphics g = bufferedImage.createGraphics(); //(2)
            g.drawImage(image, 0, 0, null);
            g.dispose();

            return (bufferedImage);
        }
    }
       public Image getImageLocal(String urlImage)
      {
        return new javax.swing.ImageIcon(getClass().getResource(urlImage)).getImage() ;
      }
}
