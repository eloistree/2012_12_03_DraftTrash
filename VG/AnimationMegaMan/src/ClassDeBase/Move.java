package ClassDeBase;

import java.awt.Point;

/**
 * Author: VG42(Str�e ELoi) 3 juin 2012: Move est une classe qui doit
 * repr�senter le d�placement d'une entit�: vitesse, direction, position
 * d'arriv�e si condition ...
 */
public class Move {

	/**
	 * Author: VG42(Str�e ELoi) 3 juin 2012: repr�sente la vitesse en
	 * pixel/seconde sur l'axe horizontale
	 */
	private int vxInPx = 0;

	/**
	 * Author: VG42(Str�e ELoi) 3 juin 2012: repr�sente la vitesse en
	 * pixel/seconde sur l'axe verticale
	 */
	private int vyInPx = 0;

	/**Author: VG42(Str�e ELoi) 3 juin 2012:  repr�sente la direction grosomodo de l'entit�. */
	private Direction direction = Direction.NOTDIRECTION;

	public enum Direction {
		NOTDIRECTION, NORTH, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST, NORTHWEST
	};
	
	

	public Direction getDirection() {
		return direction;
	}

	public int getVxInPx() {
		return vxInPx;
	}

	public void setVxInPx(int vxInPx) {
		this.vxInPx = vxInPx;
		setDirection();
	}

	private void setDirection() {

		if(vxInPx>=0)
		{
			
			
		}
		else if (vxInPx<0)
		{
			
			
			
		}
		
	}

	public int getVyInPx() {
		return vyInPx;
	}

	public void setVyInPx(int vyInPx) {
		this.vyInPx = vyInPx;
		setDirection();
	}

	static Point getPosition(Point origine, Move mov, int timeEnS) {

	}

}
