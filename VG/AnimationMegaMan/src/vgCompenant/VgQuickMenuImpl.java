package vgCompenant;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JComponent;
import javax.swing.JFrame;

import vgCompenant.VgQuickMenuButton.Couleur;


public class VgQuickMenuImpl extends JComponent implements VgQuickMenu {

	@Override
	public String toString() {
		
		StringBuilder tmp= new StringBuilder();
		for (Page c : pages) {
			tmp.append("\n"+c);
			
		}
		return "VgQuickMenuImpl [MAXPAGES=" + MAXPAGES + ", MAXCELLULES="
				+ MAXCELLULES + "]"+tmp;

		
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 8352583198378931503L;
	final public int MAXPAGES = 3;
	final public int MAXCELLULES = 6;
	private int curseurOnPage=1;
	private int curseurOnCellule=0;
	private VgQuickMenuButton [] butts = new VgQuickMenuButton[] {new VgQuickMenuButton(),new VgQuickMenuButton(),new VgQuickMenuButton(),new VgQuickMenuButton(),new VgQuickMenuButton(),new VgQuickMenuButton()};

	public Page[] pages;
	
	
	public void nextPage()
	{
		curseurOnPage++;
		if (curseurOnPage>=MAXPAGES)curseurOnPage=MAXPAGES-1;
		
	}
	public void previousPage()
	{
		curseurOnPage--;
		if (curseurOnPage<0)curseurOnPage=0;
		
	}
	public void nextCellule()
	{
		curseurOnCellule++;
		if (curseurOnCellule>=MAXCELLULES)curseurOnCellule=0;
		
	}
	public void previousCellule()
	{curseurOnCellule--;
	if (curseurOnCellule<0)curseurOnCellule=MAXCELLULES-1;
		
	}

	public VgQuickMenuImpl() {
		this.setFocusable(false);
		pages = new Page[MAXPAGES];

		for (int i = 0; i < pages.length; i++) {
			pages[i] = new Page(MAXCELLULES);
			pages[i].setNom("");
			
		}

		this.setMinimumSize(new Dimension(265, 165));
		this.setSize(265, 165);
		this.setLayout(null);
		butts[0].setBounds(180, 0, 70, 45);
		butts[0].setText("Bonjours");
		this.add(butts[0]);
		
		butts[1].setBounds(195, 60, 70, 45);
		butts[1].setText("Bonjours");
		this.add(butts[1]);
		
	
		
		butts[2].setBounds(180,120, 70, 45);
		butts[2].setText("Bonjours");
		this.add(butts[2]);
		
		butts[3].setBounds(15, 120, 70, 45);
		butts[3].setText("Bonjours");
		this.add(butts[3]);
		
	
		
		butts[4].setBounds(0, 60, 70, 45);
		butts[4].setText("Bonjours");
		this.add(butts[4]);
		
	
		
		butts[5].setBounds(15, 0, 70, 45);
		butts[5].setText("Bonjours");
		this.add(butts[5]);

	}

	@Override
	public void clearAll() {
		for (Page p : pages) {
			p.reset();
		}

	}

	public void setNamePage(int i, String nom) {
		if (nom != null)
			if (i >= 0 && i < MAXPAGES)
				pages[i].setNom(nom);

	}

	@Override
	public boolean add(String acronyme, String actionCommande) {
		int indice = -1;
		int i = 0;
		while (indice < 0 && i < pages.length) {
			indice = pages[i].giveMeFreeSpot();
			i++;
		}
		return add(acronyme, actionCommande,--i, indice);
	}

	@Override
	public boolean add(String acronyme, String actionCommande, int page) {
		int indice = -1;
	
			indice = pages[page].giveMeFreeSpot();
		
		return add(acronyme, actionCommande,page, indice);
	}

	@Override
	public boolean add(String acronyme, String actionCommande, int page,
			int cellule) {
		boolean ok = false;
		if (page >= 0 && page < MAXPAGES && cellule >= 0 && cellule < MAXCELLULES)
		if (acronyme != null && actionCommande != null) {

			ok=pages[page].setCellule(cellule, acronyme, actionCommande);
			
		}

		
		return ok;
	}
	
	

	@Override
	public void paint(Graphics arg0) {
		int i=0;
		for (VgQuickMenuButton butt : butts) {
			if(i==curseurOnCellule)butt.c=Couleur.orange;
			else
			{
				if (curseurOnPage==0)butt.c=Couleur.green;
				else if (curseurOnPage==1)butt.c=Couleur.yellow;
				else butt.c=Couleur.red;
				
				
			}
			System.out.println(curseurOnPage+"."+i+"."+pages[curseurOnPage].getCellule(i).getName());
			butt.setText(pages[curseurOnPage].getCellule(i).getName());
			butt.setActionCommand(pages[curseurOnPage].getCellule(i).getCommande());
			i++;
		}
		
		super.paint(arg0);
		
		
		
		
	}

	public static void main(String[] args) {

		VgQuickMenuImpl menu = new VgQuickMenuImpl();
		for (int i = 0; i <10; i++) {

			menu.add(""+i, "SIGNAL:"+i);
		}
		menu.setNamePage(0, "Signal");
		menu.setNamePage(1, "Action");
		menu.setNamePage(2, "Menu");
		
		menu.add("help", "SIGNAL:HELP");
		menu.add("go", "SIGNAL:GO");
		menu.add("YES", "SIGNAL:YES");
		menu.add("EXIT", "MENU:QUITE", 2, 5);
		
		System.out.println("  "+menu);
		
		
		JFrame f  = new JFrame();
		System.out.println(f.getHeight()+":"+f.getWidth());
	

		f.setSize(menu.getMinimumSize().width+5, menu.getMinimumSize().height+25);
		f.setVisible(true);
		menu.setBackground(Color.white);
		
		f.add(menu);

	}
}
