package vgCompenant;


class Page {

	@Override
	public String toString() {
		StringBuilder tmp= new StringBuilder();
		for (Cellule c : cellules) {
			tmp.append(c+"\t");
			
		}
		return "Page [nom=" + nom +"] :"+tmp;
	}

	private String nom;
	private Cellule[] cellules;

	public Page(int nombreCellule) {

		cellules = new Cellule[nombreCellule];

		for (int i = 0; i < cellules.length; i++) {
			cellules[i] = new Cellule();
		}

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean setCellule(int i, String nom, String cmdMsg) {
		boolean ok= false;
		if (nom != null && cmdMsg != null)
			if (i > -1 && i < cellules.length) {
				cellules[i].setName(nom);
				cellules[i].setCommande(cmdMsg);
				ok=true;
			}
		return ok;
	}
	
	public void selectCellule(int i) {
		
			if (i > -1 && i < cellules.length) {
				for (Cellule c : cellules) {
					c.setSelected(false);

				}
				
				cellules[i].setSelected(true);

			}

	}

	public void reset() {
		for (Cellule c : cellules) {
			c.reset();

		}

	}

	public int giveMeFreeSpot() {
		int i=0;
		for (Cellule c : cellules) {
			
			if("".equals(c.getName())) return i;
			i++;
		}
		return -1;
	}

	public Cellule getCellule(int i) {
		if (i > -1 && i < cellules.length) {
			return cellules[i];
			
		}
		return null;
	}

}
