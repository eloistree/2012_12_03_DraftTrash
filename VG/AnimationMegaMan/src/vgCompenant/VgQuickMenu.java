package vgCompenant;

public interface VgQuickMenu {
	
	public void clearAll();
	public boolean add(String acronyme, String actionCommande);
	public boolean add(String acronyme, String actionCommande,int page);
	public boolean add(String acronyme, String actionCommande,int page,int cellule);

}
