package vgInterface;

import java.awt.Image;

import vgAnimation.Animation;

public interface Animable {

	/**
	 * Author: VG42(Str�e ELoi) 3 juin 2012: La classe doit pouvoir retourner
	 * l'animation principale qui la compose
	 */
	public Animation getAnimation();

	/**
	 * Author: VG42(Str�e ELoi) 3 juin 2012: Si on lui donne un temps, la classe
	 * doit pouvoir � partir de l'animation retourner une image qu'elle d�finit
	 * comme par defaut
	 */
	public Image getImageAt(int time);

	/**
	 * Author: VG42(Str�e ELoi) 3 juin 2012: La classe doit pouvoir retourner
	 * une image par defaut � tout moment qui la repr�sente
	 */
	public Image getImageDefautl();
}

