package vgInterface;

/**
 * Author: VG42(Str�e ELoi) 3 juin 2012: TimeLinked � pour but lier un objet au temps.
 * L'interface oblige l'objet � retourne quand et depuis quand : il a �t� cr�er, action ...
 *   */
public interface TimeLinked {

	/**Author: VG42(Str�e ELoi) 3 juin 2012:  Retourne le moment � la chose � commencer  */
	public void getTimeWhenStart();
   /**Author: VG42(Str�e ELoi) 3 juin 2012:  Retourne depuis combien de temps la chose � commencer selon le temps �couler (=now)  */
	public void getSinceWhenStart(long now);
	/**Author: VG42(Str�e ELoi) 3 juin 2012: Remet le temps � "0"  */
	public void resetTime();
	/**Author: VG42(Str�e ELoi) 3 juin 2012:  Remet le temps � now */ 
	public void resetTimeTo(long now);

}
