package vgOutilDeTest;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.Timer;

public class VgJFShowImages extends JFrame implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4158328291523635586L;
	private ArrayList<Image> images =new ArrayList<>();
	protected Timer timer= new Timer(1000, this);
	private int numero=0;
	
	{
		timer.start();
	this.setSize(new Dimension(200,	200));		
	}
	
	
	
	public VgJFShowImages() throws HeadlessException {
		super();
		// TODO Auto-generated constructor stub
	}
	public void reset()
	{
		images.clear();
		
	}
	public void addImage (Image i)
	{
		if (i != null)
		images.add(i);
		
	}
	
	
	
	
	public void paint(Graphics g)
	{
		super.paint(g);
		
		if (images.size()>0){
		Image tmp= images.get(numero%images.size());
			g.drawImage(tmp, 0,0, getWidth(), getHeight(), null);
			

			g.drawImage(tmp, getWidth()-tmp.getWidth(null)-20, getHeight()-tmp.getHeight(null)-20, tmp.getWidth(null), tmp.getHeight(null), null);
			g.setColor(Color.RED);
			g.drawRect( getWidth()-tmp.getWidth(null)-20, getHeight()-tmp.getHeight(null)-20, tmp.getWidth(null), tmp.getHeight(null));
		}
		else g.fillRect(0, 0, getWidth(), getHeight());
		
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
numero++;
repaint();
		
	}

}
