package vgqd.demineur.vAlpha.ressources;

public interface IRessource {
	public String getName();
	public String getDescription();
	public String getClassDescription();

}
