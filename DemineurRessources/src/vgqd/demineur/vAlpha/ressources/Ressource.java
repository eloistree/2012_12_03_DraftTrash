package vgqd.demineur.vAlpha.ressources;

public class Ressource implements IRessource{
	private String name="Unnamed ressource";
	private String description="Undescribe ressource";
	private String classdescription="Ressource is class used to stock information a object that can be link to some manager";
	public Ressource() {
		super();
	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ressource other = (Ressource) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Ressource [name=" + name + ", description=" + description + "]";
	}
	@Override
	public String getClassDescription() {
		return classdescription;
	}
	
	

	
	
}
