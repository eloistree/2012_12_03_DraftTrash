package vgqd.devtool.exception;

import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class ShortAlert extends JDialog implements ActionListener{

	public ShortAlert(Frame arg0, boolean arg1,String message) {
	this(arg0, message, 3);
	}
	public ShortAlert(Frame arg0,String message, int tempsEnS) {
		super(arg0, false);
		this.setTitle("Short message");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		JPanel p = new JPanel();
		p.setLayout(new GridLayout());
		p.add(new JLabel(message));
		this.setContentPane(p);
		Timer t =new Timer( tempsEnS*1000, this);
		this.setSize(300,100);
		this.setBackground(Color.WHITE);
		this.setVisible(true);
		
		t.start();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.dispose();
		
	}

}
