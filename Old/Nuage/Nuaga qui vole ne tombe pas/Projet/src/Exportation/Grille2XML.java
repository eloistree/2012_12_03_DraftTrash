/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Exportation;

import AllGUI.composant.ObjetPositionné;
import java.awt.Color;
import java.util.ArrayList;
import nuagequivolenetombepas.modèle.Grille;

/**
 *
 * @author eloistree
 */
public class Grille2XML {
    
 
    
   static public String getCodeXml(Grille grille, ArrayList<ObjetPositionné> tabObjet)
    {   
        
        String codeXML;
        codeXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
        //codeXML += "<!DOCTYPE calendar PUBLIC \"http://www.stree.be/eloi/NuageQuiVoleNeTombePas/GrilleFinalDTD.dtd\" >\n";
        codeXML += "<grille>\n";
            codeXML += "\t<dimension>\n";
                codeXML += "\t\t<largeur value=\"px\">"+grille.getWidth()+"</largeur>\n";
                codeXML += "\t\t<hauteur value=\"px\">"+grille.getHeight()+"</hauteur>\n";
            codeXML += "\t</dimension>\n";
            codeXML += "\t<background>\n";
                Color tmpColor = grille.getBackGround();
                     int r = tmpColor.getRed();
                     int g = tmpColor.getGreen();
                     int b = tmpColor.getBlue();
                        codeXML += "\t\t<red>"+    r +"  </red>\n";
                        codeXML += "\t\t<green>"+  g +"  </green>\n";
                        codeXML += "\t\t<blue>"+  b  +"  </blue>\n";
            codeXML += "\t</background>\n";
            
            for(ObjetPositionné tmp: tabObjet)
            {
            
            codeXML += "\t<objet>\n"; 
            codeXML += "\t"+tmp.getText()+"\n"; 
            
                codeXML += "\t\t<dimension>\n";
                    codeXML += "\t\t\t<largeur value=\"px\">"+tmp.getSize().width +"</largeur>\n";
                    codeXML += "\t\t\t<hauteur value=\"px\">"+tmp.getSize().height+"</hauteur>\n";
                codeXML += "\t\t</dimension>\n";
       
                codeXML += "\t\t<position>\n";
                    codeXML += "\t\t\t<x value=\"px\">"+tmp.getLocation().x+"</x>\n";
                    codeXML += "\t\t\t<y value=\"px\">"+tmp.getLocation().y+"</y>\n";
                codeXML += "\t\t</position>\n";
                       
                
               
                if(true);
                codeXML += "\t\t<autre>\n";
                    if(true);
                    codeXML += "\t\t\t<size>"+tmp.getFont().getSize()+"</size>\n";
                    
                    if(true);
                    codeXML += "\t\t\t<police>"+tmp.getFont().getFontName()+"</police>\n";
                    
                    if(true);
                    codeXML += "\t\t\t<couleur>\n";
                      tmpColor = tmp.getForeground();
                      r = tmpColor.getRed();
                      g = tmpColor.getGreen();
                      b = tmpColor.getBlue();
                        codeXML += "\t\t\t\t<red>"+    r +"  </red>\n";
                        codeXML += "\t\t\t\t<green>"+  g +"  </green>\n";
                        codeXML += "\t\t\t\t<blue>"+  b  +"  </blue>\n";
                    codeXML += "\t\t\t</couleur>\n";
                    if(tmp.getRotation()==0)
                         codeXML += "\t\t\t<vertical value=\"false\"/>\n";
                    else
                         codeXML += "\t\t\t<vertical value=\"true\"/>\n";
                    
                codeXML += "\t\t</autre>\n";
            
            codeXML += "\t</objet>\n";
       
                
            }
        codeXML += "</grille>\n";
        
        
        
            
    return codeXML;
        
      
    
    }
    
    
}
