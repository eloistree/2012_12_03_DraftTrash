/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nuagequivolenetombepas.modèle;

import java.awt.Color;
import java.util.Random;

/**
 *
 * @author eloistree
 */
public class Gamme {
    
    private String nom;
    private Color [] couleurs;
    
    
    public Gamme(String nom, Color [] couleurs)
    {
        this.nom=nom;
        int longeur =couleurs.length;
        if (longeur>5) longeur =5;
        this.couleurs= new Color[longeur];
        for(int i= 0;i<longeur;++i)
        {
            
        this.couleurs[i]=new Color(couleurs[i].getRed(),
                                    couleurs[i].getGreen(),
                                couleurs[i].getBlue() 
                                        );
        }
        
    }
    
    

    public Color[] getCouleurs() {
        return couleurs;
    }


    public String getNom() {
        return nom;
    }
    
    
    @Override
    public String toString()
    {
        return getNom();
        
     
    }

    
     public String getDescription()
    {
        String tmpS=getNom();
        
        for(Color tmpColor: getCouleurs())
        {
        tmpS+= ": r-"+tmpColor.getRed()+",g-"+tmpColor.getGreen()+",b-"+tmpColor.getBlue();
        }
        return tmpS;
    }

    public Color getRandomColor() {
        int aMin=0,bMax;
        bMax = couleurs.length;
        Random r = new Random();
        return couleurs[aMin + r.nextInt(bMax - aMin)];
    }

    public Gamme getCopyOf() {
      
        Gamme tmp = new Gamme(getNom(),getCopyOfColors());
        
        return tmp;
    }

    private Color[] getCopyOfColors() {
      
        Color[] colors  = new Color[couleurs.length];
       
        int i=0;
        for(Color tmp : couleurs)
        {colors[i]= new Color(tmp.getRed(),tmp.getGreen(),tmp.getBlue());
            
        
        i++;
        }
        
        return colors;
    }
    
    public boolean equals(Object o)
    {
        if (this==o) return true;
        if (o==null) return false;
        if (getClass() != o.getClass()) return false;
        Gamme tmp = (Gamme) o;
        
        int longeur=-1;
        // si ils on la même longeur et qu'ils existe
        if (tmp.getCouleurs()!=null&& this.getCouleurs()!=null &&
                ((longeur = tmp.getCouleurs().length ) != this.getCouleurs().length)
                && longeur>0)
        {
            for (int i = 0; i<longeur;i++)
            {
            if (tmp.getCouleur(i).getRed() != getCouleur(i).getRed() 
                    || tmp.getCouleur(i).getGreen() != getCouleur(i).getGreen()
                    || tmp.getCouleur(i).getBlue() != getCouleur(i).getBlue() )
                return false;
            
            }
            return true;
            
        }
        return false;
        
    }

    private Color getCouleur(int i) {
        if (couleurs  != null && i>=0 && i<couleurs.length && couleurs[i]!=null)
        return this.couleurs[i];
        return null;
    }
    
    
}
