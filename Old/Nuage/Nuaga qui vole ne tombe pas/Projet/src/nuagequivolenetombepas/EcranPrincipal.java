/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Vue.java
 *
 * Created on 28 sept. 2011, 10:26:56
 */
package nuagequivolenetombepas;

import AllGUI.composant.Gamme;
import AllGUI.fenetre.SelectionFichier;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import nuagequivolenetombepas.processus.LecteurDeTexte;
import java.io.IOException;
import java.util.Hashtable;
import javax.swing.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author eloistree
 */
public class EcranPrincipal extends javax.swing.JFrame implements ActionListener{

    private Brain brain;
    private ButtonGroup group2 ;
    private Timer timer;
    private ActionListener listenerOfTimer = this;
    private File image;
    private javax.swing.JLabel jImageDeFont = new javax.swing.JLabel();
    private javax.swing.JLabel jImageDeFont2 = new javax.swing.JLabel();
    /** Creates new form Vue */
    
    {
        group2  = new ButtonGroup();
        
        
    
    }
    
    
    public EcranPrincipal(Brain brain) {
        
          ImageIcon icfenetre = new javax.swing.ImageIcon(getClass().getResource("/elementExterieur/nuage.png"));
        setIconImage(icfenetre.getImage());
        
       
        
        
        this.brain=brain;
        initComponents();
        
        this.setSize(new Dimension(jPanel26.getWidth()+60,getHeight()+10));
        
        
        Color tmp = new Color(125,125,125);
      
        
        
        ImageIcon ic = new javax.swing.ImageIcon(getClass().getResource("/elementExterieur/nuage.jpg"));
        
        jImageDeFont.setIcon(ic);
        jImageDeFont2.setIcon(ic);
        
      
        getContentPane().add(jImageDeFont, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,   ic.getIconWidth(), ic.getIconHeight()));
        getContentPane().add(jImageDeFont2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0,  ic.getIconWidth(), ic.getIconHeight()));
        
        
        
        jLabel5.setText("Max : "+Brain.getLargeurMaxGrille());
        jLabel6.setText("Max : "+Brain.getHauteurMaxGrille());
        
        jComboBoxPolice.setModel(new javax.swing.DefaultComboBoxModel(Brain.getNamePolices()));
        
             
       
        définirLesGammesParDefautl();
     
        
        labelMaxMinSliders();
     
        
            group2.add(jCheckBox1);
            group2.add(jCheckBoxCouleurAvecSens);
           
        
        
        
        ButtonGroup group = new ButtonGroup();
            group.add(jRadioButtRandom);
            group.add(jRadioButtStructure);
            group.add(jRadioButtonAlphaStructure);
        
            
        brain.setPolice(((java.lang.String ) jComboBoxPolice.getSelectedItem()));
        this.jCheckBoxArticles.setSelected(brain.IsArticleAgree());
        this.jCheckBoxNombre.setSelected(brain.IsNombreAgree());
        this.jCheckBoxCaractèreSpéciaux.setSelected(brain.IsCarctSpecAgree());
        
        this.jRadioButtStructure.setSelected(true);
        brain.setStructureDuNuage("aléatoire");
 
        this.jTxtLeTexte.setText("Copier votre texte ici ou importer le avec le bouton ci-dessous.");
        
        
        // normalement, je dois initialiser les données avec la classe préférence utilisateur.
        jCheckBoxCouleurAvecSens.setSelected(brain.getCouleurSignificative());
        
        jSliderNombreMots.setValue(brain.getNombreDeMots());
        jSliderTailleMinimum.setValue(brain.getTailleMinimum());
        jLabNombreMotsMinim.setText (""+ jSliderNombreMots.getValue());
        jLabNombreMinim.setText (""+ jSliderTailleMinimum.getValue());
        
        jTextFieldHauteurNuage.setText(""+brain.getHauteurNuage());
        jTextFieldLargeurNuage.setText(""+brain.getLargeurNuage());
        
        
        
        
        
        
        
      
        timer = new Timer (40, listenerOfTimer);
        timer.setActionCommand("DEPLACERNUAGE");
        timer.start();
        
       
        
        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel26 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTxtLeTexte = new javax.swing.JTextPane();
        jPanel17 = new javax.swing.JPanel();
        jButtImporter = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jPanel19 = new javax.swing.JPanel();
        jButtValider = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jPanOption = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabelTailleMinimum = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jSliderTailleMinimum = new javax.swing.JSlider();
        jLabNombreMinim = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabelNombreMots = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jSliderNombreMots = new javax.swing.JSlider();
        jLabNombreMotsMinim = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldHauteurNuage = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldLargeurNuage = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jRadioButtRandom = new javax.swing.JRadioButton();
        jRadioButtStructure = new javax.swing.JRadioButton();
        jRadioButtonAlphaStructure = new javax.swing.JRadioButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jComboBoxPolice = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        jCheckBoxArticles = new javax.swing.JCheckBox();
        jCheckBoxNombre = new javax.swing.JCheckBox();
        jCheckBoxCaractèreSpéciaux = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanGamme = new javax.swing.JPanel();
        jComboBoxGamme = new javax.swing.JComboBox();
        jPanel7 = new javax.swing.JPanel();
        jCheckBoxCouleurAvecSens = new javax.swing.JCheckBox();
        jCheckBox1 = new javax.swing.JCheckBox();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Nuage qui vole ne tombe pas ;)");
        setResizable(false);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel26.setMaximumSize(new java.awt.Dimension(1000, 800));
        jPanel26.setMinimumSize(new java.awt.Dimension(600, 700));
        jPanel26.setPreferredSize(new java.awt.Dimension(600, 700));
        jPanel26.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel16.setLayout(new java.awt.BorderLayout());

        jTxtLeTexte.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTxtLeTexteFocusLost(evt);
            }
        });
        jScrollPane1.setViewportView(jTxtLeTexte);

        jPanel16.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel17.setMaximumSize(new java.awt.Dimension(32767, 20));
        jPanel17.setLayout(new java.awt.GridLayout(1, 0));

        jButtImporter.setText("Importer un texte");
        jButtImporter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtImporterActionPerformed(evt);
            }
        });
        jPanel17.add(jButtImporter);

        jTextField1.setEditable(false);
        jTextField1.setText("test.txt");
        jPanel17.add(jTextField1);

        jPanel19.setMaximumSize(new java.awt.Dimension(32767, 10));
        jPanel19.setPreferredSize(new java.awt.Dimension(154, 10));

        org.jdesktop.layout.GroupLayout jPanel19Layout = new org.jdesktop.layout.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 140, Short.MAX_VALUE)
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 35, Short.MAX_VALUE)
        );

        jPanel17.add(jPanel19);

        jButtValider.setText("Générer le nuage");
        jButtValider.setMaximumSize(new java.awt.Dimension(100, 50));
        jButtValider.setMinimumSize(new java.awt.Dimension(0, 0));
        jButtValider.setPreferredSize(new java.awt.Dimension(108, 35));
        jButtValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtValiderActionPerformed(evt);
            }
        });
        jPanel17.add(jButtValider);

        jPanel16.add(jPanel17, java.awt.BorderLayout.SOUTH);

        jPanel26.add(jPanel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 560, 210));

        jPanel20.setLayout(new java.awt.BorderLayout());

        jPanOption.setBackground(new java.awt.Color(204, 204, 204));
        jPanOption.setBorder(javax.swing.BorderFactory.createTitledBorder("Préférence"));
        jPanOption.setMaximumSize(new java.awt.Dimension(700, 700));
        jPanOption.setPreferredSize(new java.awt.Dimension(1188, 320));
        jPanOption.setLayout(new java.awt.GridLayout(2, 3));

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Limites"));
        jPanel3.setLayout(new java.awt.GridLayout(2, 1));

        jPanel10.setBackground(new java.awt.Color(204, 204, 204));
        jPanel10.setLayout(new java.awt.GridLayout(2, 3));

        jLabelTailleMinimum.setBackground(new java.awt.Color(204, 204, 204));
        jLabelTailleMinimum.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLabelTailleMinimum.setText("Taille mini. mot");
        jPanel10.add(jLabelTailleMinimum);

        jPanel14.setBackground(new java.awt.Color(204, 204, 204));

        org.jdesktop.layout.GroupLayout jPanel14Layout = new org.jdesktop.layout.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 88, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 41, Short.MAX_VALUE)
        );

        jPanel10.add(jPanel14);

        jSliderTailleMinimum.setBackground(new java.awt.Color(204, 204, 204));
        jSliderTailleMinimum.setMaximum(13);
        jSliderTailleMinimum.setPaintLabels(true);
        jSliderTailleMinimum.setPaintTicks(true);
        jSliderTailleMinimum.setValue(3);
        jSliderTailleMinimum.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSliderTailleMinimumStateChanged(evt);
            }
        });
        jPanel10.add(jSliderTailleMinimum);

        jLabNombreMinim.setBackground(new java.awt.Color(204, 204, 204));
        jLabNombreMinim.setText("0");
        jPanel10.add(jLabNombreMinim);

        jPanel3.add(jPanel10);

        jPanel11.setBackground(new java.awt.Color(204, 204, 204));
        jPanel11.setLayout(new java.awt.GridLayout(2, 2));

        jLabelNombreMots.setBackground(new java.awt.Color(204, 204, 204));
        jLabelNombreMots.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLabelNombreMots.setText("Nombre de mots");
        jPanel11.add(jLabelNombreMots);

        jPanel15.setBackground(new java.awt.Color(204, 204, 204));

        org.jdesktop.layout.GroupLayout jPanel15Layout = new org.jdesktop.layout.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 88, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 41, Short.MAX_VALUE)
        );

        jPanel11.add(jPanel15);

        jSliderNombreMots.setBackground(new java.awt.Color(204, 204, 204));
        jSliderNombreMots.setMaximum(50);
        jSliderNombreMots.setPaintLabels(true);
        jSliderNombreMots.setPaintTicks(true);
        jSliderNombreMots.setValue(3);
        jSliderNombreMots.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSliderNombreMotsStateChanged(evt);
            }
        });
        jPanel11.add(jSliderNombreMots);

        jLabNombreMotsMinim.setBackground(new java.awt.Color(204, 204, 204));
        jLabNombreMotsMinim.setText("0");
        jPanel11.add(jLabNombreMotsMinim);

        jPanel3.add(jPanel11);

        jPanOption.add(jPanel3);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Format (en px)", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 2, 12))); // NOI18N
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel12.setBackground(new java.awt.Color(204, 204, 204));
        jPanel12.setLayout(new java.awt.GridLayout(2, 2));

        jLabel3.setText("y (hauteur):");
        jPanel12.add(jLabel3);

        jTextFieldHauteurNuage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldHauteurNuageActionPerformed(evt);
            }
        });
        jTextFieldHauteurNuage.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldHauteurNuageFocusLost(evt);
            }
        });
        jPanel12.add(jTextFieldHauteurNuage);

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLabel6.setText("Max: 800");
        jPanel12.add(jLabel6);

        jPanel1.add(jPanel12);

        jPanel13.setBackground(new java.awt.Color(204, 204, 204));
        jPanel13.setLayout(new java.awt.GridLayout(2, 2));

        jLabel2.setText("x (largeur):");
        jPanel13.add(jLabel2);

        jTextFieldLargeurNuage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldLargeurNuageActionPerformed(evt);
            }
        });
        jTextFieldLargeurNuage.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldLargeurNuageFocusLost(evt);
            }
        });
        jPanel13.add(jTextFieldLargeurNuage);

        jLabel5.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLabel5.setText("Max: 1000");
        jPanel13.add(jLabel5);

        jPanel1.add(jPanel13);

        jPanOption.add(jPanel1);

        jPanel4.setBackground(new java.awt.Color(204, 204, 204));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Disposition"));
        jPanel4.setLayout(new java.awt.GridLayout(3, 0));

        jRadioButtRandom.setText("Aléatoire");
        jRadioButtRandom.setEnabled(false);
        jRadioButtRandom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtRandomActionPerformed(evt);
            }
        });
        jPanel4.add(jRadioButtRandom);

        jRadioButtStructure.setText("Structurée");
        jRadioButtStructure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtStructureActionPerformed(evt);
            }
        });
        jPanel4.add(jRadioButtStructure);

        jRadioButtonAlphaStructure.setText("Alphabétique");
        jRadioButtonAlphaStructure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonAlphaStructureActionPerformed(evt);
            }
        });
        jPanel4.add(jRadioButtonAlphaStructure);

        jPanOption.add(jPanel4);

        jPanel8.setBackground(new java.awt.Color(204, 204, 204));
        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder("Texte"));
        jPanel8.setLayout(new java.awt.GridLayout(2, 0));

        jPanel9.setBackground(new java.awt.Color(204, 204, 204));
        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Police", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(51, 102, 0)));
        jPanel9.setLayout(new java.awt.GridLayout(1, 0));

        jComboBoxPolice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Arial", "SansSerif", "Serif", "MonoSpaced", "Dialog", "DialogInput" }));
        jComboBoxPolice.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxPoliceItemStateChanged(evt);
            }
        });
        jComboBoxPolice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPoliceActionPerformed(evt);
            }
        });
        jPanel9.add(jComboBoxPolice);

        jPanel8.add(jPanel9);

        jPanOption.add(jPanel8);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Accepter ?", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 2, 13))); // NOI18N
        jPanel2.setLayout(new java.awt.GridLayout(3, 0));

        jCheckBoxArticles.setText("le, la ,les ,dans, ou...");
        jCheckBoxArticles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxArticlesActionPerformed(evt);
            }
        });
        jPanel2.add(jCheckBoxArticles);

        jCheckBoxNombre.setText("10, -10.2, 3.14...");
        jCheckBoxNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxNombreActionPerformed(evt);
            }
        });
        jPanel2.add(jCheckBoxNombre);

        jCheckBoxCaractèreSpéciaux.setText("é, è, à, î, ù...");
        jCheckBoxCaractèreSpéciaux.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxCaractèreSpéciauxActionPerformed(evt);
            }
        });
        jPanel2.add(jCheckBoxCaractèreSpéciaux);

        jPanOption.add(jPanel2);

        jPanel5.setBackground(new java.awt.Color(204, 204, 204));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Couleur"));
        jPanel5.setLayout(new java.awt.GridLayout(2, 0));

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gamme", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(51, 102, 0)));
        jPanel6.setLayout(new java.awt.GridLayout(1, 0));

        jPanGamme.setBackground(new java.awt.Color(204, 204, 204));
        jPanGamme.setBorder(javax.swing.BorderFactory.createCompoundBorder(null, javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0))));
        jPanGamme.setLayout(new java.awt.GridLayout(1, 0));
        jPanel6.add(jPanGamme);

        jComboBoxGamme.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jComboBoxGammeMouseMoved(evt);
            }
        });
        jComboBoxGamme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxGammeActionPerformed(evt);
            }
        });
        jPanel6.add(jComboBoxGamme);

        jPanel5.add(jPanel6);

        jPanel7.setBackground(new java.awt.Color(204, 204, 204));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ou autres couleurs", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(51, 102, 0)));
        jPanel7.setLayout(new java.awt.GridLayout(2, 0));

        jCheckBoxCouleurAvecSens.setText("Sémantique");
        jCheckBoxCouleurAvecSens.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxCouleurAvecSensActionPerformed(evt);
            }
        });
        jPanel7.add(jCheckBoxCouleurAvecSens);

        jCheckBox1.setText("Totalement aléatoire");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        jPanel7.add(jCheckBox1);

        jPanel5.add(jPanel7);

        jPanOption.add(jPanel5);

        jPanel20.add(jPanOption, java.awt.BorderLayout.CENTER);

        jPanel26.add(jPanel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 580, 400));

        getContentPane().add(jPanel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 580, 630));

        jMenu2.setText("Enoncé du travail");
        jMenu2.add(jSeparator1);

        jMenuItem4.setText("http://www.stree.be/eloi/NuageQuiVoleNeTombePas/");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jCheckBoxCaractèreSpéciauxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxCaractèreSpéciauxActionPerformed
javax.swing.JCheckBox tmp = (javax.swing.JCheckBox) evt.getSource();
   brain.IsCarctSpecAgree(tmp.isSelected()); 
}//GEN-LAST:event_jCheckBoxCaractèreSpéciauxActionPerformed

private void jCheckBoxArticlesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxArticlesActionPerformed
    javax.swing.JCheckBox tmp = (javax.swing.JCheckBox) evt.getSource();
    brain.IsArticleAgree(tmp.isSelected());
}//GEN-LAST:event_jCheckBoxArticlesActionPerformed

private void jButtValiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtValiderActionPerformed

    brain.soumettreUnTexte(this.jTxtLeTexte.getText());
    
    
}//GEN-LAST:event_jButtValiderActionPerformed

private void jButtImporterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtImporterActionPerformed

    String tmpURL=null;
    SelectionFichier tmplecteur = new SelectionFichier(this, true);
    if (null!= (tmpURL = tmplecteur.getChemin()))
    {
        jTextField1.setText(tmpURL);
        jTextField1.repaint();
        jTextField1.validate();
        jTxtLeTexte.setText(
            LecteurDeTexte.lecteurDeTexte(
                tmpURL
                )
            );
    }
   
}//GEN-LAST:event_jButtImporterActionPerformed

private void jTextFieldLargeurNuageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldLargeurNuageActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldLargeurNuageActionPerformed

private void jTextFieldHauteurNuageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldHauteurNuageActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldHauteurNuageActionPerformed

private void jSliderTailleMinimumStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSliderTailleMinimumStateChanged
        javax.swing.JSlider tmpSource = (javax.swing.JSlider) evt.getSource();
        int tailleMinimum = tmpSource.getValue();
        jLabNombreMinim.setText(""+tailleMinimum);
        brain.setTailleMinimum(tailleMinimum);
}//GEN-LAST:event_jSliderTailleMinimumStateChanged

private void jSliderNombreMotsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSliderNombreMotsStateChanged
        javax.swing.JSlider tmpSource = (javax.swing.JSlider) evt.getSource();
        int tailleMinimum = tmpSource.getValue();
        jLabNombreMotsMinim.setText(""+tailleMinimum);
        brain.setNombreDeMots(tailleMinimum);
       
}//GEN-LAST:event_jSliderNombreMotsStateChanged

private void jTextFieldLargeurNuageFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldLargeurNuageFocusLost
        javax.swing.JTextField tmpSource = (javax.swing.JTextField) evt.getSource();
        int tailleMinimum = Integer.parseInt( tmpSource.getText() );
        if(tailleMinimum>Brain.getLargeurMaxGrille()){ tailleMinimum =Brain.getLargeurMaxGrille(); jTextFieldLargeurNuage.setText(""+tailleMinimum);}
        
       
    brain.setLargeurNuage(tailleMinimum);


}//GEN-LAST:event_jTextFieldLargeurNuageFocusLost

private void jTextFieldHauteurNuageFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldHauteurNuageFocusLost
     javax.swing.JTextField tmpSource = (javax.swing.JTextField) evt.getSource();
        int tailleMinimum = Integer.parseInt( tmpSource.getText() );
        
        if(tailleMinimum>Brain.getHauteurMaxGrille() ){ tailleMinimum =Brain.getHauteurMaxGrille(); jTextFieldHauteurNuage.setText(""+tailleMinimum);}
       
    brain.setHauteurNuage(tailleMinimum);

}//GEN-LAST:event_jTextFieldHauteurNuageFocusLost

private void jRadioButtStructureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtStructureActionPerformed
 brain.setStructureDuNuage("structurer");
}//GEN-LAST:event_jRadioButtStructureActionPerformed

private void jRadioButtonAlphaStructureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonAlphaStructureActionPerformed
 brain.setStructureDuNuage("structurerAlphabétique");
}//GEN-LAST:event_jRadioButtonAlphaStructureActionPerformed

private void jCheckBoxCouleurAvecSensActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxCouleurAvecSensActionPerformed
 javax.swing.JCheckBox tmp = (javax.swing.JCheckBox) evt.getSource();
    brain.setIsColorTotalyRandom(!tmp.isSelected());
   brain.setCouleurSignificative(tmp.isSelected());
   brain.setGamme(null);
 this.jPanGamme.setVisible(!tmp.isSelected());
    
    
}//GEN-LAST:event_jCheckBoxCouleurAvecSensActionPerformed

private void jRadioButtRandomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtRandomActionPerformed

 brain.setStructureDuNuage("aléatoire");
}//GEN-LAST:event_jRadioButtRandomActionPerformed

private void jCheckBoxNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxNombreActionPerformed
javax.swing.JCheckBox tmp = (javax.swing.JCheckBox) evt.getSource();
   brain.IsNombreAgree(tmp.isSelected());    
   
}//GEN-LAST:event_jCheckBoxNombreActionPerformed

private void jComboBoxPoliceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPoliceActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jComboBoxPoliceActionPerformed

private void jComboBoxPoliceItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxPoliceItemStateChanged
    javax.swing.JComboBox tmp = (javax.swing.JComboBox) evt.getSource();
    brain.setPolice((String) tmp.getSelectedItem());
}//GEN-LAST:event_jComboBoxPoliceItemStateChanged

private void jComboBoxGammeMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxGammeMouseMoved
nuagequivolenetombepas.modèle.Gamme tmpGamme=
        (nuagequivolenetombepas.modèle.Gamme )
                ((javax.swing.JComboBox) evt.getSource()).getSelectedItem();
jPanGamme.removeAll();
jPanGamme.add( new Gamme(tmpGamme));
jPanGamme.repaint();

jPanGamme.validate();
System.out.print("a");

}//GEN-LAST:event_jComboBoxGammeMouseMoved

private void jComboBoxGammeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxGammeActionPerformed
nuagequivolenetombepas.modèle.Gamme tmpGamme=
        (nuagequivolenetombepas.modèle.Gamme )
                ((javax.swing.JComboBox) evt.getSource()).getSelectedItem();

   brain.setIsColorTotalyRandom(false);
   brain.setCouleurSignificative(false);
brain.setGamme(tmpGamme);
jPanGamme.removeAll();
jPanGamme.add( new Gamme(tmpGamme));
jPanGamme.repaint();
jPanGamme.validate();
jPanGamme.setVisible(true);



group2.clearSelection();

}//GEN-LAST:event_jComboBoxGammeActionPerformed

private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed

// envoyer une requête au terminal de mac os x d'ouvrir un site web avec cette adresse.
/*String urlString= "http://www.stree.be/eloi/NuageQuiVoleNeTombePas/";
String cmd = "open " + urlString;
        try {
            Process p = Runtime.getRuntime().exec(cmd);
        } catch (IOException ex) {
            Logger.getLogger(EcranPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
*/


    Desktop desktop = null;
    java.net.URI url;
    try {
    url = new java.net.URI("http://www.stree.be/eloi/NuageQuiVoleNeTombePas/index.html");
        if (Desktop.isDesktopSupported())
        {
        desktop = Desktop.getDesktop();
        desktop.browse(url);
        }
    }
    catch (Exception ex) {

    }
}//GEN-LAST:event_jMenuItem4ActionPerformed

private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
                                                
javax.swing.JCheckBox tmp = (javax.swing.JCheckBox) evt.getSource();   
   brain.setIsColorTotalyRandom(tmp.isSelected());
   brain.setCouleurSignificative(!tmp.isSelected());
   brain.setGamme(null);
    this.jPanGamme.setVisible(!tmp.isSelected());
}//GEN-LAST:event_jCheckBox1ActionPerformed

private void jTxtLeTexteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTxtLeTexteFocusLost

  
}//GEN-LAST:event_jTxtLeTexteFocusLost

private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized

    
    
}//GEN-LAST:event_formComponentResized

    public void setBrain(Brain brain) {
        this.brain = brain;
    }
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtImporter;
    private javax.swing.JButton jButtValider;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBoxArticles;
    private javax.swing.JCheckBox jCheckBoxCaractèreSpéciaux;
    private javax.swing.JCheckBox jCheckBoxCouleurAvecSens;
    private javax.swing.JCheckBox jCheckBoxNombre;
    private javax.swing.JComboBox jComboBoxGamme;
    private javax.swing.JComboBox jComboBoxPolice;
    private javax.swing.JLabel jLabNombreMinim;
    private javax.swing.JLabel jLabNombreMotsMinim;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelNombreMots;
    private javax.swing.JLabel jLabelTailleMinimum;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPanGamme;
    private javax.swing.JPanel jPanOption;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JRadioButton jRadioButtRandom;
    private javax.swing.JRadioButton jRadioButtStructure;
    private javax.swing.JRadioButton jRadioButtonAlphaStructure;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSlider jSliderNombreMots;
    private javax.swing.JSlider jSliderTailleMinimum;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextFieldHauteurNuage;
    private javax.swing.JTextField jTextFieldLargeurNuage;
    private javax.swing.JTextPane jTxtLeTexte;
    // End of variables declaration//GEN-END:variables

    private void définirLesGammesParDefautl() {
         
        for(nuagequivolenetombepas.modèle.Gamme tmp :Brain.getGammes())
        {
            jComboBoxGamme.addItem(tmp);
        
        }
    }

    private void labelMaxMinSliders() {
        

jSliderTailleMinimum.setPaintTicks(true);

java.awt.Font tmpFont = new java.awt.Font("Arial", 1, 10);

Hashtable labelTable = new Hashtable();
JLabel tmpLab = new JLabel(""+jSliderTailleMinimum.getMinimum());
tmpLab.setFont(tmpFont);
labelTable.put( new Integer( jSliderTailleMinimum.getMinimum() ), tmpLab );

tmpLab = new JLabel(""+jSliderTailleMinimum.getMaximum());
tmpLab.setFont(tmpFont);
labelTable.put( new Integer( jSliderTailleMinimum.getMaximum() ), tmpLab );
jSliderTailleMinimum.setLabelTable( labelTable );
jSliderTailleMinimum.setPaintLabels(true);
jSliderTailleMinimum.setPaintTicks(true);

labelTable = new Hashtable();
tmpLab = new JLabel(""+jSliderNombreMots.getMinimum());
tmpLab.setFont(tmpFont);
labelTable.put( new Integer( jSliderNombreMots.getMinimum() ), tmpLab );

tmpLab = new JLabel(""+jSliderNombreMots.getMaximum()) ;
tmpLab.setFont(tmpFont);
labelTable.put( new Integer( jSliderNombreMots.getMaximum() ), tmpLab);
jSliderNombreMots.setLabelTable( labelTable );
jSliderNombreMots.setPaintLabels(true);
        
    }

    
    private  int compteurDeplacement=0;
    @Override
    public void actionPerformed(ActionEvent ae) {
        compteurDeplacement+=1;
       
       
        
        Toolkit kit = Toolkit.getDefaultToolkit();
      
       
       
        
        if (jImageDeFont.getWidth()>0){
         int width = compteurDeplacement % (jImageDeFont.getWidth());
         int width2 = 1+(compteurDeplacement % (jImageDeFont.getWidth()))-jImageDeFont.getWidth();
         
      
         
         
       getContentPane().remove(jImageDeFont);
       getContentPane().remove(jImageDeFont2);
       getContentPane().add(jImageDeFont, new org.netbeans.lib.awtextra.AbsoluteConstraints(width, 0, jImageDeFont.getWidth(), 660));
       getContentPane().add(jImageDeFont2, new org.netbeans.lib.awtextra.AbsoluteConstraints(width2, 0, jImageDeFont2.getWidth(), 660));
        
        this.repaint();
        this.validate();
        }
    }
}
