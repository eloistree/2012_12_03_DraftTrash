/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nuagequivolenetombepas.processus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import nuagequivolenetombepas.modèle.MotPesé;

/**
 *
 * @author eloistree
 */
public class AnalyseurDeTexte {

    static public int withMinimumLengthOf;
    static public int withMaximumWordOf;
    
    static public boolean withCaractèreSpéciaux; // manger, boire...
    static public boolean withWord; // manger, boire...
    //static public boolean withCompoundWords; // lui-même...
    static public boolean withArticle;// le, la, pour, dedans...
    static public boolean withPseudo;// Eloi42, K1ll3r ...
    static public boolean withNumber;// 10, 12,34, 12.34
    //static public boolean withSpaceBetweenThousand;// 10 000, 1 000 000 ...
    
    public AnalyseurDeTexte()
    {
        withCaractèreSpéciaux=true;
        withWord=true;
      //  withCompoundWords=true;
        withArticle=true;
        withPseudo=true;
        withNumber=true;
       // withSpaceBetweenThousand=false;
        withMinimumLengthOf=0;
        withMaximumWordOf=-1;
    
    }
    
    public ArrayList<MotPesé> analyseMoiCa(String texte)
    {
        
       texte = texte.toLowerCase();
        char [] tableauDeTexte = texte.toCharArray();
        texte="";
        
        for(int ii =0 ; ii< tableauDeTexte.length;ii++)
        {
            char tmp = tableauDeTexte[ii];
            
            if(  !((tmp>='0'&&tmp<='9')||(tmp>='A'&&tmp<='Z')||(tmp>='a'&&tmp<='z') ||(tmp>=192&&tmp<=255)|| tmp==',' ||tmp=='-' || tmp=='.')  ) tableauDeTexte[ii]=' ';
            else
            {
                // si l'on ne veux pas de caractères spéciaux
                if(!withCaractèreSpéciaux && (tmp>=192&&tmp<=255))
                {
                    
                    tableauDeTexte[ii]=' ';
                    
                    int j=ii,k=ii;
                  
                  
                    while(k+1<tableauDeTexte.length && tableauDeTexte[k+1]!=' ')
                    {
                        tableauDeTexte[k+1]=' ';k++;}
                    
                      
                    while( j-1>0 && tableauDeTexte[j-1]!=' ')
                    {
                        tableauDeTexte[j--] =' ';}
                
                }
                if(!withNumber && ((tmp>='0'&&tmp<='9')||( tmp==',' ||tmp=='-' || tmp=='.')) )
                             tableauDeTexte[ii]=' ';
                
                if(withNumber)
                { 
                  if (tmp=='-' && !(tableauDeTexte[ii+1]>='0'&& tableauDeTexte[ii+1] <='9'))
                  {tableauDeTexte[ii]=' ';}
                  
                  if ( (tmp=='.'||tmp==',')
                          && ( ii+1<tableauDeTexte.length)
                          && !(tableauDeTexte[ii+1]>='0'&& tableauDeTexte[ii+1] <='9')
                          && !(tableauDeTexte[ii-1]>='0'&& tableauDeTexte[ii-1] <='9')
                          )
                  {tableauDeTexte[ii]=' ';}

                }
                
            } 
            
            
        }
        for(char tmp: tableauDeTexte)
            texte+=tmp;
        
        
        
        // Je supprime tout les articles de la langue française.
        if (!withArticle)
        {
            String[] articlesFrancais = {" le "," la "," les "," il "," ils "," vous "," nous "," un "," une ",
                " des "," dans "," sur "," je "," tu "," de "," du "," et "," pour "};
            
            for(String temp: articlesFrancais)
            {
                texte = texte.replace(temp," ");
        
            
            }
            
        }
        
    
        
        while(texte.indexOf("  ")>=0)
        {
            texte = texte.replace("  "," ");
        }
        
         HashMap<String , MotPesé> hashTmp= new HashMap<String , MotPesé>();
        
         for(String mot: texte.split(" "))
        {
           if(mot.length()>= withMinimumLengthOf)
            if(hashTmp.containsKey(mot)) hashTmp.get(mot).incOccurrence();
            else hashTmp.put(mot, new MotPesé(mot) );
            
            
        }
         ArrayList<MotPesé> aRetourner= new ArrayList<MotPesé>();
       
         for (Iterator i = hashTmp.keySet().iterator() ; i.hasNext() ; )
         {
            String cle = (String)i.next();
            aRetourner.add((MotPesé) hashTmp.get(cle));
            
         }
         
         
          Collections.sort(aRetourner, Collections.reverseOrder());
          
          if(withMaximumWordOf>0)
              for (int i=aRetourner.size()-1;i>=withMaximumWordOf;i--)
                  aRetourner.remove(i);
         
         
         
         
        
        
    return aRetourner;
    }
    
    
}
