/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nuagequivolenetombepas;


import AllGUI.composant.ObjetPositionné;
import de.javasoft.plaf.synthetica.SyntheticaBlackStarLookAndFeel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import nuagequivolenetombepas.modèle.MotAffiché;
import nuagequivolenetombepas.modèle.MotPesé;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import nuagequivolenetombepas.modèle.Gamme;
import nuagequivolenetombepas.modèle.Grille;
import nuagequivolenetombepas.modèle.PréférenceUtilisateur;
import nuagequivolenetombepas.processus.AnalyseurDeTexte;
import javax.swing.JApplet;
import javax.swing.UIManager;


/**
 *
 * @author eloistree
 */
public class Brain extends JApplet {

  
   
 

        private ArrayList<MotPesé> mots=null;
        private ArrayList<MotAffiché> motsAff=null ;
        private PréférenceUtilisateur préféUti =null;
        private Grille grille =null;
        private EcranPrincipal vue =null;
        private AnalyseurDeTexte ADeT =null;
        
        static private Gamme [] gammes;
        static final private String [] polices;
        static private Brain leControleurPrincipale;
    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
           try 
        {
         
         UIManager.setLookAndFeel(new SyntheticaBlackStarLookAndFeel());
         
        } 
        catch (Exception e) 
        {
          e.printStackTrace();
        }
       
      leControleurPrincipale = new Brain();

  
        
    }
    public void run() {
         leControleurPrincipale = new Brain();
         getGraphics().drawOval(0, 0, 100, 100);
    }
    public void destroy() {
       leControleurPrincipale =null;
       System.exit(0);

    }
    public void start() {
         leControleurPrincipale = new Brain();
         getGraphics().drawOval(0, 0, 100, 100);
    }
    
    static 
    {
       définirLesGammesParDefautl();
       polices = new String[] { "Arial", "SansSerif", "Serif", "MonoSpaced", "Dialog", "DialogInput" };
       
       Dimension dimScreen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
       Grille.setMaxSize(dimScreen.width-350,dimScreen.height-400);
       
    }
    
    
    {
    préféUti = new PréférenceUtilisateur();
    ADeT = new AnalyseurDeTexte();
    
      
    }
    public Brain()
    {
      
        vue= new EcranPrincipal(this);  
        vue.setVisible(true);
  
     /************ Préférence désiré de l'utilisateur ****************/
        
            
            this.setNombreDeMots(20);
            this.setTailleMinimum(3);
   
    }
    
    
    public void soumettreUnTexte(String leTexte)
    {
        /************ Texte analysé -> liste de mots pesé ****************/
       
     
        if (préféUti!=null)
        {
        
        ADeT.withMaximumWordOf=préféUti.getNbMots();
        ADeT.withCaractèreSpéciaux  =préféUti.isCaractèreSpéciaux();
        ADeT.withArticle = préféUti.isAgreeArticle();
        ADeT.withMinimumLengthOf = préféUti.getTailleMin();
        ADeT.withNumber = préféUti.isAgreeNombre();
       
            
        }
        
        mots= ADeT.analyseMoiCa(leTexte); 
          int minOccurence=0, maxOccurence=0;
        if( mots.size()>0)
            {
                minOccurence=mots.get(0).getOccurrence();
                for(MotPesé mot: mots)
                {
                    if(minOccurence>mot.getOccurrence())minOccurence =mot.getOccurrence();
                    if(maxOccurence<mot.getOccurrence())maxOccurence =mot.getOccurrence();

                }
                System.out.println("Minimum occurrence ="+minOccurence+", Maximum occurrence "+maxOccurence);
            }
        
        
    
/************ Décoration des mots affiché ****************/
        
        // J'initilalise la liste des mots affiché avec les mots pesé selectioner
        // pour un totale décidé par les péférences dans trié par occurence
        motsAff = new ArrayList<MotAffiché>();
       
        
        
         
         int i = 0;
        for(MotPesé mot: mots)
        {
           
            motsAff.add(i, new MotAffiché(mot.getMot(), mot.getOccurrence()));
            motsAff.get(i).setTauxPopularité(définirLaPopulartiéDuMot(minOccurence, maxOccurence,mot.getOccurrence()));
            motsAff.get(i).setSize(getRealSize(12,motsAff.get(i).getTauxPopularité() ));
            // ici, j'affecte les valeurs que l'utilisateur à choisit à tout les futures mots à afficher.
                 
                 motsAff.get(i).setPolice( préféUti.getPolice() );
          
                 i++;
        }
        
        
        
        
        
/************ Dépot des mots à afficher dans la grille ****************/
        // j'affiche les mots
        
        grille = new Grille(préféUti.getLargeurNuage(),préféUti.getHauteurNuage());
      
       
        
       ControlerNuage test= new ControlerNuage(grille,motsAff,préféUti);
          
        
    
    }
    
     public double  définirLaPopulartiéDuMot(int occurrenceLaPlusPetite, int occurenceLaPlusGrande, int occurrence)
    {
        double tauxPopularité=0.0;
         if(occurrenceLaPlusPetite>0 && occurenceLaPlusGrande>0)
         {
          if (occurrenceLaPlusPetite> occurenceLaPlusGrande){
              int tmp=occurrenceLaPlusPetite;
              occurrenceLaPlusPetite=occurenceLaPlusGrande;
              occurenceLaPlusGrande=tmp;
          }
          
          
          // taux popularité = (x/MaxOccurence)^8  (plus l'exposant est grand, plus les mots forts répété sont populaire.
          if(0!=occurenceLaPlusGrande-occurrenceLaPlusPetite)
         tauxPopularité= Math.pow(((double) occurrence)/((double)(occurenceLaPlusGrande-occurrenceLaPlusPetite)),1.0);
          
         // si foutage de gueule...
         if (tauxPopularité>=1.0) tauxPopularité=1.0;
         if (tauxPopularité<=0.0) tauxPopularité=0.0;
         return tauxPopularité;
         }
    return 0.0;
    }
    public int getRealSize(int size, double taux) {
        int tmp = size+(int) (((double)size)*taux*2);
        System.out.println(size+"+"+size +"*"+taux*2+"="+tmp);
        return tmp;
        
    }
 
    public int getLargeurNuage(){if (préféUti!=null) return préféUti.getLargeurNuage(); return 0;}
    public int getHauteurNuage(){if (préféUti!=null)  return préféUti.getHauteurNuage();return 0;}
    public final void setLargeurNuage(int value)
    {   if (préféUti!=null){
            préféUti.setLargeurNuage(value);
  
            
        }
    }
    public final void setHauteurNuage(int value)
    {
        if (préféUti!=null){
        préféUti.setHauteurNuage(value);
        
        }
    }
   
        
        
        
    public int getTailleMinimum(){if (préféUti!=null) return préféUti.getTailleMin(); return 0;}
    public int getNombreDeMots(){if (préféUti!=null)  return préféUti.getNbMots();return 0;}
    public final void setTailleMinimum(int value)
    {   if (préféUti!=null){
            préféUti.setTailleMin(value);
        }
    }
    public final void setNombreDeMots(int value)
    {
        if (préféUti!=null){
            préféUti.setNbMots(value);
        }
    }
    
     public boolean getCouleurSignificative(){if (préféUti!=null)  return préféUti.isCouleurSignificative();return false;}
    public final void setCouleurSignificative(boolean value)
    {   if (préféUti!=null){
            préféUti.setCouleurSignificative(value);
        }
    }
    
    public boolean IsColorTotalyRandom(){if (préféUti!=null)  return préféUti.isColorTotalyRandom();return false;}
    public final void setIsColorTotalyRandom(boolean value)
    {   if (préféUti!=null){
            préféUti.setIsColorTotalyRandom(value);
        }
    }
    
    static public int nombreAléatoireEntreA_B(int aMin,int bMax)
    {
        Random r = new Random();
        return aMin + r.nextInt(bMax - aMin);
       
    }


    public void setStructureDuNuage(String value) {
        préféUti.setStructureDuNuage(value);
    } 
    public String getStructureDuNuage() {
        return préféUti.getStructureDuNuage();
    }

   
    public boolean IsArticleAgree(){if (préféUti!=null) return préféUti.isAgreeArticle(); return false;}
    public  void IsArticleAgree(boolean value)
    {   if (préféUti!=null){
            préféUti.setAgreeArticle(value);
  
            
        }
    }
    public boolean IsNombreAgree(){if (préféUti!=null) return préféUti.isAgreeNombre(); return false;}
    public  void IsNombreAgree(boolean value)
    {   if (préféUti!=null){
            préféUti.setAgreeNombre(value);
  
            
        }
    }
    public boolean IsCarctSpecAgree(){if (préféUti!=null) return préféUti.isCaractèreSpéciaux(); return false;}
    public  void IsCarctSpecAgree(boolean value)
    {   if (préféUti!=null){
            préféUti.setCaractèreSpéciaux(value);
  
            
        }
    }
    
    
    public nuagequivolenetombepas.modèle.Gamme getGamme(){if (préféUti!=null) return préféUti.getGamme(); return null;}
    public  void setGamme(nuagequivolenetombepas.modèle.Gamme value)
    {   if (préféUti!=null){
            préféUti.setGamme(value);
  
            
        }
    }
    
    
   
     
       static void trierAlphabetiquementMotsStocker(ArrayList<MotAffiché> motsAffichésTemporaire) {
        // tri par insertion
           System.out.println("Trier");

        int i,nouvellePosition;
       
         int n = motsAffichésTemporaire.size();
            for(i=0;i<n;++i)
            {
                for (MotAffiché o: motsAffichésTemporaire)
                System.out.print(o.getMot()+" ");
                System.out.println("");
               nouvellePosition = i;
               while (nouvellePosition != 0 && 
                  (motsAffichésTemporaire.get(nouvellePosition).compareTo(motsAffichésTemporaire.get(nouvellePosition-1)) <0 ))
                        {
                                
                                motsAffichésTemporaire.set(nouvellePosition-1,
                                        motsAffichésTemporaire.set(nouvellePosition,motsAffichésTemporaire.get(nouvellePosition-1))
                                        );

                                nouvellePosition = nouvellePosition-1 ;
                        }
            } 
           
    }
       
    
     
    static public void shuffle(ArrayList<ObjetPositionné> lesObjets) {
   
        
        Collections.shuffle(lesObjets);
    } 
    static public void shuffleMots(ArrayList<MotAffiché> lesObjets) {
   
        
        Collections.shuffle(lesObjets);
    } 
        
    

    public String getPolice(){if (préféUti!=null) return préféUti.getPolice(); return "";}
    public  void setPolice(String value)
    {   if (préféUti!=null){
            préféUti.setPolice(value);
  
            
        }
    }

    static public Gamme[] getGammes() {
        return gammes;
    }

    static public String[] getNameGammes() {
       if (getGammes()!=null)
       {
           int index= getGammes().length;
            
            String [] tmpS;
           if (index >0)
           {

               Gamme[] tmpG= getGammes();
               tmpS= new String[index];
               for(int i= 0;i<index; i++)
               {
                   tmpS[i]= tmpG[i].getNom();


               }
           }
           else return new String[0];

           return tmpS;
       }
       return new String[0];
    }
     static public Gamme getGammes(String nomGamme) {
       
         for(Gamme tmpG: getGammes())
         {
         if (tmpG.getNom().equals(nomGamme)) return tmpG;
         }
         return null;
    }

    
    static public String[] getNamePolices() {
        return polices;
    }


    
     static private void définirLesGammesParDefautl() {
        
         gammes = new Gamme[6];
         
         String tmpName;
        Color[] tmpTabCouleur;
        nuagequivolenetombepas.modèle.Gamme tmpGamme;
        Gamme tmpPanGamme;
        tmpName ="Default";
        tmpTabCouleur = new Color[]
                    {new Color(255,0,0),new Color(0,255,0),new Color(0,0,255)};
        tmpGamme= new nuagequivolenetombepas.modèle.Gamme(tmpName,tmpTabCouleur);
        //tmpPanGamme = new Gamme(tmpGamme);
        
        gammes[0]=tmpGamme;
        
        tmpName ="Octobre1";
        tmpTabCouleur = new Color[]
                    {new Color(87,120,103),new Color(237,206,130),new Color(214,134,68),new Color(171,50,41),new Color(102,40,69)};
        tmpGamme = new nuagequivolenetombepas.modèle.Gamme(tmpName,tmpTabCouleur);
       gammes[1]=tmpGamme;
        
        
        
        
                tmpName ="Bunch of colors";
        tmpTabCouleur = new Color[]
                    {new Color(148,142,42),new Color(221,141,53),new Color(184,104,52),new Color(133,60,44),new Color(89,43,39)};
        tmpGamme = new nuagequivolenetombepas.modèle.Gamme(tmpName,tmpTabCouleur);
        gammes[2]=tmpGamme;
        
        
        
                tmpName ="Mercury Glass";
        tmpTabCouleur = new Color[]
                    {new Color(188,193,182),new Color(140,136,123),new Color(115,98,77),new Color(64,24,1),new Color(38,15,1)};
        tmpGamme = new nuagequivolenetombepas.modèle.Gamme(tmpName,tmpTabCouleur);
        gammes[3]=tmpGamme;
        
        
        
        
                tmpName ="lunchfabriek01";
        tmpTabCouleur = new Color[]
                    {new Color(162,191,59),new Color(217,217,217),new Color(140,140,140),new Color(38,38,38),new Color(0,50,89)};
        tmpGamme = new nuagequivolenetombepas.modèle.Gamme(tmpName,tmpTabCouleur);
        gammes[4]=tmpGamme;
        
        
        
                tmpName ="gaelich";
        tmpTabCouleur = new Color[]
                   {new Color(195,201,204),new Color(134,211,255),new Color(71,187,255)};
        tmpGamme = new nuagequivolenetombepas.modèle.Gamme(tmpName,tmpTabCouleur);
        gammes[5]=tmpGamme;
        

    }

    static int getHauteurMaxGrille() {
    
    return Grille.getMaxHeight();
    
    }
    
    static int getLargeurMaxGrille() {
   
        return Grille.getMaxWidth();
   
    }


    
}
