<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : transformateurXML2HTML.xsl
    Created on : 9 octobre 2011, 19:35
    Author     : eloistree
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Fichier XHTML depuis XML</title>
                <!--
                <style type="text/css">
                div
                {
                    background:none repeat scroll 0 0 transparent;
                    border:0 none;
                    font-size: 100%;
                    padding:0;
                    margin:0;
                  
                }
                </style>-->
            </head>
            <body>
                <xsl:call-template name="process"/>
               
            </body>
        </html>
    </xsl:template>
    
    
    <xsl:template name="process">
      
        <div><xsl:attribute name="style">
                height:<xsl:value-of select="/grille/dimension/hauteur"/><xsl:value-of select="/grille/dimension/hauteur/@value"/>;
                width:<xsl:value-of select="/grille/dimension/largeur"/><xsl:value-of select="/grille/dimension/largeur/@value"/>;   
                background-color:rgb(<xsl:value-of select="/grille/background/red"/>,<xsl:value-of select="/grille/background/green"/>,<xsl:value-of select="/grille/background/blue"/>);
                border: 1px solid black;
                position: absolute;
                left:0;
                top:0;
            </xsl:attribute>
            <xsl:for-each select="/grille/objet">
               
                <xsl:apply-templates select="." />
            </xsl:for-each>
        </div>
            
    </xsl:template>
    <xsl:template match="objet">
    
    

        <div ><xsl:attribute name="style">
                height:<xsl:value-of select="dimension/hauteur"/><xsl:value-of select="dimension/hauteur/@value"/>;
                width:<xsl:value-of select="dimension/largeur"/><xsl:value-of select="dimension/largeur/@value"/>;   
                font-size:<xsl:value-of select="autre/size"/>;
                position: absolute;<!--la relative foire et j'ai pas trouver pour positionner à partir du html-->
                top:<xsl:value-of select="position/y"/><xsl:value-of select="position/y/@value"/>;
                left:<xsl:value-of select="position/x"/><xsl:value-of select="position/x/@value"/>;
                color:rgb(<xsl:value-of select="autre/couleur/red"/>,<xsl:value-of select="autre/couleur/green"/>,<xsl:value-of select="autre/couleur/blue"/>);
                font-family:"<xsl:value-of select="autre/police"/>";
                font-weight:bold;
                
                <!--problème avec l'alignement vertical-->
                <!--vertical-align:text-top;-->
            </xsl:attribute><xsl:value-of select="./text()"/></div>
    
    
    </xsl:template>

</xsl:stylesheet>


