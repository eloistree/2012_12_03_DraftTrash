/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * selectionMotDansNuage.java
 *
 * Created on 17 oct. 2011, 21:18:01
 */
package AllGUI.composant;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;
import nuagequivolenetombepas.ControlerNuage;
import nuagequivolenetombepas.modèle.MotAffiché;

/**
 *
 * @author eloistree
 */
    public class JPanSelection extends javax.swing.JPanel implements Observer{
    
    private ControlerNuage cn;
    

    /** Creates new form selectionMotDansNuage */
    public JPanSelection(ControlerNuage cn) {
        this.cn = cn;
        initComponents();
    }

    public JPanSelection(ControlerNuage cn,MotAffiché value) {
       this(cn);
       
       
    }
    
    public JPanSelection(ControlerNuage cn,JPanNuage value) {
        this(cn);
    }
    
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanModifierSelection = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabMotSelectionné = new javax.swing.JLabel();
        jLabTitreMotSelectionner = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabTitreTaille = new javax.swing.JLabel();
        jSliderTaille = new javax.swing.JSlider();
        jLabelTaille = new javax.swing.JLabel();
        jLabelTitrePolice = new javax.swing.JLabel();
        jComboBoxPolice = new javax.swing.JComboBox();
        jRadioButtonIsVertical = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        jPanAfficheCouleur = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jButtSupprimerLeMot = new javax.swing.JButton();

        jPanModifierSelection.setBorder(javax.swing.BorderFactory.createTitledBorder("Modifier mot selectionné"));
        jPanModifierSelection.setMaximumSize(new java.awt.Dimension(228, 360));
        jPanModifierSelection.setMinimumSize(new java.awt.Dimension(178, 291));
        jPanModifierSelection.setSize(new java.awt.Dimension(228, 360));
        jPanModifierSelection.setLayout(new java.awt.GridLayout(3, 1, 1, 1));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        jLabMotSelectionné.setFont(new java.awt.Font("Lucida Grande", 0, 18));
        jLabMotSelectionné.setForeground(new java.awt.Color(153, 153, 153));
        jLabMotSelectionné.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(jLabMotSelectionné);

        jLabTitreMotSelectionner.setText("Mot sélectionner:");

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabTitreMotSelectionner)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(20, 20, 20)
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(jLabTitreMotSelectionner)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanModifierSelection.add(jPanel5);

        jLabTitreTaille.setText("Taille de caractère");

        jSliderTaille.setMinimum(12);
        jSliderTaille.setPaintLabels(true);
        jSliderTaille.setValue(12);
        jSliderTaille.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSliderTailleStateChanged(evt);
            }
        });

        jLabelTaille.setText("12");

        jLabelTitrePolice.setText("Police");

        jComboBoxPolice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { ""}));
        jComboBoxPolice.setEnabled(false);

        jRadioButtonIsVertical.setText("Vertical");
        jRadioButtonIsVertical.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonIsVerticalActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jLabTitreTaille))
                    .add(jPanel3Layout.createSequentialGroup()
                        .add(19, 19, 19)
                        .add(jSliderTaille, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 122, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jLabelTaille))
                    .add(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jLabelTitrePolice)))
                .addContainerGap())
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                .add(22, 22, 22)
                .add(jComboBoxPolice, 0, 155, Short.MAX_VALUE)
                .add(18, 18, 18))
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .add(jRadioButtonIsVertical)
                .addContainerGap(99, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jLabTitreTaille)
                .add(2, 2, 2)
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabelTaille)
                    .add(jSliderTaille, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(4, 4, 4)
                .add(jLabelTitrePolice)
                .add(5, 5, 5)
                .add(jComboBoxPolice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jRadioButtonIsVertical)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanModifierSelection.add(jPanel3);

        jPanel4.setLayout(new java.awt.GridLayout(3, 0));

        jPanAfficheCouleur.setBackground(new java.awt.Color(255, 255, 255));
        jPanAfficheCouleur.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        jPanAfficheCouleur.setForeground(new java.awt.Color(255, 255, 255));
        jPanAfficheCouleur.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanAfficheCouleurMouseClicked(evt);
            }
        });

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(153, 153, 153));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Changer la couleur");
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanAfficheCouleurLayout = new org.jdesktop.layout.GroupLayout(jPanAfficheCouleur);
        jPanAfficheCouleur.setLayout(jPanAfficheCouleurLayout);
        jPanAfficheCouleurLayout.setHorizontalGroup(
            jPanAfficheCouleurLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
        );
        jPanAfficheCouleurLayout.setVerticalGroup(
            jPanAfficheCouleurLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
        );

        jPanel4.add(jPanAfficheCouleur);

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 193, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 41, Short.MAX_VALUE)
        );

        jPanel4.add(jPanel7);

        jButtSupprimerLeMot.setText("Supprimer le mot");
        jButtSupprimerLeMot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtSupprimerLeMotActionPerformed(evt);
            }
        });
        jPanel4.add(jButtSupprimerLeMot);

        jPanModifierSelection.add(jPanel4);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanModifierSelection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 205, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanModifierSelection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 399, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

private void jSliderTailleStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSliderTailleStateChanged

    javax.swing.JSlider tmpSource = (javax.swing.JSlider) evt.getSource();
    int value = tmpSource.getValue();
    cn.setSizeToThisMot(this.jLabMotSelectionné.getText(),value);
    //this.jLabelTaille.setText(""+value);
        
}//GEN-LAST:event_jSliderTailleStateChanged

private void jRadioButtonIsVerticalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonIsVerticalActionPerformed

    javax.swing.JRadioButton tmp = (javax.swing.JRadioButton) evt.getSource();
    
    boolean seleciton =tmp.isSelected();
    cn.setVerticalToThisMot(jLabMotSelectionné.getText(),seleciton);

       
    


}//GEN-LAST:event_jRadioButtonIsVerticalActionPerformed

private void jPanAfficheCouleurMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanAfficheCouleurMouseClicked
/*
 changerCouleur();
   
     * 
     */
}//GEN-LAST:event_jPanAfficheCouleurMouseClicked

private void jButtSupprimerLeMotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtSupprimerLeMotActionPerformed

    cn.supprimer(jLabMotSelectionné.getText());
    
}//GEN-LAST:event_jButtSupprimerLeMotActionPerformed

private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked

    if(jLabMotSelectionné.getText().equals("{BackGround}")){
    Color tmp =    cn.ouvrirUnPanelCouleur(jPanAfficheCouleur.getBackground());
     cn.setNuageBackGroundTo( tmp);
    }
    else cn.setColorToThisMot(jLabMotSelectionné.getText(), cn.ouvrirUnPanelCouleur(jPanAfficheCouleur.getBackground()));
}//GEN-LAST:event_jLabel2MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtSupprimerLeMot;
    private javax.swing.JComboBox jComboBoxPolice;
    private javax.swing.JLabel jLabMotSelectionné;
    private javax.swing.JLabel jLabTitreMotSelectionner;
    private javax.swing.JLabel jLabTitreTaille;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelTaille;
    private javax.swing.JLabel jLabelTitrePolice;
    private javax.swing.JPanel jPanAfficheCouleur;
    private javax.swing.JPanel jPanModifierSelection;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JRadioButton jRadioButtonIsVertical;
    private javax.swing.JSlider jSliderTaille;
    // End of variables declaration//GEN-END:variables




      public void configurerPanelModifierSelection(JPanNuage value) {
     
       if( value!=null )
     { 
         configurerPanelModifierSelection();
         jLabTitreTaille.setVisible(false);
         jLabelTitrePolice.setVisible(false);
         jRadioButtonIsVertical.setVisible(false);
         jRadioButtonIsVertical.setEnabled(false);
         jRadioButtonIsVertical.setSelected(false);
         jLabMotSelectionné.setText("{BackGround}");
       
         String tmp = "";
         jComboBoxPolice.setVisible(false);
         jComboBoxPolice.setModel(new javax.swing.DefaultComboBoxModel(new String[] {tmp}));
         jSliderTaille.setVisible(false);
         jSliderTaille.setValue(0);
         jLabelTaille.setVisible(false);
         jLabelTaille.setText("");
         jPanAfficheCouleur.setBackground(value.getBackground());
         jPanAfficheCouleur.repaint();
         jButtSupprimerLeMot.setEnabled(false);
         
         
            

     }
       else configurerPanelModifierSelection();
     
    
       
    }

    public void configurerPanelModifierSelection(MotAffiché value)
    {
         if( value!=null )
         { 
             
             System.out.append("ok");

             jLabTitreTaille.setVisible(true);
             jLabelTitrePolice.setVisible(true);
             jRadioButtonIsVertical.setVisible(true);
             jRadioButtonIsVertical.setEnabled(true);
             jRadioButtonIsVertical.setSelected(value.isVertical());
             jLabMotSelectionné.setText(value.getMot());
             jLabMotSelectionné.repaint();

             String tmp = value.getPolice();
             jComboBoxPolice.setVisible(true);
             jComboBoxPolice.setModel(new javax.swing.DefaultComboBoxModel(new String[] {tmp}));
             jSliderTaille.setVisible(true);
             jSliderTaille.setValue(value.getSize());
             jLabelTaille.setVisible(true);
             jLabelTaille.setText(""+value.getSize());
             jPanAfficheCouleur.setBackground(value.getCouleur());
             jPanAfficheCouleur.repaint();
             jButtSupprimerLeMot.setEnabled(true);
          

         }  
       else configurerPanelModifierSelection();

    }
    public void configurerPanelModifierSelection()
    {
             
             jLabTitreTaille.setVisible(true);
             jLabelTitrePolice.setVisible(true);
                    jRadioButtonIsVertical.setEnabled(false);
                    jRadioButtonIsVertical.setSelected(false);
             jPanAfficheCouleur.setBackground(Color.WHITE);
             jLabMotSelectionné.setText("");
             jSliderTaille.setValue(0);
             jLabelTaille.setText("0");
             jRadioButtonIsVertical.setEnabled(false);
             jComboBoxPolice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { ""}));
             jButtSupprimerLeMot.setEnabled(false);
   
    }

    private void supprimerLeMotCourant() {
       
        // supprimer du panel nuage de mot  le mots courant basé sur le String
        
        
    }

    @Override
    public void update(Observable o, Object o1) {
       
         if (o1 instanceof String)
        {
            if (o1.equals("autoDesctrution"))
            {
            System.out.println("Suppression : "+ (String )o1);
            configurerPanelModifierSelection();
            }
        }
        else if (o instanceof MotAffiché){configurerPanelModifierSelection((MotAffiché) o );}
        
    }

}
